@extends('layouts.master.master')

@section('title', 'Home')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/house/list">House List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">House {{ $house->nickname }}</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Individuals</h4>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
							@foreach($house->individuals AS $individual)
							<tr>
								<td>{{ $individual->first_name }} {{ $individual->last_name }}</td>
								<td>
									<a class="btn btn-sm btn-success" href="/individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a>
									@if(Auth::user()->hasGroup('provider', 'Manager'))
									<a class="btn btn-sm btn-warning" href="/individual/edit/{{encrypt($individual->id)}}"><i class="fa fa-pencil"></i></a>
									@endif
								</td>
							</tr>	
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Employees</h4>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
							@foreach($house->employees AS $employee)
							<tr>
								<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
								<td></td>
							</tr>	
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">House Information</h4>
					</div>
					<div class="panel-body">
						{{ $house->address }} <br />
						{{ $house->city }}, {{ $house->state }} {{ $house->zip }} <br />
						{{ $house->phone }}
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Emergency Drill Report & Monthly Safety Checklist
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Drill Type</th>
								<th>Date</th>
								<th>User</th>
								<th>Individuals</th>
								<th>Options</th>
							</tr>
							@foreach($house->safety_checklists AS $checklist)
								<tr>
									<td>{{$checklist->drill_type}}</td>
									<td>{{$checklist->drill_date->format('m/d/Y')}}</td>
									<td>{{$checklist->user->first_name}} {{$checklist->user->last_name}}</td>
									<td>
										<ul>
										@foreach($checklist->individuals AS $individual)
											<li>{{$individual->first_name}}</li>
										@endforeach
										</ul>
									</td>
									<td>
										<a class="btn btn-sm btn-success" href="/house/safety-checklist/{{encrypt($house->id)}}/view/{{encrypt($checklist->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroup('provider', 'Manager'))
										<a class="btn btn-sm btn-warning" href="/house/safety-checklist/{{encrypt($house->id)}}/edit/{{encrypt($checklist->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/house/safety-checklist/{{encrypt($house->id)}}/remove/{{encrypt($checklist->id)}}"><i class="fa fa-trash"></i></a>
										@endif
										
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/house/safety-checklist/{{encrypt($house->id)}}/add" class="btn btn-sm btn-success">Add Emergency/Safety Checklist</a>
						@endif
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- end #content -->
@stop