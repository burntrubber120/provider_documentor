@extends('layouts.master.master')

@section('title', 'House')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">House</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">House <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>House Add</legend>
								<div class="form-group">
									<label for="nickname">Nickname</label>
									<input type="text" name="nickname" class="form-control" placeholder="Nickname" value="{{ old('nickname') }}"/>
								</div>
								<div class="form-group">
									<label for="address">Address</label>
									<input type="text" name="address" class="form-control" placeholder="Address" value="{{ old('address') }}"/>
								</div>
								<div class="form-group">
									<label for="city">City</label>
									<input type="text" name="city" class="form-control" placeholder="City" value="{{ old('city') }}"/>
								</div>
								<div class="form-group">
									<label for="state">State</label>
									<input type="text" name="state" class="form-control" placeholder="State" value="{{ old('state') }}"/>
								</div>
								<div class="form-group">
									<label for="zip">Zip</label>
									<input type="text" name="zip" id="zip" class="form-control" placeholder="Zip" value="{{ old('zip') }}"/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone') }}"/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Add</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
$('#zip').mask('99999');
</script>
@stop