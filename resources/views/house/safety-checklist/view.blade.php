@extends('layouts.master.master')

@section('title', 'Drill/Safety')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Home</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Emergency Drill Report & Monthly Safety Checklist for <a href="/house/view/{{encrypt($house->id)}}">{{$house->nickname}}</a></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body f-s-14">
						<fieldset>
							<legend>
								<strong>Date Event/Drill Performed:</strong> 
									{{isset($safety_checklist->drill_date) ? $safety_checklist->drill_date->format('m/d/Y') : ''}} <br /> 
									Start {{isset($safety_checklist->drill_start_time) ? date('h:i A', strtotime($safety_checklist->drill_start_time)) : ''}}<br />
									End {{isset($safety_checklist->drill_end_time) ? date('h:i A', strtotime($safety_checklist->drill_end_time)) : ''}}
							</legend>
						</fieldset>
						<fieldset>
							<legend>Event/Drill Type</legend>
							<div class="form-group">
								<label for="drill_type">Event/Drill Type</label>
								<strong>{{$safety_checklist->drill_type}}</strong>
							</div>
							<div class="form-group">
								<label for="individuals">Individual(s):</label><br />
								<strong>
									@foreach($safety_checklist->individuals AS $k => $individual)
										{{$individual->full_name}}@if($k+1 < $safety_checklist->individuals->count()),@endif
									@endforeach
								</strong>
							</div>
							<div class="form-group">
								<label for="drill_type">What happened before the event/drill?</label><br />
								<strong>{{nl2br($safety_checklist->before_drill)}}</strong>
							</div>
							<div class="form-group">
								<label for="drill_type">What happened during the event/drill? Were any issues/concerns identified?</label><br />
								<strong>{{nl2br($safety_checklist->during_drill)}}</strong>
							</div>
							<div class="form-group">
								<label for="drill_type">What happened after the event/drill?</label><br />
								<strong>{{nl2br($safety_checklist->after_drill)}}</strong>
							</div>
						</fieldset>
						<fieldset>
							<legend>Environmental Safety</legend>
							<div class="form-group">
								<label for="fire_extinguisher">Fire extinguisher gauge in green area:</label>
								<strong>{{$safety_checklist->fire_extinguisher}}</strong>
							</div>
							
							<div class="form-group">
								<label for="fire_extinguisher_tag">Fire extinguisher tag initialed:</label>
								<strong>{{$safety_checklist->fire_extinguisher_tag}}</strong>
							</div>
							
							<div class="form-group">
								<label for="co_detector">CO Detector:</label>
								<strong>{{$safety_checklist->co_detector}}</strong>
							</div>
							
							<div class="form-group">
								<label for="water_temp_lowered">Water temp lowered:</label>
								<strong>{{$safety_checklist->water_temp_lowered}}</strong>
							</div>
							<div class="form-group">
								<label for="flash_light">Flashlight:</label>
								<strong>{{$safety_checklist->flash_light}}</strong>
							</div>
							<div class="form-group">
								<label for="chemicals_locked">Chemicals locked:</label>
								<strong>{{$safety_checklist->chemicals_locked}}</strong>
							</div>
							<div class="form-group">
								<label for="smoke_detectors">Smoke detectors:</label>
								<strong>{{$safety_checklist->smoke_detectors}}</strong>
							</div>
							<div class="form-group">
								<label for="first_aid_kit">First aid kit:</label>
								<strong>{{$safety_checklist->first_aid_kit}}</strong>
							</div>
							<div class="form-group">
								<label for="precautions_kit">Universal precautions kit:</label>
								<strong>{{$safety_checklist->precautions_kit}}</strong>
							</div>
							<div class="form-group">
								<label for="evac_plan">Evacuation plan:</label>
								<strong>{{$safety_checklist->evac_plan}}</strong>
							</div>
							<div class="form-group">
							<label for="phone_list">Emergency phone list:</label>
								<strong>{{$safety_checklist->phone_list}}</strong>
							</div>
							<div class="form-group">
								<label for="fire_ladder">Fire ladder:</label>
								<strong>{{$safety_checklist->fire_ladder}}</strong>
							</div>
							
						</fieldset>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop