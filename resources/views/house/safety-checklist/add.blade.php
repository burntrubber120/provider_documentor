@extends('layouts.master.master')

@section('title', 'Drill/Safety')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Home</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Emergency Drill Report & Monthly Safety Checklist for <a href="/house/view/{{encrypt($house->id)}}">{{$house->nickname}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Event/Drill Type</legend>
								<div class="form-group">
									<label for="drill_type">Event/Drill Type</label>
									{!! Form::select('drill_type', $drill_types, $safety_checklist->drill_type, ['class' => 'form-control', 'id' => 'drill_type', 'placeholder' => 'Select Drill Type']) !!}

									{!! Form::text('drill_type_other', $safety_checklist->drill_type_other, ['class' => 'form-control collapse', 'id' => 'drill_type_other', 'placeholder' => 'Drill Type Other']) !!}
								</div>
								<div class="form-group">
									<label for="individuals">Individual(s)</label>
									{!! Form::select('individuals[]', $house->individuals->pluck('full_name', 'id'), $safety_checklist->individuals->pluck('id'), ['class' => 'form-control multiple-select', 'multiple' => '']) !!}
								</div>
								<div class="form-group">
									<label for="drill_date">Event/Drill Date:</label>
									{!! Form::text('drill_date', isset($safety_checklist->drill_date) ? $safety_checklist->drill_date->format('m/d/Y') : '', ['class' => 'form-control calDatePicker', 'placeholder' => 'Drill Date']) !!}
								</div>
								<div class="form-group">
									<label for="drill_start_time">Event/Drill Start Time?</label>
									{!! Form::text('drill_start_time', $safety_checklist->drill_start_time, ['class' => 'form-control timePicker', 'placeholder' => 'Drill Start Time']) !!}
								</div>
								<div class="form-group">
									<label for="drill_end_time">Event/Drill End Time?</label>
									{!! Form::text('drill_end_time', $safety_checklist->drill_end_time, ['class' => 'form-control timePicker', 'placeholder' => 'Drill End Time']) !!}
								</div>
								<div class="form-group">
									<label for="before_drill">What happened before the event/drill?</label>
									{!! Form::textarea('before_drill', $safety_checklist->before_drill, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
								<div class="form-group">
									<label for="during_drill">What happened during the event/drill? Were any issues/concerns identified?</label>
									{!! Form::textarea('during_drill', $safety_checklist->during_drill, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
								<div class="form-group">
									<label for="after_drill">What happened after the event/drill?</label>
									{!! Form::textarea('after_drill', $safety_checklist->after_drill, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Environmental Safety</legend>
								<div class="form-group">
									<label for="fire_extinguisher">Fire extinguisher gauge in green area:</label>
									{!! Form::select('fire_extinguisher', $has_needs, $safety_checklist->fire_extinguisher, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="fire_extinguisher_tag">Fire extinguisher tag initialed:</label>
									{!! Form::select('fire_extinguisher_tag', $has_needs, $safety_checklist->fire_extinguisher_tag, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="co_detector">CO Detector:</label>
									{!! Form::select('co_detector', $has_needs, $safety_checklist->co_detector, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="water_temp_lowered">Water temp lowered:</label>
									{!! Form::select('water_temp_lowered', $has_needs, $safety_checklist->water_temp_lowered, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="flash_light">Flashlight:</label>
									{!! Form::select('flash_light', $has_needs, $safety_checklist->flash_light, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="chemicals_locked">Chemicals locked:</label>
									{!! Form::select('chemicals_locked', $has_needs, $safety_checklist->chemicals_locked, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="smoke_detectors">Smoke detectors:</label>
									{!! Form::select('smoke_detectors', $has_needs, $safety_checklist->smoke_detectors, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="first_aid_kit">First aid kit:</label>
									{!! Form::select('first_aid_kit', $has_needs, $safety_checklist->first_aid_kit, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="precautions_kit">Universal precautions kit:</label>
									{!! Form::select('precautions_kit', $has_needs, $safety_checklist->precautions_kit, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="evac_plan">Evacuation plan:</label>
									{!! Form::select('evac_plan', $has_needs, $safety_checklist->evac_plan, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="phone_list">Emergency phone list:</label>
									{!! Form::select('phone_list', $has_needs, $safety_checklist->phone_list, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="fire_ladder">Fire ladder:</label>
									{!! Form::select('fire_ladder', $has_needs, $safety_checklist->fire_ladder, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Checklist</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
function fnc_other_drill_type(drill_type) {
	if(drill_type == 'Other') {
			$('#drill_type_other').show();
		}
		else {
			$('#drill_type_other').hide();
		}
	}
$( document ).ready(function() {
	fnc_other_drill_type($('#drill_type').val());
	FormPlugins.init();
});
$('#drill_type').change(function () {
	fnc_other_drill_type($('#drill_type').val());
});
</script>
@stop