@extends('layouts.master.master')

@section('title', 'House')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/house/list">House List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$house->nickname}} <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<!-- begin panel -->
			<div class="panel panel-inverse">
			    <div class="panel-heading">
			        <div class="panel-heading-btn">
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			        </div>
			        <h4 class="panel-title">Calendar</h4>
			    </div>
			    <div class="panel-body p-0">
			        <div class="vertical-box">
			            <div class="vertical-box-column p-15 bg-silver width-sm">
                            <div id="external-events" class="calendar-event">
                                <h4 class=" m-b-20">Employees <i class='fa fa-users'></i></h4>
                                @foreach($house->employees AS $key => $employee)
                                <div class="external-event {{$color_array[$key]}}" data-bg="{{$color_array[$key]}}" data-title="{{$employee->first_name}} {{$employee->last_name}}" data-media="" data-desc="">
                                    <h5><i class="fa fa-user fa-lg fa-fw"></i> {{ $employee->first_name }} {{$employee->last_name}}</h5>
                                    <p>
                                    {!! Form::select('individual', $house->individuals->pluck('full_name', 'id'), NULL, ['class' => 'form-control schedule_individual', 'placeholder' => 'Select Individual']) !!}
                                    </p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="calendar2" class="vertical-box-column p-15 calendar width-full"></div>
			        </div>
			    </div>
			</div>
			<!-- end panel -->
		</div>
	</div>
	<!-- end #content -->
    <link href='/js/fullCalendar/core/main.css' rel='stylesheet' />
    <link href='/js/fullCalendar/daygrid/main.css' rel='stylesheet' />
    <link href='/js/fullCalendar/timegrid/main.css' rel='stylesheet' />
    <link href='/js/fullCalendar/list/main.css' rel='stylesheet' />

    <script src='/js/fullCalendar/core/main.js'></script>
    <script src='/js/fullCalendar/daygrid/main.js'></script>
    <script src='/js/fullCalendar/timegrid/main.js'></script>
    <script src='/js/fullCalendar/list/main.js'></script>
    <script>

      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar2');

        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'dayGrid', 'timeGrid', 'list'
            ],
            defaultView: 'timeGridWeek',
            views: {
            listDay: { buttonText: 'list day' },
            listWeek: { buttonText: 'list week' },
            dayGridWeek: { buttonText: 'Grid Week' },
            timeGridWeek: { buttonText: 'Grid Week' },
            dayGridMonth: { buttonText: 'Grid Month' }
            },

            header: {
            left: 'title',
            center: '',
            right: 'listDay,listWeek,timeGridWeek,dayGridMonth'
            },
            events: 'https://fullcalendar.io/demo-events.json'
        });

        calendar.render();
      });

    </script>
@stop