@extends('layouts.master.master')

@section('title', 'House')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">House List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">House <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				<table class="table table-striped table-bordered table-manage">
					<thead>
						<tr>
							<th>Nickname Name</th>
							<th>Address</th>
							<th>Phone</th>
							<th># Individuals</th>
							<th># Employees</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($houses AS $k => $house)
						<tr>
							<td>{{ $house->nickname }}</td>
							<td>{{ $house->address }}</td>
							<td>{{ $house->phone }}</td>
							<td>{{ $house->individuals->count() }}</td>
							<td>{{ $house->employees->count() }}</td>
							<td>
								<a class="btn btn-sm btn-success" href="./view/{{encrypt($house->id)}}"><i class="fa fa-eye"></i></a>
								<a class="btn btn-sm btn-warning" href="./edit/{{encrypt($house->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger" href="./remove/{{encrypt($house->id)}}"><i class="fa fa-times"></i></a> 
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop