@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Edit Service Note</li>
		</ol>
        <!-- end breadcrumb -->
		
        <!-- begin page-header -->
		<h1 class="page-header"><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form method="POST" id="edit_cn_form" class="form" action="">
							<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
							<fieldset>
								<legend>{{$action}} Service Note</legend>
								<div class="form-group">
									<div class="input-group width-full date">
									<label for="first_name">Start Date/Time</label>
									<input type="text" name="start_dt" class="form-control calDateTimePicker" placeholder="Start Date/Time" value="{{ old('start_dt', isset($case_note->start_dt) ? $case_note->start_dt->format('m/d/Y h:i A') : '') }}"/>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group width-full  date">
									<label for="first_name">End Date/Time</label>
									<input type="text" name="end_dt" class="form-control calDateTimePicker" placeholder="End Date/Time" value="{{ old('end_dt', isset($case_note->end_dt) ? $case_note->end_dt->format('m/d/Y h:i A') : '') }}"/>
									</div>
								</div>
                                <div class="form-group">
									<label for="location">Location</label>
                                    @php 
                                        $location = NULL;
                                        if($case_note->location != 'Home' && $case_note->location != '') {
                                            $location = 'Other';
                                        }
                                        if($case_note->location == 'Home') {
                                            $location = 'Home';
                                        }
                                    @endphp                                    
                                    {!! Form::select('location', $location_value, $location, ['class' => 'form-control', 'id' => 'location', 'placeholder' => 'Please Select']) !!}
								</div>
                                <div class="form-group {{ $location == 'Other' ? '' : 'collapse' }}" id="location_other_div">
									<div class="input-group width-full  date">
									<label for="location_other">Location Other:</label>
                                    {!! Form::text('location_other', $case_note->location, ['class' => 'form-control', 'placeholder' => 'Enter Location']) !!}
									</div>
								</div>
								<div class="form-group">
									<label for="note">Note</label>
									<textarea name="note" class="form-control" placeholder="Note" >{{ old('note', $case_note->case_note) }}</textarea>
								</div>
                                <div class="form-group">
									<label for="engaged">Was the Individual engaged at least 80% of the time</label>
                                    {!! Form::select('engaged', $yes_no, $case_note->engaged, ['class' => 'form-control', 'id' => 'engaged', 'placeholder' => 'Please Select']) !!}
								</div>
                                <div class="form-group {{ $case_note->engaged == 'Yes' ? '' : 'collapse' }}" id="engaged_other_div">
									<div class="input-group width-full  date">
									<label for="engaged_other">Who was the individual involved with? (Caregiver/Community Members)</label>
                                    {!! Form::text('engaged_other', $case_note->engaged_other, ['class' => 'form-control', 'placeholder' => 'Enter Caregiver/Community Members']) !!}
									</div>
								</div>
								@if($individual->goals->count() > 0)
								<div class="form-group">
									<label for="service">Goals/Strategies</label>
                                    {!! Form::select('strategy', $individual->strategies->pluck('strategy_goal', 'id'), $case_note->strategy_id, ['class' => 'form-control', 'id' => 'engaged', 'placeholder' => 'Please Select']) !!}
								</div>
								@endif
                                
                                <div class="form-group">
									<div class="input-group">
									<label for="goal_duration">How long was the goal/strategy taught? (ex. 35)</label>
                                    {!! Form::text('goal_duration', $case_note->goal_duration, ['class' => 'form-control', 'placeholder' => 'Enter Goal/Strategy Teaching Amount']) !!}
									</div>
								</div>
								<div class="form-group">
									<label for="service">Service</label>
									<select name="service" class="form-control">
										<option value="">Please Select</option>
										@foreach($services AS $service)
											<option value="{{$service->id}}" {{$case_note->service_id == $service->id ? 'selected="selected"' : '' }}>{{$service->code}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="priority">Priority</label>
									<select name="priority" id="priority" class="form-control">
										<option value="Normal" {{$case_note->priority == 'Normal' ? 'selected="selected"' : '' }}>Normal</option>
										<option value="Non Reportable Incident" {{$case_note->priority == 'Non Reportable Incident' ? 'selected="selected"' : '' }}>Non Reportable Incident</option>
										<option value="Reportable Incident" {{$case_note->priority == 'Reportable Incident' ? 'selected="selected"' : '' }}>Reportable Incident</option>
									</select>
								</div>
								<div class="form-group {{isset($case_note->priority) && $case_note->priority == 'Reportable Incident' ? '' : 'collapse' }}" id="reportable_div">
									<label for="reportable">Reportable Text</label>
									<select name="reportable" class="form-control">
										<option value="">Please Select</option>
										@foreach($reportables AS $reportable)
											<option value="{{$reportable->id}}" {{$case_note->reportable_id == $reportable->id ? 'selected="selected"' : '' }}>{{$reportable->reportable_text}}</option>
										@endforeach
									</select>
								</div>

                                <h4>Quality of Life</h4>
                                <div class="form-group">
									<label for="meaning_full">Was the activity meaningful?</label>
                                    {!! Form::select('meaning_full', $yes_no, $case_note->meaning_full, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

                                <div class="form-group">
									<label for="qol_value">Using the QoL Plan choose a value that best matches the activity </label>
                                    {!! Form::select('qol_value', $qol_value, $case_note->qol_value, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

                                <div class="form-group">
									<label for="qol_plus_eoc">Reflect on your interactions. Choose one Element of Companionship that indicates how you valued the individual</label>
                                    {!! Form::select('qol_plus_eoc', $qol_plus_eoc, $case_note->qol_plus_eoc, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

                                <div class="form-group">
									<label for="qol_minus_eoc">Reflect on your interactions. Choose one Element of Companionship that you want to <strong>improve</strong>.</label>
                                    {!! Form::select('qol_minus_eoc', $qol_minus_eoc, $case_note->qol_minus_eoc, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

                                <div class="form-group">
									<label for="goal_ability">Ability to complete the task.</label>
                                    {!! Form::select('goal_ability', $goal_ability_engagement, $case_note->goal_ability, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

                                <div class="form-group">
									<label for="goal_engagement">Engagment with others in the activity/task.</label>
                                    {!! Form::select('goal_engagement', $goal_ability_engagement, $case_note->goal_engagement, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>

								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save Note</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
		</div>
	</div>
	<!-- end #content -->
<script>
$("#engaged").change(function () {
    if($(this).val() == 'Yes') {
        $('#engaged_other_div').show();
    }
    else {
        $('#engaged_other_div').hide();
    }

});
$("#location").change(function () {
    if($(this).val() == 'Other') {
        $('#location_other_div').show();
    }
    else {
        $('#location_other_div').hide();
    }

});
$("#priority").change(function () {
    if($(this).val() == 'Reportable Incident') {
        $('#reportable_div').show();
    }
    else {
        $('#reportable_div').hide();
    }

});
</script>
@stop