@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Service Note Reject</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header"><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form method="POST" id="edit_cn_form" class="form" action="">
							<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
							<fieldset>
								<legend>Reject Service Note</legend>
								
								<div class="form-group">
									<label for="reject_note">Reject Note</label>
									<textarea name="reject_note" class="form-control" placeholder="Note" >{{ old('reject_note', $case_note->reject_note) }}</textarea>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop