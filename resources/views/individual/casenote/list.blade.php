@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Service Note List</li>
		</ol>
        <!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header"><a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->first_name}} {{$individual->last_name}}</a> <small>service note list</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				@if($individual->ten_casenotes->count() == 0)
					<div class="note note-warning"><h4>No Service Notes</h4></div>
				@else
					<table class="table table-striped table-bordered table-manage">
						<thead>
							<tr>
								<th class="collapse">&nbsp;</th>
								<th>Start Date/Time</th>
								<th>End Date/Time</th>
								<th>Created</th>
								<th>Note</th>
								<th>Added By</th>
								<th>Priority</th>
								<th>Service</th>
								<th>Goal</th>
								<th>Strategy</th>
								<th>Options</th>
							</tr>	
						</thead>
						<tbody>
							@foreach($individual->casenotes AS $case_note)
							<tr>
								<td class="collapse">&nbsp;</td>
								<td>{{ $case_note->start_dt->format('m/d/Y h:i A') }}</td>
								<td>{{ $case_note->end_dt->format('m/d/Y h:i A') }}</td>
								<td>{{ $case_note->created_at->format('m/d/Y h:i A') }}</td>
								<td>{!! nl2br($case_note->case_note) !!}</td>
								<td>{{ $case_note->user->first_name }} {{ $case_note->user->last_name }}</td>
								<td>{{ $case_note->priority}}</td>
								<td>{{ isset($case_note->service) ? $case_note->service->code : "" }}</td>
								<td>{{ isset($case_note->goal) ? $case_note->goal->goal : "" }} </td>
								<td>{{ isset($case_note->strategy) ? $case_note->strategy->strategy : "" }}	</td>
								<td>
									@if($case_note->note_date > date('Y-m-d', strtotime('-2 day')))
										@if($case_note->user_id == Auth::id() || Auth::user()->hasGroupType('provider', 'Administrator'))
											<a href="/individual/casenote/{{encrypt($individual->id)}}/edit/{{encrypt($case_note->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
										@endif
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@endif
				</table>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop