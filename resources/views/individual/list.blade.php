@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Individual List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Individual <small>list</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				<table class="table table-striped table-bordered table-manage">
					<thead>
						<tr>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>House</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($individuals AS $k => $individual)
						<tr>
							<td>{{ $individual->first_name }} {{ $individual->last_name }}</td>
							<td>{{ $individual->phone }}</td>
							<td>{{ $individual->email }}</td>
							<td>{{ $individual->house }}</td>
							<td>
								<a class="btn btn-sm btn-success" href="./view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a>
								<a class="btn btn-sm btn-warning" href="./edit/{{encrypt($individual->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger" href="./remove/{{encrypt($individual->id)}}"><i class="fa fa-times"></i></a> 
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>		
			<div>
				<a href="./add" class="btn btn-primary">Add Individual</a>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop