@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Goal/Strategy View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-heading">
						<h2>
							{{ $individual->first_name }} {{ $individual->last_name }}'s Quarterly Summary &mdash; 
							{{$quarterly_summary->year}} Quarter {{$quarterly_summary->quarter}}
						</h2>
					</div>
					<div class="panel-body f-s-14">
						
							<fieldset class="">
								<div class="form-group">
									<label for="date_complete">Date Quarterly Service Summary Performed</label>
									<strong>{{isset($quarterly_summary->date_complete) ? $quarterly_summary->date_complete->format('m/d/Y') : null}}</strong>
								</div>
								<div class="form-group">
									<label for="year">Year/Quarter</label><br />
									<strong>{{$quarterly_summary->year}} - Quarter {{$quarterly_summary->quarter}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Services Provider By Month</legend>
								<div class="form-group">
									<label for="month_one_services">Month 1:</label><br />
									<strong>
										@foreach($quarterly_summary->month_one_services AS $k => $month_service)
											{{$month_service->code}}@if($k+1 < $quarterly_summary->month_one_services->count()),@endif
										@endforeach
									</strong>
								</div>
								<div class="form-group">
									<label for="month_two_services">Month 2:</label><br />
									<strong>
										@foreach($quarterly_summary->month_two_services AS $k => $month_service)
											{{$month_service->code}}@if($k+1 < $quarterly_summary->month_two_services->count()),@endif
										@endforeach
									</strong>
								</div>
								<div class="form-group">
									<label for="month_three_services">Month 3:</label><br />
									<strong>
										@foreach($quarterly_summary->month_three_services AS $k => $month_service)
											{{$month_service->code}}@if($k+1 < $quarterly_summary->month_three_services->count()),@endif
										@endforeach
									</strong>
								</div>
								<div class="form-group">
									<label for="service_issues">List any issues related to service provided. This would include the need for a BMR, change in services provided, etc...:</label><br />
									<strong>{{nl2br($quarterly_summary->service_issues)}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>NOA | PCISP | BSP</legend>
								<div class="form-group">
									<label for="noa_start_date">NOA Start Date:</label>
									<strong>{{isset($quarterly_summary->noa_start_date) ? $quarterly_summary->noa_start_date->format('m/d/Y') : NULL}}</strong>
								</div>
								<div class="form-group">
									<label for="noa_end_date">NOA End Date:</label>
									<strong>{{isset($quarterly_summary->noa_end_date) ? $quarterly_summary->noa_end_date->format('m/d/Y') : NULL}}</strong>
								</div>
								<div class="form-group">
									<label for="noa_reflect">Does NOA reflect services provided?</label>
									<strong>{{$quarterly_summary->noa_reflect}}</strong>
									</div>
								<div class="form-group">
									<label for="pcisp_date">Current PCSIP Date:</label>
									<strong>{{isset($quarterly_summary->pcisp_date) ? $quarterly_summary->pcisp_date->format('m/d/Y') : NULL}}</strong>
								</div>
								<div class="form-group">
									<label for="bsp_date">Current BSP Date:</label>
									<strong>{{isset($quarterly_summary->bsp_date) ? $quarterly_summary->bsp_date->format('m/d/Y') : NULL}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Transportation Service</legend>
								<div class="form-group">
									<label for="transportation_utilized">Is transportation being utilized?</label>
									<strong>{{$quarterly_summary->transportation_utilized}}</strong>
								</div>
								<div class="form-group">
									<label for="transportation_noa">Is transportation on the NOA?</label>
									<strong>{{$quarterly_summary->transportation_noa}}</strong>
								</div>
								<div class="form-group">
									<label for="transportation_type">Explain type of transportation (i.e. Mets/Taxi/Staff):</label>
										<strong>{{$quarterly_summary->transportation_type}}</strong>
									</div>
								<div class="form-group">
									<label for="transportation_logs">Transportation logs utilized?</label>
									<strong>{{$quarterly_summary->transportation_logs}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Environmental</legend>
								<div class="form-group">
									<label for="five_day_med">Is QRL responsible per the PCISP?</label>
									<strong>{{$quarterly_summary->five_day_med}}</strong>
								</div>
								<div class="form-group">
									<label for="fire_extinguisher">Fire extinguisher in green and initialed:</label>
									<strong>{{$quarterly_summary->fire_extinguisher}}</strong>
								</div>
								<div class="form-group">
									<label for="evac_posted">Evacuation route posted:</label>
									<strong>{{$quarterly_summary->evac_posted}}</strong>
								</div>
								<div class="form-group">
									<label for="smoke_detector">Smoke detector tested:</label>
									<strong>{{$quarterly_summary->smoke_detector}}</strong>
								</div>
								<div class="form-group">
									<label for="carbon_detector">Carbon monoxide detector tested:</label>
									<strong>{{$quarterly_summary->carbon_detector}}</strong>
								</div>
								<div class="form-group">
									<label for="adaptive_equipment">Adaptive equipment (i.e. utensils/wheelchair/ shower chair):</label>
									<strong>{{$quarterly_summary->adaptive_equipment}}</strong>
								</div>
								<div class="form-group">
									<label for="equipment_issues">Any issues or concerns with equipment?</label><br />
									<strong>{{nl2br($quarterly_summary->equipment_issues)}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Community</legend>
								<div class="form-group">
									<label for="community_events">Any special events or activities participated in this quarter?</label>
									<strong>{{$quarterly_summary->community_events}}</strong>
								</div>
								<div class="form-group">
									<label for="community_assessment">Assessment of Community Activities: (Where did the consumer go and is the consumer benefiting from these community experiences, what changes should be made to their current community involvement):</label><br />
									<strong>{{nl2br($quarterly_summary->community_assessment)}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Wellness and Health Coordination</legend>
								<div class="form-group">
									<label for="wellness_utilized">Wellness service utilized?</label>
									<strong>{{$quarterly_summary->wellness_utilized}}</strong>
								</div>
								<div class="form-group">
									<label for="medical_concerns">Describe any other significant Medical Concerns / Issues / Changes (Were any appointments missed, therapies, prior authorizations, or health care professional needed, etc…)</label><br />
									<strong>{{nl2br($quarterly_summary->medical_concerns)}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Medical Appointments</legend>
								<div class="form-group">
								@if($individual->medical_appointments->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Appointment Date</div>
										<div class="col-sm-3">Physician</div>
										<div class="col-sm-3">Purpose of Visit</div>
										<div class="col-sm-3">Follow up date</div>
									</div>
									@foreach($individual->medical_appointments AS $medical_appointment)
										<div class="row">
											<div class="col-sm-3">{{isset($medical_appointment->appointment_date) ? $medical_appointment->appointment_date->format('m/d/Y') : ''}}</div>
											<div class="col-sm-3">{{$medical_appointment->doctor_name}}</div>
											<div class="col-sm-3">{{nl2br($medical_appointment->purpose_of_visit)}}</div>
											<div class="col-sm-3">{{isset($medical_appointment->follow_up_date) ? $medical_appointment->follow_up_date->format('m/d/Y') : ''}}</div>
										</div>
									@endforeach
								@else
									<div><strong class="text-warning">No Medical Appointments</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Risk Plans</legend>
								<div class="form-group">
								@if($individual->risk_plans->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Plan Date Date</div>
										<div class="col-sm-3">Plan Type</div>
									</div>
									@foreach($individual->risk_plans AS $risk_plan)
										<div class="row">
											<div class="col-sm-3">{{isset($risk_plan->plan_date) ? $risk_plan->plan_date->format('m/d/Y') : ''}}</div>
											<div class="col-sm-3">{{$risk_plan->plan_type}}</div>
										</div>
									@endforeach
								@else
									<div><strong class="text-warning">No Risk Plans Available</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Medication Assistance</legend>
								<div class="form-group">
									<label for="medication_responsibility">Per the current PCISP, what is QRL’s medication administration responsibility?</label><br />
									<strong>{{nl2br($quarterly_summary->medication_responsibility)}}</strong>
								</div>
							</fieldset>
							<fieldset>
								<legend>Medication Started</legend>
								<div class="form-group">
								@if($individual->medications->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Medication Name</div>
										<div class="col-sm-3">Dosage</div>
										<div class="col-sm-3">Type</div>
									</div>
									@foreach($individual->medications AS $medication)
										<div class="row">
											<div class="col-sm-3">{{$medication->name}}</div>
											<div class="col-sm-3">{{$medication->dosage}}</div>
											<div class="col-sm-3">{{$medication->type}}</div>
										</div>
									@endforeach
								@else
									<div><strong class="text-warning">No Medications Started</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Medication Stopped</legend>
								<div class="form-group">
								@if($individual->retired_medications->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Medication Name</div>
										<div class="col-sm-3">Dosage</div>
										<div class="col-sm-3">Type</div>
									</div>
									@foreach($individual->retired_medications AS $medication)
										<div class="row">
											<div class="col-sm-3">{{$medication->name}}</div>
											<div class="col-sm-3">{{$medication->dosage}}</div>
											<div class="col-sm-3">{{$medication->type}}</div>
										</div>
									@endforeach
								@else
									<div><strong class="text-warning">No Medications Stopped</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Seizures</legend>
								<div class="form-group">
								@if($individual->seizures->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Seizure Date</div>
										<div class="col-sm-3">Follow up needed</div>
										<div class="col-sm-3">Notes</div>
									</div>
									@foreach($individual->seizures AS $seizure)
										<div class="row">
											<div class="col-sm-3">{{isset($seizure->seizure_date) ? $seizure->seizure_date->format('m/d/Y') : ''}}</div>
											<div class="col-sm-3">{{$medication->follow_up_needed}}</div>
											<div class="col-sm-3">{{nl2br($medication->additional_note)}}</div>
										</div>
									@endforeach
								@else
									<div><strong class="text-warning">No Medications Stopped</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Support Team Concerns</legend>
								<div class="form-group">
								@if($individual->support_team->count() > 0)
									<div class="row text-inverse">
										<div class="col-sm-3">Name</div>
										<div class="col-sm-3">Type</div>
										<div class="col-sm-3">Concern</div>
										<div class="col-sm-3">Concern Date</div>
									</div>
									@foreach($individual->support_team AS $support_member)
										@foreach($support_member->concerns AS $concern)
											<div class="row">
												<div class="col-sm-3">{{$support_member->full_name}}</div>
												<div class="col-sm-3">{{$support_member->member_type}}</div>
												<div class="col-sm-3">{{nl2br($concern->concern)}}</div>
												<div class="col-sm-3">{{$concern->date_of_concern->format('m/d/Y')}}</div>
											</div>
										@endforeach
									@endforeach
								@else
									<div><strong class="text-warning">No Support Team</strong></div>
								@endif
								</div>
							</fieldset>
							<fieldset>
								<legend>Additional Notes</legend>
								<div class="form-group">
									<label for="additional_notes">Additional comments, concerns, or issues associated with the site (include behavioral, medical, work, family):</label><br />
									<strong>{{nl2br($quarterly_summary->additional_notes)}}</strong>
								</div>
							</fieldset>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop