@extends('layouts.master.master')

@section('title', 'Quarterly Summary')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Quarterly Summary Add</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Quarterly Service Summary for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="date_complete">Date Quarterly Service Summary Performed</label>
									{!! Form::text('date_complete', isset($quarterly_summary->date_complete) ? $quarterly_summary->date_complete->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="year">Year</label>
									{!! Form::select('year', $years, $quarterly_summary->year ? $quarterly_summary->year : date('Y'), ['class' => 'form-control', 'placeholder' => 'Select Year']) !!}
								</div>
								<div class="form-group">
									<label for="quarter">Quarter:</label>
									{!! Form::select('quarter', $quarters, $quarterly_summary->quarter, ['class' => 'form-control', 'placeholder' => 'Select Quarter']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Services Provider By Month (select all)</legend>
								<div class="form-group">
									<label for="month_one_services">Month 1:</label>
									{!! Form::select('month_one_services[]', $services->pluck('code', 'id'), $quarterly_summary->month_one_services->pluck('id'), ['class' => 'form-control multiple-select', 'multiple' => '']) !!}
								</div>
								<div class="form-group">
									<label for="month_two_services">Month 2:</label>
									{!! Form::select('month_two_services[]', $services->pluck('code', 'id'), $quarterly_summary->month_two_services->pluck('id'), ['class' => 'form-control multiple-select', 'multiple' => '']) !!}
								</div>
								<div class="form-group">
									<label for="month_three_services">Month 3:</label>
									{!! Form::select('month_three_services[]', $services->pluck('code', 'id'), $quarterly_summary->month_three_services->pluck('id'), ['class' => 'form-control multiple-select', 'multiple' => '']) !!}
								</div>
								<div class="form-group">
									<label for="service_issues">List any issues related to service provided. This would include the need for a BMR, change in services provided, etc...:</label>
									{!! Form::textarea('service_issues', $quarterly_summary->service_issues, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>NOA | PCISP | BSP</legend>
								<div class="form-group">
									<label for="noa_start_date">NOA Start Date:</label>
									{!! Form::text('noa_start_date', isset($quarterly_summary->noa_start_date) ? $quarterly_summary->noa_start_date->format('m/d/Y') : NULL, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="noa_end_date">NOA End Date:</label>
									{!! Form::text('noa_end_date', isset($quarterly_summary->noa_end_date) ? $quarterly_summary->noa_end_date->format('m/d/Y') : NULL, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="noa_reflect">Does NOA reflect services provided?</label>
									{!! Form::select('noa_reflect', $yes_no, $quarterly_summary->noa_reflect, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="pcisp_date">Current PCSIP Date:</label>
									{!! Form::text('pcisp_date', isset($quarterly_summary->pcisp_date) ? $quarterly_summary->pcisp_date->format('m/d/Y') : NULL, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="bsp_date">Current BSP Date:</label>
									{!! Form::text('bsp_date', isset($quarterly_summary->bsp_date) ? $quarterly_summary->bsp_date->format('m/d/Y') : NULL, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Transportation Service</legend>
								<div class="form-group">
									<label for="transportation_utilized">Is transportation being utilized?</label>
									{!! Form::select('transportation_utilized', $yes_no, $quarterly_summary->transportation_utilized, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="transportation_noa">Is transportation on the NOA?</label>
									{!! Form::select('transportation_noa', $yes_no, $quarterly_summary->transportation_noa, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="transportation_type">Explain type of transportation (i.e. Mets/Taxi/Staff):</label>
									{!! Form::text('transportation_type', $quarterly_summary->transportation_type, ['class' => 'form-control', 'placeholder' => 'Enter Type']) !!}
								</div>
								<div class="form-group">
									<label for="transportation_logs">Transportation logs utilized?</label>
									{!! Form::select('transportation_logs', $yes_no, $quarterly_summary->transportation_logs, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Environmental</legend>
								<div class="form-group">
									<label for="five_day_med">Is Agency responsible per the PCISP?</label>
									{!! Form::select('five_day_med', $yes_no, $quarterly_summary->five_day_med, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="fire_extinguisher">Fire extinguisher in green and initialed:</label>
									{!! Form::select('fire_extinguisher', $yes_no, $quarterly_summary->fire_extinguisher, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="evac_posted">Evacuation route posted:</label>
									{!! Form::select('evac_posted', $yes_no, $quarterly_summary->evac_posted, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="smoke_detector">Smoke detector tested:</label>
									{!! Form::select('smoke_detector', $yes_no, $quarterly_summary->smoke_detector, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="carbon_detector">Carbon monoxide detector tested:</label>
									{!! Form::select('carbon_detector', $yes_no, $quarterly_summary->carbon_detector, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="adaptive_equipment">Adaptive equipment (i.e. utensils/wheelchair/ shower chair):</label>
									{!! Form::select('adaptive_equipment', $yes_no, $quarterly_summary->adaptive_equipment, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="equipment_issues">Any issues or concerns with equipment?</label>
									{!! Form::textarea('equipment_issues', $quarterly_summary->equipment_issues, ['class' => 'form-control', 'placeholder' => 'Enter Equipment Issues']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Community (see CHIO goals if applicable)</legend>
								<div class="form-group">
									<label for="community_events">Any special events or activities participated in this quarter?</label>
									{!! Form::select('community_events', $yes_no, $quarterly_summary->community_events, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="community_assessment">Assessment of Community Activities: (Where did the consumer go and is the consumer benefiting from these community experiences, what changes should be made to their current community involvement):</label>
									{!! Form::textarea('community_assessment', $quarterly_summary->community_assessment, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Wellness and Health Coordination</legend>
								<div class="form-group">
									<label for="wellness_utilized">Wellness service utilized?</label>
									{!! Form::select('wellness_utilized', $yes_no, $quarterly_summary->wellness_utilized, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="medical_concerns">Describe any other significant Medical Concerns / Issues / Changes (Were any appointments missed, therapies, prior authorizations, or health care professional needed, etc…)</label>
									{!! Form::textarea('medical_concerns', $quarterly_summary->medical_concerns, ['class' => 'form-control', 'placeholder' => 'Enter Equipment Issues']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Risk Plans</legend>
							</fieldset>
							<fieldset>
								<legend>Medication</legend>
								<div class="form-group">
									<label for="medication_responsibility">Per the current PCISP, what is Agency's medication administration responsibility?</label>
									{!! Form::textarea('medication_responsibility', $quarterly_summary->medication_responsibility, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Additional Notes</legend>
								<div class="form-group">
									<label for="additional_notes">Additional comments, concerns, or issues associated with the site (include behavioral, medical, work, family):</label>
									{!! Form::textarea('additional_notes', $quarterly_summary->additional_notes, ['class' => 'form-control', 'placeholder' => 'Enter Text']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Summary</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop