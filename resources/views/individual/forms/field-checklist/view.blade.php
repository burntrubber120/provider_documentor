@extends('layouts.master.master')

@section('title', 'FC Checklist')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Field Coordinator Checklist</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">
		{{ $individual->first_name }} {{ $individual->last_name }} &mdash; Field Coordinator Checklist
		</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel">
					<div class="panel-body f-s-14">
						<fieldset>
							<legend>
								<strong>Date Field Checklist Performed:</strong> {{isset($field_checklist->date_complete) ? $field_checklist->date_complete->format('m/d/Y') : ''}}
							</legend>
						</fieldset>
						<fieldset>
							<legend>Red Books (items to check):</legend>
							
							<div class="form-group">
								<label for="receipt_match">Money Count: </label>
								<strong>${{$field_checklist->money_count}}</strong>
							</div>
							<div class="form-group">
								<label for="receipt_match">Receipts and Log Match</label>
								<strong>{{$field_checklist->receipt_match}}</strong>
							</div>
							
						</fieldset>
						<fieldset>
							<legend>Black Books (items to check):</legend>
							<div class="form-group">
								<label for="review_done">Count the controlled medications:</label>
								<strong>{{$field_checklist->controlled_med_count}}</strong>
							</div>
							<div class="form-group">
								<label for="review_done">Does the count match the count sheet?</label>
								<strong>{{$field_checklist->controlled_med_count_match}}</strong>
							</div>
							<div class="form-group">
								<label for="review_done">Buddy Checklist (completed each shift):</label>
								<strong>{{$field_checklist->buddy_checklist}}</strong>
							</div>
						</fieldset>
						<fieldset>
							<legend>General Paperwork (is there enough):</legend>
							<div class="form-group">
								<label for="chio_notes">CHIO Notes</label>
								<strong>{{$field_checklist->chio_notes}}</strong>
							</div>
							<div class="form-group">
								<label for="time_sheets">Time Sheets</label>
								<strong>{{$field_checklist->time_sheets}}</strong>
							</div>
							<div class="form-group">
								<label for="transportation_logs">Transportation Logs:</label>
								<strong>{{$field_checklist->transportation_logs}}</strong>
							</div>
						</fieldset>
						<fieldset>
							<legend>Supplies for Home Needed:</legend>
							<div class="form-group">
								<label for="cleaning">Cleaning Supplies</label>
								<strong>{{$field_checklist->cleaning}}</strong>
							</div>
							<div class="form-group">
								<label for="office">Office Supplies (tape, paper, batteries, light bulbs, file cabinet):</label>
								<strong>{{$field_checklist->office}}</strong>
							</div>
							<div class="form-group">
								<label for="medical">Medical Supplies (cups, gloves, med box, lock):</label>
								<strong>{{$field_checklist->medical}}</strong>
							</div>
							<div class="form-group">
								<label for="bus_passes">Bus Passes:</label>
								<strong>{{$field_checklist->bus_passes}}</strong>
							</div>
						</fieldset>
						<fieldset>
							<legend>Medications:</legend>
							<div class="form-group">
								<label for="five_day_med">Are there 5 days of medcations present?</label>
								<strong>{{$field_checklist->five_day_med}}</strong>
							</div>
							<div class="form-group">
								<label for="check_dates">Check dates & pull out of date medications.</label>
								<strong>{{$field_checklist->check_dates}}</strong>
							</div>
							<div class="form-group">
								<label for="prn_reorder">Do any PRN's need reordered?</label>
								<strong>{{$field_checklist->prn_reorder}}</strong>
							</div>
							<div class="form-group">
								<label for="double_locked">Are medications double locked?</label>
								<strong>{{$field_checklist->double_locked}}</strong>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop