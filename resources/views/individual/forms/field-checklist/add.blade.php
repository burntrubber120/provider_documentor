@extends('layouts.master.master')

@section('title', 'FC Checklist')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Field Checklist Add</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Field Coordinator Checklist for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="date_complete">Date Field Checklist Performed:</label>
									{!! Form::text('date_complete', isset($field_checklist->date_complete) ? $field_checklist->date_complete->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<legend>Red Books (items to check):</legend>
								<label for="money_count">Money Count:</label>
								<div class="input-group p-b-10">
									<span class="input-group-addon">$</span>
									{!! Form::text('money_count', $field_checklist->money_count, ['class' => 'form-control', 'placeholder' => 'Enter Value']) !!}
								</div>
								<div class="form-group">
									<label for="receipt_match">Receipts and Log Match</label>
									{!! Form::select('receipt_match', $yes_no, $field_checklist->receipt_match, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="review_done">Reviews are done:</label>
									{!! Form::select('review_done', $yes_no, $field_checklist->review_done, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Black Books (items to check):</legend>
								
								<label for="controlled_med_count">Count the controlled medications:</label>
								<div class="input-group p-b-10">
									<span class="input-group-addon">#</span>
									{!! Form::text('controlled_med_count', $field_checklist->controlled_med_count, ['class' => 'form-control', 'placeholder' => 'Enter Count']) !!}
								</div>
								
								<div class="form-group">
									<label for="controlled_med_count_match">Does the count match the count sheet?</label>
									{!! Form::select('controlled_med_count_match', $yes_no, $field_checklist->controlled_med_count_match, ['class' => 'form-control', 'placeholder' => 'Enter Count']) !!}
								</div>
								<div class="form-group">
									<label for="buddy_checklist">Buddy Checklist (completed each shift):</label>
									{!! Form::select('buddy_checklist', $yes_no, $field_checklist->buddy_checklist, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>General Paperwork (is there enough):</legend>
								<div class="form-group">
									<label for="chio_notes">CHIO Notes</label>
									{!! Form::select('chio_notes', $yes_no, $field_checklist->chio_notes, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="time_sheets">Time Sheets</label>
									{!! Form::select('time_sheets', $yes_no, $field_checklist->time_sheets, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="transportation_logs">Transportation Logs:</label>
									{!! Form::select('transportation_logs', $yes_no, $field_checklist->transportation_logs, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Supplies for Home Needed:</legend>
								<div class="form-group">
									<label for="cleaning">Cleaning Supplies</label>
									{!! Form::select('cleaning', $yes_no, $field_checklist->cleaning, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="office">Office Supplies (tape, paper, batteries, light bulbs, file cabinet):</label>
									{!! Form::select('office', $yes_no, $field_checklist->office, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="medical">Medical Supplies (cups, gloves, med box, lock):</label>
									{!! Form::select('medical', $yes_no, $field_checklist->medical, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="bus_passes">Bus Passes:</label>
									{!! Form::select('bus_passes', $yes_no, $field_checklist->bus_passes, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<fieldset>
								<legend>Medications:</legend>
								<div class="form-group">
									<label for="five_day_med">Are there 5 days of medcations present?</label>
									{!! Form::select('five_day_med', $yes_no, $field_checklist->five_day_med, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="check_dates">Check dates & pull out of date medications.</label>
									{!! Form::select('check_dates', $yes_no, $field_checklist->check_dates, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="prn_reorder">Do any PRN's need reordered?</label>
									{!! Form::select('prn_reorder', $yes_no, $field_checklist->prn_reorder, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="double_locked">Are medications double locked?</label>
									{!! Form::select('double_locked', $yes_no, $field_checklist->double_locked, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Checklist</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop