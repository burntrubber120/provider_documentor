@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Medication Taken List</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$individual->first_name}} {{$individual->last_name}} <small>Medication Taken List</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				@if($individual->medication_logs->count() == 0)
					<div class="note note-warning"><h4>No Medication Taken</h4></div>
				@else
					<table class="table table-striped table-bordered table-manage">
						<thead>
							<tr>
								<th class="collapse">&nbsp;</th>
								<th>Medication</th>
								<th>Dosage</th>
								<th>Administered By</th>
								<th>Administered</th>
								<th>Options</th>
							</tr>	
						</thead>
						<tbody>
							@foreach($individual->medication_logs AS $medication_log)
							
							<tr>
								<td class="collapse">&nbsp;</td>
								<td>{{ $medication_log->medication->name }}</td>
								<td>{{ $medication_log->medication->dosage }}</td>
								<td>{{ $medication_log->user->first_name }} {{ $medication_log->user->last_name }}</td>
								<td>{{ $medication_log->administered_dt->format('m/d/Y h:i A') }}</td>
								<td><a href="./remove/{{encrypt($medication_log->medication->id)}}/{{encrypt($medication_log->id)}}" class="btn btn-danger btn-sm areYouSure">Remove Log</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@endif
				</table>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop