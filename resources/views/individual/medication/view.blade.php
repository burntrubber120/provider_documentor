@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Medication View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}'s Medication</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Current Medications</h4>
					</div>
					<div class="panel-body">
						@if($individual->medications->count() == 0)
							<div class="note note-warning"><h4>No Medication</h4></div>
						@else
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Dosage</th>
									<th>Type</th>
									<th>Number Doses</th>
									<th>Fill Date</th>
									<th>Note</th>
									<th>Options</th>
								</tr>	
							</thead>
							<tbody>
								@foreach($individual->medications AS $medication)
								<tr>
									<td>{{ $medication->name }}</td>
									<td>{{ $medication->dosage }}</td>
									<td>{{ $medication->type }}</td>
									<td>{{ $medication->number_doses }}</td>
									<td>{{ isset($medication->fill_date) ? $medication->fill_date->format('m/d/Y') : '' }}</td>
									<td>{!! nl2br($medication->note) !!}</td>
									<td>
										<a href="/individual/medication/{{encrypt($individual->id)}}/remove/{{encrypt($medication->id)}}" class="btn btn-xs btn-danger areYouSure">Retire</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@endif
						<div>
							<a class="btn btn-xs btn-success pull-right" id="show_add_medication_form">{{ old('name') !== null ? 'Cancel' : 'Add Medication' }}</a>
						</div>
						<div class="{{ old('name') !== null ? '' : 'collapse' }}">
							<form method="POST" action="./add">
								<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
								<fieldset>
									<legend>Add Medication</legend>
									<div class="form-group">
										<label for="name">Medication Name</label>
										<input type="text" name="name" class="form-control" placeholder="Medication Name" value="{{ old('name') }}" />
									</div>
									<div class="form-group">
										<label for="dosage">Dosage</label>
										<input type="text" name="dosage" class="form-control" placeholder="Dosage" value="{{ old('dosage') }}" />
									</div>
									<div class="form-group">
										<label for="number_doses">Number of Dose(s)</label>
										<input type="text" name="number_doses" class="form-control" placeholder="Number Dose(s)" value="{{ old('number_doses') }}" />
									</div>
									<div class="form-group">
										<label for="type">Type of Dosage (Pill, Powder, Syringe)</label>
										<input type="text" name="type" class="form-control" placeholder="Type" value="{{ old('type') }}" />
									</div>
									<div class="form-group">
										<label for="fill_date">Fill Date</label>
										<input type="text" name="fill_date" class="form-control calDatePicker" placeholder="Date" value="{{ old('fill_date') }}" />
									</div>
									<fieldset>
										<label for="schedule">Schedule</label>
										<div class="row text-center p-5">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-lg-1">
													<div>Sunday</div>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="sun" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</div>
												<div class="col-md-12 col-sm-12 col-lg-2">
													<div>Monday</div>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="mon" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</div>
												<div class="col-md-12 col-sm-12 col-lg-2">
													<div>Tuesday</div>
													
														<div class="input-group p-b-5">
															<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
															<span day="tue" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
														</div>
													
												</div>
												<div class="col-md-12 col-sm-12 col-lg-2">
													<div>Wednesday</div>
													
														<div class="input-group p-b-5">
															<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
															<span day="wed" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
														</div>
													
												</div>
												<div class="col-md-12 col-sm-12 col-lg-2">
													<div>Thursday</div>
													
														<div class="input-group p-b-5">
															<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
															<span day="thu" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
														</div>
													
												</div>
												<div class="col-md-12 col-sm-12 col-lg-2">
													<div>Friday</div>
													
														<div class="input-group p-b-5">
															<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
															<span day="fri" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
														</div>
													
												</div>
												<div class="col-md-12 col-sm-12 col-lg-1">
													<div>Saturday</div>
													
														<div class="input-group p-b-5">
															<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
															<span day="sat" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
														</div>
													
												</div>
											</div>
										</div>
									</fieldset>
									<div class="form-group">
										<label for="note">Note</label>
										<textarea name="note" class="form-control" placeholder="Note" >{{ old('note') }}</textarea>
									</div>
									
									<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Medication</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title">Retired Medications</h4>
					</div>
					<div class="panel-body">
						@if($individual->retired_medications->count() == 0)
							<div class="note note-warning"><h4>No Retired Medication</h4></div>
						@else
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Dosage</th>
									<th>Type</th>
									<th>Number Doses</th>
									<th>Fill Date</th>
									<th>Times Taken</th>
								</tr>
							</thead>
							<tbody>
								@foreach($individual->retired_medications AS $medication)
								<tr>
									<td>{{ $medication->name }}</td>
									<td>{{ $medication->dosage }}</td>
									<td>{{ $medication->type }}</td>
									<td>{{ $medication->number_doses }}</td>
									<td>{{ isset($medication->fill_date) ? $medication->fill_date->format('m/d/Y') : '' }}</td>
									<td>{{ $medication->logs->count() }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@endif
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- end #content -->
	<script>
		$('#show_add_medication_form').click(function () {
			
			if($(this).html() == 'Add Medication') {
				$(this).html('Cancel');
				$(this).addClass('btn-danger');
				$(this).removeClass('btn-success');
				$(this).parent().next().toggle('slow');
			}
			else {
				$(this).html('Add Medication');
				$(this).addClass('btn-success');
				$(this).removeClass('btn-danger');
				$(this).parent().next().toggle('slow');
			}
		});
		$('.add_schedule').click(function () {
			var time = $(this).parent().children(':input').val();
			var day = $(this).attr('day');
			$(this).parent().parent().append('<div class="input-group"><input type="text" readonly="readonly" name="' + day + '_schedule[]" class="form-control" value="' + time + '"/><span class="input-group-addon remove_schedule"><i class="fa fa-times"></i></span></div>');
		});
		$(document).on('click', '.remove_schedule', function() {
			$(this).parent().remove();
		});
	</script>
@stop