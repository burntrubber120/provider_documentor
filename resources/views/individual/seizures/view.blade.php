@extends('layouts.master.master')

@section('title', 'Seizure')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Seizure View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}'s Seizure</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body f-s-14">
							<fieldset>
								<div class="form-group">
									<label for="seizure_date">Date of Seizure:</label>
									<strong>{{isset($seizure->seizure_date) ? $seizure->seizure_date->format('m/d/Y') : ''}}</strong>
								</div>
								<div class="form-group">
									<label for="follow_up_needed">Is a follow-up needed?</label>
									<strong>{{$seizure->follow_up_needed}}</strong>
								</div>
								<div class="form-group">
									<label for="additional_note">Additional Information</label><br />
									<strong>{{nl2br($seizure->additional_note)}}</strong>
								</div>
							</fieldset>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
	
@stop