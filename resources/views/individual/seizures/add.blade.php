@extends('layouts.master.master')

@section('title', 'Seizure')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Seizure Add</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Seizure Incident for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="seizure_date">Date of Seizure:</label>
									{!! Form::text('seizure_date', isset($seizure->seizure_date) ? $seizure->seizure_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="follow_up_needed">Is a follow-up needed?</label>
									{!! Form::select('follow_up_needed', $yes_no, $seizure->follow_up_needed, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="additional_note">Additional Information</label>
									{!! Form::textarea('additional_note', $seizure->additional_note, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Incident</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop