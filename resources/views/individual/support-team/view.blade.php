@extends('layouts.master.master')

@section('title', 'Support Team')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Support Team View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $support_team->first_name }} {{ $support_team->last_name }}</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
							<div class="form-group">
									<label for="first_name">First Name:</label>
									<strong>{{$support_team->first_name}}</strong>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name:</label>
									<strong>{{$support_team->last_name}}</strong>
								</div>
								<div class="form-group">
									<label for="member_type">Support Type:</label>
									<strong>{{$support_team->member_type}}</strong>
								</div>
								<div class="form-group">
									<label for="email">Email Address:</label>
									<strong>{{$support_team->email}}</strong>
								</div>
								<div class="form-group">
									<label for="phone">Phone Number:</label>
									<strong>{{$support_team->phone}}</strong>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->	
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-heading">
					Past Concerns
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Date of concern</th>
								<th>Concern</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($support_team->concerns AS $concern)
								<tr>
									<td>{{$concern->date_of_concern->format('m/d/Y')}}</td>
									<td>{{$concern->concern}} {{$concern->concern}}</td>
									<td class="text-center">
										@if(Auth::user()->hasGroup('provider', 'Manager'))
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/team-concern/{{encrypt($support_team->id)}}/remove/{{encrypt($concern->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
					</div>
				</div>
				<!-- end panel -->
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-heading">
					Add Concern
					</div>
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
							<div class="form-group">
									<label for="date_of_concern">Date of concern:</label>
									{!! Form::text('date_of_concern', isset($concern->date_of_concern) ? $concern->date_of_concern->format('m/d/Y') : '', ['class' => 'form-control calDatePicker', 'placeholder' => 'Select Date']) !!}
								</div>
								<div class="form-group">
									<label for="concern">Concern:</label>
									{!! Form::textarea('concern', $concern->concern, ['class' => 'form-control', 'placeholder' => 'Enter Concern']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Concern</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->			
		</div>
	</div>
	<!-- end #content -->

@stop