@extends('layouts.master.master')

@section('title', 'Suport Team')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Support Team Add</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Support Team for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
							<div class="form-group">
									<label for="first_name">First Name:</label>
									{!! Form::text('first_name', $support_team->first_name, ['class' => 'form-control', 'placeholder' => 'Enter First Name']) !!}
								</div>
								<div class="form-group">
									<label for="last_name">Last Name:</label>
									{!! Form::text('last_name', $support_team->last_name, ['class' => 'form-control', 'placeholder' => 'Enter Last Name']) !!}
								</div>
								<div class="form-group">
									<label for="member_type">Support Type:</label>
									{!! Form::select('member_type', $member_types, $support_team->member_type, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="email">Email Address:</label>
									{!! Form::text('email', $support_team->email, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) !!}
								</div>
								<div class="form-group">
									<label for="phone">Phone Number:</label>
									{!! Form::text('phone', $support_team->phone, ['class' => 'form-control', 'placeholder' => 'Enter Phone']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Member</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop