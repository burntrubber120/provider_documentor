@extends('layouts.master.master')

@section('title', 'Graph')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Dashboard</a></li>
			<li><a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></li>
            <li>View Graph</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Add {{$graph->graph_title}} for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="data_date">Date:</label>
									{!! Form::text('data_date', date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
                                <div class="form-group {{$graph->item_1 == '' ? 'collapse' : ''}}">
									<label for="data_1">{{$graph->item_1}}:</label>
									{!! Form::text('data_1', NULL, ['class' => 'form-control', 'placeholder' => 'Label 1']) !!}
								</div>
                                <div class="form-group {{$graph->item_2 == '' ? 'collapse' : ''}}">
									<label for="data_2">{{$graph->item_2}}:</label>
									{!! Form::text('data_2', NULL, ['class' => 'form-control', 'placeholder' => 'Label 2']) !!}
								</div>
                                <div class="form-group {{$graph->item_3 == '' ? 'collapse' : ''}}">
									<label for="data_3">{{$graph->item_3}}:</label>
									{!! Form::text('data_3', NULL, ['class' => 'form-control', 'placeholder' => 'Label 3']) !!}
								</div>
                                <div class="form-group {{$graph->item_4 == '' ? 'collapse' : ''}}">
									<label for="data_4">{{$graph->item_4}}:</label>
									{!! Form::text('data_4', NULL, ['class' => 'form-control', 'placeholder' => 'Label 4']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Data</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
                <!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<div>
                            <input type="hidden" id="graph_title" value="{{$graph->graph_title}}" />
                            <input type="hidden" id="item_labels" value="{{$graph->item_labels}}" />
                            <input type="hidden" id="x_axis_labels" value="{{$x_axis_labels}}" />
                            <input type="hidden" id="item_1" value="{{$data_1}}" />
                            <input type="hidden" id="item_2" value="{{$data_2}}" />
                            <input type="hidden" id="item_3" value="{{$data_3}}" />
                            <input type="hidden" id="item_4" value="{{$data_4}}" />
                        </div>
                        <div id="chart" class="width-full height-md"></div>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->
            <div class="col-md-6">
				

                <!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<div>Data</div>
                        <table class="table table-manage table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>{{$graph->item_1}}</th>
                                    <th class="{{$graph->item_2 == '' ? 'collapse' : ''}}">{{$graph->item_2}}</th>
                                    <th class="{{$graph->item_3 == '' ? 'collapse' : ''}}">{{$graph->item_3}}</th>
                                    <th class="{{$graph->item_4 == '' ? 'collapse' : ''}}">{{$graph->item_4}}</th>
                                    <th class="text-center">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($graph->graph_data AS $data)
                                <tr>
                                    <td>{{$data->data_date->format('m/d/Y')}}</td>
                                    <td>{{$data->data_1}}</td>
                                    <td class="{{$graph->item_2 == '' ? 'collapse' : ''}}">{{$data->data_2}}</td>
                                    <td class="{{$graph->item_3 == '' ? 'collapse' : ''}}">{{$data->data_3}}</td>
                                    <td class="{{$graph->item_4 == '' ? 'collapse' : ''}}">{{$data->data_4}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-danger areYouSure" href="/individual/graph-data/{{encrypt($individual->id)}}/remove/{{encrypt($graph->id)}}/{{$data->id}}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>                        
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->
		</div>
	</div>
	<!-- end #content -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
    <script src="/assets/plugins/nvd3/build/nv.d3.js"></script>
<script>
$(document).ready(function () {
    var item_1, item_2, item_3, item_4;
    var graph_title = $('#graph_title').val();
    var item_labels = JSON.parse($('#item_labels').val());
    var x_axis_labels = JSON.parse($('#x_axis_labels').val());
        item_1 = JSON.parse($('#item_1').val());
    if($('item_3').val() != '')
        item_2 = JSON.parse($('#item_2').val());
    if($('item_3').val() != '')
        item_3 = JSON.parse($('#item_3').val());
    if($('item_4').val() != '')
        item_4 = JSON.parse($('#item_4').val());
    handleLineChart(graph_title, item_labels, x_axis_labels, item_1, item_2, item_3, item_4)
});

var blue		= '#348fe2',
    blueLight	= '#5da5e8',
    blueDark	= '#1993E4',
    aqua		= '#49b6d6',
    aquaLight	= '#6dc5de',
    aquaDark	= '#3a92ab',
    green		= '#00acac',
    greenLight	= '#33bdbd',
    greenDark	= '#008a8a',
    orange		= '#f59c1a',
    orangeLight	= '#f7b048',
    orangeDark	= '#c47d15',
    dark		= '#2d353c',
    grey		= '#b6c2c9',
    purple		= '#727cb6',
    purpleLight	= '#8e96c5',
    purpleDark	= '#5b6392',
    red         = '#ff5b57';


var handleLineChart = function(graph_title, item_labels, x_axis_labels, item_1, item_2, item_3, item_4) {
    "use strict";
    
    nv.addGraph(function() {
        
        var line_1 = [], line_2 = [], line_3 = [], line_4 = [], lineChartData = [];

        for (var i = 0; i < x_axis_labels.length; i++) {
            line_1.push({x: x_axis_labels[i], y: item_1[i] });
            
            if(item_2.length > 0 && item_2[i] != '' && item_2[i] != null && item_2[i] != undefined )
                line_2.push({x: x_axis_labels[i], y: item_2[i]});
            
            if(item_3.length > 0 && item_3[i] != '' && item_3[i] != null && item_3[i] != undefined )
                line_3.push({x: x_axis_labels[i], y: item_3[i]});
            
            if(item_4.length > 0 && item_4[i] != '' && item_4[i] != null && item_4[i] != undefined )
                line_4.push({x: x_axis_labels[i], y: item_4[i]});
            
        }
        
        if(line_1.length > 0) 
            lineChartData.push({values: line_1, key: item_labels[0], color: green});
        
        if(line_2.length > 0)    
            lineChartData.push({values: line_2, key: item_labels[1], color: blue});
        
        if(line_3.length > 0)     
            lineChartData.push({values: line_3, key: item_labels[2], color: orange});
        
        if(line_4.length > 0)
            lineChartData.push({values: line_4, key: item_labels[3], color: purple});
        
        
        var lineChart = nv.models.lineChart()
            .options({
                transitionDuration: 300,
                useInteractiveGuideline: true
            });

        lineChart.xAxis
            .axisLabel('Date').tickFormat(function(d) {
                return d3.time.format('%m-%d-%y')(new Date(d))
            });

        lineChart.yAxis
            .axisLabel(graph_title)
            .tickFormat(function(d) {
                if (d == null) {
                    return 'N/A';
                }
                return d3.format(',.2f')(d);
            });

        d3.select('#chart').append('svg')
            .datum(lineChartData)
            .call(lineChart);

        nv.utils.windowResize(lineChart.update);

        return lineChart;
    });
};
</script>
@stop