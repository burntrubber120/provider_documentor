@extends('layouts.master.master')

@section('title', 'Graph')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Dashboard</a></li>
			<li><a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></li>
            <li>Add Graph</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Add Graph for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="plan_date">Graph Title:</label>
									{!! Form::text('graph_title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
								</div>
                                <div class="form-group">
									<label for="item_1">Graph Label 1:</label>
									{!! Form::text('item_1', NULL, ['class' => 'form-control', 'placeholder' => 'Label 1']) !!}
								</div>
                                <div class="form-group">
									<label for="item_2">Graph Label 2:</label>
									{!! Form::text('item_2', NULL, ['class' => 'form-control', 'placeholder' => 'Label 2']) !!}
								</div>
                                <div class="form-group">
									<label for="item_3">Graph Label 3:</label>
									{!! Form::text('item_3', NULL, ['class' => 'form-control', 'placeholder' => 'Label 3']) !!}
								</div>
                                <div class="form-group">
									<label for="item_4">Graph Label 4:</label>
									{!! Form::text('item_4', NULL, ['class' => 'form-control', 'placeholder' => 'Label 4']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Graph</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop