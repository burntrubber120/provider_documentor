@extends('layouts.master.master')

@section('title', 'Med Appointment')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Medical Appointment Add</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Medical Appointment for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="appointment_date">Date of Visit:</label>
									{!! Form::text('appointment_date', isset($medical_appointment->appointment_date) ? $medical_appointment->appointment_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="doctor_name">Name of Physician:</label>
									{!! Form::text('doctor_name', $medical_appointment->doctor_name, ['class' => 'form-control', 'placeholder' => 'Enter Physicians Name']) !!}
								</div>
								<div class="form-group">
									<label for="purpose_of_visit">Purpose of Visit</label>
									{!! Form::textarea('purpose_of_visit', $medical_appointment->purpose_of_visit, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
								<div class="form-group">
									<label for="follow_up_date">Follow Up Appointment:</label>
									{!! Form::text('follow_up_date', isset($medical_appointment->follow_up_date) ? $medical_appointment->follow_up_date->format('m/d/Y') : null, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Appointment</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop