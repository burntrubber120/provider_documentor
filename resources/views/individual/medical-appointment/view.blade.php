@extends('layouts.master.master')

@section('title', 'Med Appointment')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li>Goal/Strategy View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}'s Medical Appointment</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body f-s-14">
						<fieldset>
							<div class="form-group">
								<label for="appointment_date">Date of Visit:</label>
								<strong>{{isset($medical_appointment->appointment_date) ? $medical_appointment->appointment_date->format('m/d/Y') : ''}}</strong>
							</div>
							<div class="form-group">
								<label for="doctor_name">Name of Physician:</label>
								<strong>{{$medical_appointment->doctor_name}}</strong>
							</div>
							<div class="form-group">
								<label for="follow_up_date">Follow Up Appointment:</label>
								<strong>{{isset($medical_appointment->follow_up_date) ? $medical_appointment->follow_up_date->format('m/d/Y') : null}}</strong>
							</div>
							<div class="form-group">
								<label for="purpose_of_visit">Purpose of Visit</label><br />
								<strong>{{nl2br($medical_appointment->purpose_of_visit)}}</strong>
							</div>
							
						</fieldset>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop