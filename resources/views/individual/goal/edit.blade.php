@extends('layouts.master.master')

@section('title', 'Edit Med')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li>Medication View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Edit {{ $medication->name }}</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form method="POST" action="./add">
							<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
							<fieldset>
								<legend>Add Medication</legend>
								<div class="form-group">
									<label for="name">Medication Name</label>
									<input type="text" name="name" class="form-control" placeholder="Medication Name" value="{{ old('name', $medication->name) }}" required />
								</div>
								<div class="form-group">
									<label for="dosage">Dosage</label>
									<input type="text" name="dosage" class="form-control" placeholder="Dosage" value="{{ old('dosage', $medication->dosage) }}" required />
								</div>
								<div class="form-group">
									<label for="number_doses">Number of Dose(s)</label>
									<input type="text" name="number_doses" class="form-control" placeholder="Number Dose(s)" value="{{ old('number_doses', $medication->number_doses) }}" />
								</div>
								<div class="form-group">
									<label for="type">Type of Dosage (Pill, Powder, Syringe)</label>
									<input type="text" name="type" class="form-control" placeholder="Type" value="{{ old('type', $medication->type) }}" />
								</div>
								<div class="form-group">
									<label for="fill_date">Fill Date</label>
									<input type="text" name="fill_date" class="form-control calDatePicker" placeholder="Date" value="{{ old('fill_date', $medication->fill_date) }}" />
								</div>
								
									<label for="schedule">Schedule</label>
									
									<table class="table text-center table-bordered">
										<thead>
											<tr>
												<th>Sunday</th>
												<th>Monday</th>
												<th>Tuesday</th>
												<th>Wednesday</th>
												<th>Thursday</th>
												<th>Friday</th>
												<th>Saturday</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="sun" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="mon" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="tue" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="wed" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="thu" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="fri" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
												<td>
													<div class="input-group p-b-5">
														<input type="text" name="schedule" class="form-control timePicker schedule" value=""/>
														<span day="sat" class="input-group-addon add_schedule"><i class="fa fa-plus"></i></span>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								
								<div class="form-group">
									<label for="note">Note</label>
									<textarea name="note" class="form-control" placeholder="Note" >{{ old('note') }}</textarea>
								</div>
								
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Medication</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop