@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
            <li><a href="/">Dashboard</a></li>
            <li><a href="/individual/view/{{encrypt($individual->id)}}">{{ $individual->first_name }} {{ $individual->last_name }}</a></li>
            <li></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Individual <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Individual Add</legend>
								<div class="form-group">
									<label for="first_name">First Name</label>
									<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ old('first_name') }}"/>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ old('last_name') }}"/>
								</div>
								<div class="form-group">
									<label for="ssn">SSN</label>
									<input type="text" name="ssn" class="form-control" placeholder="SSN" value="{{ old('ssn') }}"/>
								</div>
								<div class="form-group">
									<label for="dob">Date of Birth</label>
									<input type="text" name="dob" class="form-control calDatePicker" placeholder="Date of Birth" value="{{ old('dob') }}"/>
								</div>
								
								<div class="form-group">
									<label for="height">Height</label>
									<input type="text" name="height" class="form-control" placeholder="Height" value="{{ old('height') }}"/>
								</div>
								<div class="form-group">
									<label for="weight">Weight</label>
									<input type="text" name="weight" class="form-control" placeholder="Weight" value="{{ old('weight') }}"/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ old('email') }}"/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone') }}"/>
								</div>
								<div class="form-group">
									<label for="dob">Date of death</label>
									<input type="text" name="dod" class="form-control calDatePicker" placeholder="Date of Death" value="{{ old('dob') }}"/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Add</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
$('#zip').mask('99999');
</script>
@stop