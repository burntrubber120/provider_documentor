@extends('layouts.master.master')

@section('title', 'Risk Plan')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Home</a></li>
			<li><a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></li>
            <li>Risk Plan</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{$action}} Risk Plan for <a href="/individual/view/{{encrypt($individual->id)}}">{{$individual->full_name}}</a></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<div class="form-group">
									<label for="plan_date">Plan Date:</label>
									{!! Form::text('plan_date', isset($risk_plan->plan_date) ? $risk_plan->plan_date->format('m/d/Y') : null, ['class' => 'form-control calDatePicker', 'placeholder' => 'Enter Date']) !!}
								</div>
								<div class="form-group">
									<label for="plan_type">Last Name:</label>
									{!! Form::select('plan_type', $plan_types, $risk_plan->plan_type, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}
								</div>
							</fieldset>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">{{$action}} Plan</button>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
<script>
</script>
@stop