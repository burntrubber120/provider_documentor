@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li>Goal/Strategy View</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}'s Goals</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Add Goal</h4>
					</div>
					<div class="panel-body">
						<div>
							<a class="btn btn-md btn-success" id="show_add_medication_form">{{ old('goal') !== null ? 'Cancel' : 'Add Goal' }}</a>
						</div>
						<div class="{{ old('goal') !== null ? '' : 'collapse' }}">
							<form method="POST" action="./add">
								<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
								<fieldset>
									<legend>Add Goal</legend>
									<div class="form-group">
										<label for="goal">Goal</label>
										<textarea type="text" name="goal" class="form-control" placeholder="Enter Goal">{{ old('goal') }}</textarea>
									</div>
										
									<button type="submit" class="btn btn-md btn-primary m-r-5">Add Goal</button>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
			@if($individual->goals->count() == 0)
				<div class="col-md-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h4 class="panel-title">Current Goals</h4>
						</div>
						<div class="panel-body">
							<div class="note note-warning"><h4>No Goals</h4></div>
						</div>
					</div>
				</div>
			@else
				@foreach($individual->goals AS $goal)
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">{{ $goal->goal }} <a href="/individual/goal/{{encrypt($individual->id)}}/remove/{{encrypt($goal->id)}}" class="btn btn-xs btn-danger pull-right areYouSure">Retire</a></h4> 
						</div>
						<div class="panel-body">
							
							@if($goal->strategies->count() > 0)
								<ol>
								@foreach($goal->strategies AS $k => $strategy)
									<li class="{{ $k % 2 == 0 ? 'bg-silver' : ''}}">{{ $strategy->strategy }}</li>
								@endforeach
								</ol>
							@else
								<div class="bg-warning">No Strategies</div>
							@endif
							<a class="btn btn-xs btn-primary strat_add_btn pull-right pull-bottom" id="{{encrypt($goal->id)}}">Add Strategy</a>							
						</div>
					</div>
				</div>
				@endforeach
			@endif
			<div class="col-md-12 collapse" id="strat_add_div">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Add Strategy</h4>
					</div>
					<div class="panel-body">
						<form method="POST" action="./strategy">
							<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
							<input type="hidden" name="goal_id" id="goal_id" value=""  />
							<fieldset>
								<div class="form-group">
									<label for="strategy">Strategy</label>
									<textarea type="text" id="strategy" name="strategy" class="form-control" placeholder="Enter Goal">{{ old('strategy') }}</textarea>
								</div>
									
								<button type="submit" class="btn btn-md btn-primary m-r-5">Add Strategy</button> <a id="strategy_cancel_form" class="btn btn-md btn-danger">Cancel</a>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- end #content -->
	<script>
		$('#show_add_medication_form').click(function () {
			
			if($(this).html() == 'Add Goal') {
				$(this).html('Cancel');
				$(this).addClass('btn-danger');
				$(this).removeClass('btn-success');
				$(this).parent().next().toggle('slow');
			}
			else {
				$(this).html('Add Goal');
				$(this).addClass('btn-success');
				$(this).removeClass('btn-danger');
				$(this).parent().next().toggle('slow');
			}
		});
		$('.strat_add_btn').click(function () {
			$(this).parent().parent().parent().parent().find('div.panel-body').removeClass('bg-warning');
			$(this).parent().addClass('bg-warning');
			$('#strat_add_div').show('slow');
			$('#goal_id').val($(this).attr('id'));
			$('#strategy').focus();
		});
		$('#strategy_cancel_form').click(function () {
			$('#strat_add_div').hide('slow');
			$(this).parent().parent().parent().parent().parent().parent().find('div.panel-body').removeClass('bg-warning');
			$('#goal_id').val('');
		});
	</script>
@stop