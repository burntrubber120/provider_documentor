@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">House</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}</h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Individual Edit</legend>
								<div class="form-group">
									<label for="first_name">First Name</label>
									<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ old('first_name', $individual->first_name) }}"/>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ old('last_name', $individual->last_name) }}"/>
								</div>
								<div class="form-group">
									<label for="ssn">SSN</label>
									<input type="text" name="ssn" class="form-control" placeholder="SSN" value="{{ old('ssn', $individual->ssn != '' ? decrypt($individual->ssn) : '') }}"/>
								</div>
								<div class="form-group">
									<label for="dob">Date of Birth</label>
									<input type="text" name="dob" class="form-control calDatePicker" placeholder="Date of Birth" value="{{ old('dob', $individual->dob) }}"/>
								</div>
								
								<div class="form-group">
									<label for="height">Height</label>
									<input type="text" name="height" class="form-control" placeholder="Height" value="{{ old('height', $individual->height) }}"/>
								</div>
								<div class="form-group">
									<label for="weight">Weight</label>
									<input type="text" name="weight" class="form-control" placeholder="Weight" value="{{ old('weight', $individual->weight) }}"/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" id="email" class="form-control" placeholder="Email" value="{{ old('email', $individual->email) }}"/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone', $individual->phone) }}"/>
								</div>
								<div class="form-group">
									<label for="about_me">About Me:</label>
									<textarea name="about_me" class="form-control" placeholder="About Me">{{ old('about_me', $individual->about_me) }}</textarea>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="./{{encrypt($individual->id)}}/house" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>House Information</legend>
								<div class="form-group">
									<label for="house_id">Select House</label>
									<select name="house_id" class="form-control">
										<option value="">Please Select</option>
										@foreach($houses AS $house)
										<option value="{{ encrypt($house->id) }}" {{ isset($individual->current_house[0]->id) && $individual->current_house[0]->id == $house->id ? 'selected="selected"' : '' }}>
											{{ $house->nickname }} - {{ $house->address }}
										</option>
										@endforeach
									</select>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Update</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop