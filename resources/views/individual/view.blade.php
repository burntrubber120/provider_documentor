@extends('layouts.master.master')

@section('title', 'Individual')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Dashboard</a></li>
			<li>{{ $individual->first_name }} {{ $individual->last_name }}</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $individual->first_name }} {{ $individual->last_name }}</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Individual Information</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-4">
								<div class="row">
									<div class="col-sm-4">Name: </div><div class="col-sm-8">{{ $individual->first_name }} {{ $individual->last_name }}</div>
									<div class="col-sm-4">Email: </div><div class="col-sm-8">{{ isset($individual->email) ? $individual->email : "EMPTY" }}</div>
									<div class="col-sm-4">Phone: </div><div class="col-sm-8">{{ isset($individual->phone) ? $individual->phone : "EMPTY" }}</div>
									<div class="col-sm-4">DOB: </div><div class="col-sm-8">{{ isset($individual->dob) ? $individual->dob->format('m/d/Y') : "EMPTY" }}</div>
									<div class="col-sm-4">Height: </div><div class="col-sm-8">{{ isset($individual->height) ? $individual->height : "EMPTY" }}</div>
									<div class="col-sm-4">Weight: </div><div class="col-sm-8">{{ isset($individual->weight) ? $individual->weight : "EMPTY"}}</div>
								</div>
							</div>
							<div class="col-sm-8">
								<legend>About Me:</legend>
								<p>{!! nl2br($individual->about_me) !!}</p>
								
							</div>
						</div>
						<div class="row p-t-10">
							<div class="col-sm-12">
								<h4>House Information:</h4>
								<div>
									@if($individual->current_house->count() > 0)
										{{ $individual->current_house[0]->nickname }} <br />
										{{ $individual->current_house[0]->address }} {{ $individual->current_house[0]->city }}, {{ $individual->current_house[0]->state }} {{ $individual->current_house[0]->zip }}<br />
										{{ $individual->current_house[0]->phone }}
									@else
										<h4 class="bg-danger">No House</h4>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Medication Information
							@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
								<span class="pull-right"><a href="/individual/medication/{{encrypt($individual->id)}}/taken-report" class="btn btn-xs btn-warning">View All Taken Medications</a></span>
							@endif
						</h4>
					</div>
					<div class="panel-body">
						@if($individual->medications->count() == 0)
							<div class="note note-warning"><h4>No Medication</h4></div>
						@else
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Dosage</th>
									<th>Type</th>
									<th>Number Doses</th>
									<th>Day/Time</th>
									<th>Note</th>
								</tr>	
							</thead>
							<tbody>
								@foreach($individual->medications AS $medication)
								<tr>
									<td>{{ $medication->name }}</td>
									<td>{{ $medication->dosage }}</td>
									<td>{{ $medication->type }}</td>
									<td>{{ $medication->number_doses }}</td>
									<td>
										@foreach($medication->schedules AS $schedule)
											<span>{{ $schedule->week_day }}</span> <span>{{ date('h:i A', strtotime($schedule->time)) }}</span> <br />
										@endforeach
									</td>
									<td>{!! nl2br($medication->note) !!}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@endif
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a class="btn btn-success btn-sm" href="/individual/medication/{{encrypt($individual->id)}}/view">Manage Medication(s)</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Medical Appointments
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Date of Visit</th>
								<th>Doctor</th>
								<th>Follow-up Appointment</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->medical_appointments AS $medical_appointment)
								<tr>
									<td>{{$medical_appointment->appointment_date->format('m/d/Y')}}</td>
									<td>{{$medical_appointment->doctor_name}}</td>
									<td>{{isset($medical_appointment->follow_up_date) ? $medical_appointment->follow_up_date->format('m/d/Y') : 'No Follow-up Appointment'}}</td>
									<td class="text-center">
										<a class="btn btn-sm btn-success" href="/individual/medical-appointment/{{encrypt($individual->id)}}/view/{{encrypt($medical_appointment->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-warning" href="/individual/medical-appointment/{{encrypt($individual->id)}}/edit/{{encrypt($medical_appointment->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/medical-appointment/{{encrypt($individual->id)}}/remove/{{encrypt($medical_appointment->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/medical-appointment/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Medical Appointment</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">Goals/Strategies</h4>
					</div>
					<div class="panel-body">
						@if($individual->goals->count() == 0)
							<div class="note note-warning"><h4>No Goals</h4></div>
						@else
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Goal</th>
									<th>Strategy</th>
								</tr>	
							</thead>
							<tbody>
								@foreach($individual->goals AS $goal)
								<tr>
									<td>{{ $goal->goal }}</td>
									<td>@if($goal->strategies->count() > 0)
											<ol>
											@foreach($goal->strategies AS $strategy)
												<li>{{ $strategy->strategy }}</li>
											@endforeach
											</ol>
										@else
											<p class="bg-warning">No Strategy</p>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@endif
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a class="btn btn-success btn-sm" href="/individual/goal/{{encrypt($individual->id)}}/view">Manage Goal/Strategy(s)</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Support Team
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Name</th>
								<th>Type</th>
								<th>Email</th>
								<th>Recent Concern</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->support_team AS $support_member)
								<tr>
									<td>{{$support_member->full_name}}</td>
									<td>{{$support_member->member_type}}</td>
									<td>
										Email: {{$support_member->email}}<br />
										Phone: {{$support_member->phone}}									
									</td>
									<td>{{$support_member->concerns->count() > 0 ? $support_member->concerns->first()->concern : ''}}</td>
									<td class="text-center">
										<a class="btn btn-sm btn-success" href="/individual/support-team/{{encrypt($individual->id)}}/view/{{encrypt($support_member->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-warning" href="/individual/support-team/{{encrypt($individual->id)}}/edit/{{encrypt($support_member->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/support-team/{{encrypt($individual->id)}}/remove/{{encrypt($support_member->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/support-team/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Support Member</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Seizures
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Date of Seizure</th>
								<th>Follow-up Needed</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->seizures AS $seizure)
								<tr>
									<td>{{$seizure->seizure_date->format('m/d/Y')}}</td>
									<td>{{$seizure->follow_up_needed}}</td>
									<td class="text-center">
										<a class="btn btn-sm btn-success" href="/individual/seizures/{{encrypt($individual->id)}}/view/{{encrypt($seizure->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-warning" href="/individual/seizures/{{encrypt($individual->id)}}/edit/{{encrypt($seizure->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/seizures/{{encrypt($individual->id)}}/remove/{{encrypt($seizure->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/seizures/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Seizure Incident</a>
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Recent Service Note
							@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
								<span class="pull-right"><a href="/individual/casenote/{{encrypt($individual->id)}}/list" class="btn btn-xs btn-warning">View All Service Notes</a></span>
							@endif
						</h4>
					</div>
					<div class="panel-body">
						@if($individual->ten_casenotes->count() == 0)
							<div class="note note-warning"><h4>No Service Notes</h4></div>
						@else
                            @php $case_note = $individual->ten_casenotes->first() @endphp
                            <div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p><strong>User:</strong> {{ $case_note->user->first_name }} {{ $case_note->user->last_name }}</p>
                                        <p><strong>Start Date/Time:</strong> {{ $case_note->start_dt->format('m/d/Y h:i A') }}</p>
                                        <p><strong>End Date/Time:</strong> {{ $case_note->end_dt->format('m/d/Y h:i A') }}</p>
                                        <p><strong>Goal:</strong> {{isset($case_note->goal) ? $case_note->goal->goal : ''}}<p>
                                        <p><strong>Strategy:</strong> {{isset($case_note->strategy) ? $case_note->strategy->strategy : ''}}<p>
                                        <p><strong>Priority:</strong> {{$case_note->priority}}<p>
                                        @if(isset($case_note->priority) && $case_note->priority == 'Reportable Incident')
                                        <p><strong>Reportable Text:</strong> {{$case_note->priority}}<p>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <p><strong>Note Text:</strong></p>
                                        <p>{!! nl2br($case_note->case_note) !!}</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>
                                            <a class="btn btn-warning btn-sm" href="/individual/casenote/{{encrypt($individual->id)}}/edit/{{encrypt($case_note->id)}}"><i class="fa fa-pencil"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                
                            </div>
						@endif
						<a class="btn btn-success btn-sm" href="/individual/casenote/{{encrypt($individual->id)}}/add">Add Service Note</a>
						
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Field Coordinator Checklist 
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Date Complete</th>
								<th>User</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->field_checklists AS $checklist)
								<tr>
									<td>{{$checklist->date_complete->format('m/d/Y')}}</td>
									<td>{{$checklist->user->first_name}} {{$checklist->user->last_name}}</td>
									<td class="text-center">
										<a class="btn btn-sm btn-success" href="/individual/fc-checklist/{{encrypt($individual->id)}}/view/{{encrypt($checklist->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroup('provider', 'Manager'))
										<a class="btn btn-sm btn-warning" href="/individual/fc-checklist/{{encrypt($individual->id)}}/edit/{{encrypt($checklist->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/fc-checklist/{{encrypt($individual->id)}}/remove/{{encrypt($checklist->id)}}"><i class="fa fa-trash"></i></a>
										@endif
										
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/fc-checklist/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Field Coordinator Checklist</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Quarterly Service Summary
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Date Complete</th>
								<th>Yr/Qtr</th>
								<th>User</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->quarterly_summaries AS $summary)
								<tr>
									<td>{{$summary->date_complete->format('m/d/Y')}}</td>
									<td>{{$summary->year}}/{{$summary->quarter}}</td>
									<td>{{$summary->user->first_name}} {{$summary->user->last_name}}</td>
									<td class="text-center">
										<a class="btn btn-sm btn-success" href="/individual/quarterly-summary/{{encrypt($individual->id)}}/view/{{encrypt($summary->id)}}"><i class="fa fa-eye"></i></a>
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-warning" href="/individual/quarterly-summary/{{encrypt($individual->id)}}/edit/{{encrypt($summary->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/quarterly-summary/{{encrypt($individual->id)}}/remove/{{encrypt($summary->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/quarterly-summary/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Quarterly Service Summary</a>
						@endif
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Risk Plans
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Plan</th>
								<th>Date</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->risk_plans AS $risk_plan)
								<tr>
									<td>{{$risk_plan->plan_type}}</td>
									<td>{{$risk_plan->plan_date->format('m/d/Y')}}</td>
									<td class="text-center">
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-warning" href="/individual/risk-plan/{{encrypt($individual->id)}}/edit/{{encrypt($risk_plan->id)}}"><i class="fa fa-pencil"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/risk-plan/{{encrypt($individual->id)}}/remove/{{encrypt($risk_plan->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/risk-plan/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Risk Plan</a>
						@endif
					</div>
				</div>
                <div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							Graphs
						</h4>
					</div>
					<div class="panel-body">
						<table class="table table-bordered table-stripe">
							<tr>
								<th>Graph</th>
								<th>Last Data Entered</th>
								<th class="text-center">Options</th>
							</tr>
							@foreach($individual->graphs AS $graph)
								<tr>
									<td>{{$graph->graph_title}}</td>
									<td>{{$graph->graph_data->count() > 0 ? $graph->graph_data->last()->data_date->format('m/d/Y') : 'No Data Entered'}}</td>
									<td class="text-center">
										@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
										<a class="btn btn-sm btn-success" href="/individual/graph/{{encrypt($individual->id)}}/view/{{encrypt($graph->id)}}"><i class="fa fa-eye"></i></a>
										<a class="btn btn-sm btn-danger areYouSure" href="/individual/graph/{{encrypt($individual->id)}}/remove/{{encrypt($graph->id)}}"><i class="fa fa-trash"></i></a>
										@endif
									</td>
								</tr>
							@endforeach
							
						</table>
						@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
							<a href="/individual/graph/{{encrypt($individual->id)}}/add" class="btn btn-sm btn-success">Add Graph</a>
						@endif
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- end #content -->
	<script>
		$('#show_add_medication').click(function () {
			$('#add_medication_form').toggle();
			if($(this).html() == 'Add Medication') {
				$(this).html('Cancel');
				$(this).addClass('btn-danger');
				$(this).removeClass('btn-success');
			}
			else {
				$(this).html('Add Medication');
				$(this).addClass('btn-success');
				$(this).removeClass('btn-danger');
			}
		});
		
		$('#show_add_house').click(function () {
			$('#add_house_form').toggle();
			if($(this).html() == 'Add House') {
				$(this).html('Cancel');
				$(this).addClass('btn-danger');
				$(this).removeClass('btn-success');
			}
			else {
				$(this).html('Add House');
				$(this).addClass('btn-success');
				$(this).removeClass('btn-danger');
			}
		});
		$("#priority").change(function () {
			if($(this).val() == 'Reportable Incident') {
				$('#reportable_div').show();
			}
			else {
				$('#reportable_div').hide();
			}

		});
	</script>
@stop