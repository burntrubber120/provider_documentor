	<!-- begin #header -->
	<div id="header" class="header navbar navbar-default navbar-fixed-top navbar-inverse">
		<!-- begin container-fluid -->
		<div class="container-fluid">
			<!-- begin mobile sidebar expand / collapse button -->
			<div class="navbar-header">
				<a href="/" class="navbar-brand"><span class="navbar-logo"></span> DocuMentor</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end mobile sidebar expand / collapse button -->
			
			
			
			<!-- begin header navigation right -->
			<ul class="nav navbar-nav navbar-right">
				
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span> <b class="caret"></b>
					</a>
					<ul class="dropdown-menu animated fadeInLeft">
						<li class="arrow"></li>
						<li><a href="/my-profile">Setting</a></li>
						<li class="divider"></li>
						<li><a href="/logout">Log Out</a></li>
					</ul>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end container-fluid -->
	</div>
	<!-- end #header -->