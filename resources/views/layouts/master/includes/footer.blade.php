<!-- ================== BEGIN BASE JS ================== -->	
	<script src="/assets/plugins/jquery/jquery-1.9.1.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
  	<script src="/assets/plugins/lightbox/js/lightbox-2.6.min.js"></script>
	<script src="/assets/plugins/superbox/js/superbox.js"></script>	
	
	<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Buttons/js/buttons.colVis.min.js"></script>
	
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="/assets/plugins/masked-input/masked-input.min.js"></script>
	<script src="/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="/assets/plugins/password-indicator/js/password-indicator.js"></script>
	<script src="/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"></script>
	<script src="/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
	<script src="/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
	<script src="/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js"></script>
	<script src="/assets/plugins/jquery-tag-it/js/tag-it.min.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
    <script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="/assets/js/apps.min.js"></script>
	
	
	<!-- ================== END PAGE LEVEL JS ================== -->
	<div class="modal fade" id="are-you-sure">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Alert</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-danger m-b-0">
						<h4><i class="fa fa-info-circle"></i> Are you sure?</h4>
						<p></p>
					</div>
				</div>
				<div class="modal-footer text-center">
					<a href="javascript:;" class="btn btn-sm btn-danger" id="i_am_sure">Yes</a>
					<a href="javascript:;" class="btn btn-sm btn-info" data-dismiss="modal">No</a>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
            App.init();
            //TableManageDefault.init();
            $('.calDatePickerOpen').datepicker({
				todayHighlight: true,
				autoclose: true
			});
			$('.calDatePicker').datepicker({
				todayHighlight: true,
				autoclose: true,
				endDate: new Date(new Date().setDate(new Date().getDate()))
			});
			$('.calDateTimePicker').datetimepicker({
				maxDate: Date(),
				sideBySide: true
			});
			$('.timePicker').datetimepicker({
				format: 'LT'
			});
			$('.table-manage').each(function () {
				if($(this).length !== 0) {
					$(this).dataTable().fnDestroy();
					$(this).dataTable({
						dom: 'Bfrtip',
						buttons: [
							{ extend: 'csv', className: 'btn-sm' },
							{ extend: 'pdf', className: 'btn-sm' },
						],
						responsive: true
					});
				}
			});
			$(".multiple-select").select2({});
			var modal_href = '';
			$('.areYouSure').click(function(e) {
				e.preventDefault();
				$('#are-you-sure').modal();
				modal_href = $(this).attr('href');
			});
			$('#i_am_sure').click(function () {
				$('#are-you-sure').modal('hide');
				window.location = modal_href;
				modal_href = '';
			});
		});
	</script>