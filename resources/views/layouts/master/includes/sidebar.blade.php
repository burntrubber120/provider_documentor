<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
		<!-- begin sidebar nav -->
		<ul class="nav">
			<li class="nav-header"></li>
			<li>
				<a href="/">
					<i class="fa fa-dashboard"></i> 
					<span>Dashboard</span>
				</a>
			</li>
			@if(Auth::user()->hasGroupType('provider','Administrator'))
			<li class="has-sub">
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-users"></i> 
					<span>User Tools</span>
				</a>
				<ul class="sub-menu">
					<li><a href="/user/list">User List</a></li>
					<li><a href="/user/add">Add User</a></li>					
				</ul>
			</li>
			
			<li class="has-sub">
				
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-home"></i> 
					<span>House Tools</span>
				</a>
				<ul class="sub-menu">
					<li><a href="/house/list">House List</a></li>
						<li><a href="/house/add">Add House</a></li>
				</ul>
			</li>
			<li class="has-sub">
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-child"></i> 
					<span>Individual Tools</span>
				</a>
				<ul class="sub-menu">
					<li><a href="/individual/list">Individual List</a></li>
					<li><a href="/individual/add">Add Individual</a></li>
				</ul>
			</li>
			<li class="has-sub">
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-bar-chart"></i> 
					<span>Reports</span>
				</a>
				<ul class="sub-menu">
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<span>Case Note/Incident</span>
						</a>
						<ul class="sub-menu">
							<li><a href="/provider/report/case-notes/approval">Approve Case Note</a></li>
							<li><a href="/provider/report/case-notes/approved">Approved Case Note</a></li>
							<li><a href="/provider/report/case-notes/by-individual">Service Notes By Individual</a></li>
							<li><a href="/provider/report/case-notes/with-qol">Service Notes w/ QoL Data</a></li>
							<li><a href="/provider/report/case-notes/minor">Non Reportable Incident Notes</a></li>
							<li><a href="/provider/report/case-notes/major">Reportable Incident Notes</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<span>Medication</span>
						</a>
						<ul class="sub-menu">
							<li><a href="/provider/report/medication/missed-by-time">Missed by Time</a></li>
							<li><a href="/provider/report/medication/by-individual">By Individual</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<span>Training</span>
						</a>
						<ul class="sub-menu">
							<li><a href="/provider/report/training/by-employee">Training By Employee</a></li>
						</ul>
					</li>
                    <li class="has-sub">
						<a href="javascript:;">
							<b class="caret pull-right"></b>
							<span>Time Punches</span>
						</a>
						<ul class="sub-menu">
							<li><a href="/provider/report/time-punches/by-employee">Punches By Employee</a></li>
							<li><a href="/provider/report/time-punches/employee-hours">Hours By Employee</a></li>
						</ul>
					</li>
				</ul>
			</li>
			@endif
			<li class="has-sub">
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-book"></i> 
					<span>Training Tools</span>
				</a>
				<ul class="sub-menu">
					<li><a href="/training/list">Training List</a></li>
					@if(Auth::user()->hasGroupType('provider','Administrator'))
					<li><a href="/training/add">Add Training</a></li>				
					@endif	
				</ul>
			</li>
			@if(Auth::user()->hasGroupType('site','admin'))
			<li class="has-sub">
				<a href="javascript:;">
					<b class="caret pull-right"></b>
					<i class="fa fa-university"></i> 
					<span>Admin Tools</span>
				</a>
				<ul class="sub-menu">
					<li><a href="/site-admin/provider/list">Provider List</a></li>
				</ul>
			</li>
			@endif
            <div class="text-center p-t-10">
                @if(!Auth::user()->clocked_in_out || (Auth::user()->clocked_in_out && Auth::user()->clocked_in_out->punch == 'out'))
                    <a class="btn btn-sm btn-primary areYouSure clockInOut" href="/clock/in"><i class="fa fa-clock-o"></i> Clock In</a>
                @else
                    <a class="btn btn-sm btn-warning areYouSure clockInOut" href="/clock/out"><i class="fa fa-clock-o"></i> Clock Out</a>
                @endif
            </div>
			<!-- begin sidebar minify button -->
			<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			<!-- end sidebar minify button -->
		</ul>
		<!-- end sidebar nav -->
	</div>
	<!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
