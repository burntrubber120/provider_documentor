<html>
   
	@include('layouts.master.includes.head')
	
    <body> 
		<!-- begin #page-loader -->
		<div id="page-loader" class="fade in"><span class="spinner"></span></div>
		<!-- end #page-loader -->
        <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
			@include('layouts.master.includes.header')
		    
			@include('layouts.master.includes.sidebar')
			
			@yield('content')
        </div>
		
		@include('layouts.master.includes.footer')
    </body>
</html>