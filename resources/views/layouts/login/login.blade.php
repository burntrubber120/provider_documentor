<html>
   
	@include('layouts.login.includes.head')
	
    <body class="pace-top bg-white">
				
        <div id="page-container" class="fade">
            @yield('content')
        </div>
		
		@include('layouts.login.includes.footer')
    </body>
</html>