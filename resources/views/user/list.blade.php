@extends('layouts.master.master')

@section('title', 'User')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">User List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">User <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				<table class="table table-striped table-bordered table-manage">
					<thead>
						<tr>
							<th>Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users AS $k => $user)
						<tr>
							<td>{{ $user->first_name }} {{ $user->last_name }}</td>
							<td>{{ $user->username }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->phone }}</td>
							<td>
								<a class="btn btn-sm btn-success" href="./profile/{{encrypt($user->id)}}"><i class="fa fa-eye"></i></a>
								<a class="btn btn-sm btn-warning" href="./edit/{{encrypt($user->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger" href="./remove/{{encrypt($user->id)}}"><i class="fa fa-times"></i></a> 
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop