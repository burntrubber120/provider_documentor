@extends('layouts.master.master')

@section('title', 'User')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">User</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">User <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>User Add</legend>
								<div class="form-group">
									<label for="first_name">First Name</label>
									<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ old('first_name') }}"/>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ old('last_name') }}"/>
								</div>
								<div class="form-group">
									<label for="username">User Name</label>
									<input type="text" name="username" class="form-control" placeholder="User Name" value="{{ old('username') }}"/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone') }}"/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}"/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Add User</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop