@extends('layouts.master.master')

@section('title', 'User')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">User</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $user->first_name }} {{ $user->last_name }}</h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>User Edit</legend>
								<div class="form-group">
									<label for="first_name">First Name</label>
									<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ old('first_name', $user->first_name) }}"/>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ old('last_name', $user->last_name) }}"/>
								</div>
								<div class="form-group">
									<label for="username">User Name</label>
									<input type="text" name="" class="form-control" placeholder="User Name" value="{{ old('username', $user->username) }}" readonly/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone', $user->phone) }}"/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email', $user->email) }}"/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<fieldset>
							<legend>User Permissions</legend>
						</fieldset>
						<table id="data-table" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Permission</th>
									<th>Date Granted</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								@foreach($user->roles AS $k => $role)
								<tr>
									<td>{{ $role->type }}</td>
									<td>{{ $role->pivot->created_at->format('m/d/Y') }}</td>
									<td>
										<a class="btn btn-sm btn-danger" href="/user/permission/{{encrypt($user->id)}}/remove/{{encrypt($role->pivot->id)}}"><i class="fa fa-times"></i></a> 
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					
						<form action="/user/permission/{{encrypt($user->id)}}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<input type="hidden" name="user_id" value="{{ encrypt($user->id) }}" />
							<fieldset>
								<legend>Permission Add</legend>
								<div class="form-group">
									<label for="first_name">Permission</label>
									<select name="permission" class="form-control">
										<option value="">Select Role</option>
										@foreach($roles AS $role)
											<option value="{{ encrypt($role->id) }}">{{ $role->type }}</option>
										@endforeach
									</select>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->
			<!-- start col-6 -->		
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="./{{encrypt($user->id)}}/house" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>House Information</legend>
								<div class="form-group">
									<table class="table table-striped table-bordered table-manage">
									<thead>
										<tr>
											<th>Select</th>
											<th>Address</th>
											<th>City</th>
										</tr>
									</thead>
									<tbody>
										@foreach($houses AS $house)
											<tr>
												<td><label>
													<input name="house_id[]" type="checkbox" value="{{ encrypt($house->id) }}" {{ isset($user_houses[$house->id]) ? 'checked="checked"' : '' }}/> - {{ $house->nickname }}
													</label>
												</td>
												<td>{{ $house->address }}</td>
												<td>{{ $house->city }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Update</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
                <div class="panel panel-inverse">
                    <div class="panel-body">
                        <fieldset>
                        <legend>File Add</legend>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>File Name</th>
                                        <th>Type</th>
                                        <th>Valid</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user->files AS $k => $file)
                                    <tr>
                                        <td>{{ $file->file_name }}</td>
                                        <td>{{ $file->pivot->file_type }}</td>
                                        <td>{{ $file->pivot->start_date ? date('m/d/Y', strtotime($file->pivot->start_date)) : '' }} - {{ $file->pivot->end_date ? date('m/d/Y', strtotime($file->pivot->end_date)) : '' }}</td>
                                        <td>
                                            <a class="btn btn-sm btn-danger areYouSure" href="/user/file/{{encrypt($user->id)}}/remove/{{encrypt($file->id)}}"><i class="fa fa-times"></i></a> 
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <form action="/user/file/{{encrypt($user->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="user_id" value="{{ encrypt($user->id) }}" />
                                
                                <div class="form-group">
                                    <label for="file">Select File</label>
                                    {!! Form::file('file', NULL, ['class' => 'form-control', 'placeholder' => 'Select a File']) !!}
                                    
                                    <label for="file_type">File Type</label>
                                    {!! Form::select('file_type', $file_types, NULL, ['class' => 'form-control', 'placeholder' => 'Please Select']) !!}

                                    <label for="start_date">Start Date</label>
                                    {!! Form::text('starte_date', NULL, ['class' => 'form-control calDatePickerOpen', 'placeholder' => 'Start Date']) !!}

                                    <label for="end_date">End Date</label>
                                    {!! Form::text('end_date', NULL, ['class' => 'form-control calDatePickerOpen', 'placeholder' => 'End Date']) !!}
                                </div>
                                <button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
                            </form>
                        </fieldset>
                    </div>
                </div>
			</div>
			<!-- end col-6 -->				
		</div>
	</div>
	<!-- end #content -->
@stop