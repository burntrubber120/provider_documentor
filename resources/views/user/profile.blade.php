@extends('layouts.master.master')

@section('title', 'User')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">User</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $user->first_name }} {{ $user->last_name }}</h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
							<fieldset>
								<legend>Account Information</legend>
								{{ $user->first_name }}	{{ $user->last_name }} <br />
								{{ $user->phone }} <br />
								{{ $user->email }}
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->
			@if($user->id == Auth::id())
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-success">
					<div class="panel-heading">
						<div class="panel-heading-btn"><a class="btn btn-xs btn-warning" id="change_password_show">Change Password</a></div>
						&nbsp;
					</div>
					<div class="panel-body collapse">
						<fieldset>
							<legend>Change Password</legend>
						</fieldset>
							
						<form action="/my-profile/password" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<input type="hidden" name="user_id" value="{{ encrypt($user->id) }}" />
							<fieldset>
								<div class="form-group">
									<label for="current_password">Password</label>
									<input type="password" name="current_password" class="form-control" placeholder="Current Password" value=""/>
								</div>
								<div class="form-group">
									<label for="new_password">Password</label>
									<input type="password" name="new_password" class="form-control" placeholder="New Password" value=""/>
								</div>
								<div class="form-group">
									<label for="confirm_password">Confirm Password</label>
									<input type="password" name="confirm_password" class="form-control" placeholder="Confirm New Password" value=""/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			@endif
		</div>
	</div>
	<!-- end #content -->
	<script>
		$('#change_password_show').click(function () {
			$(this).parent().parent().next().toggle();
		});
	</script>
@stop