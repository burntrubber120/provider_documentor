@extends('layouts.master.master')

@section('title', 'Reports')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Medication by individual<small>report</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
						<table class="table table-striped table-bordered table-manage">
							<thead>
								<tr>
									<th>Individual</th>
									<th>Medications</th>
								</tr>
							</thead>
							<tbody>
								@foreach($individuals AS $k => $individual)
								<tr>
									<td>
										<a class="btn btn-sm btn-success" href="/individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a> {{ $individual->first_name }} {{ $individual->last_name }}
									</td>
									
									@if($individual->medications->count() > 0)
									<td>
										<ul>
										@foreach($individual->medications AS $medication)
											<li>{{$medication->name}}</li>
										@endforeach
										</ul>
									</td>
									@else
										<td class="text-warning">No Medications</td>
									@endif
									
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop