@extends('layouts.master.master')

@section('title', 'Reports')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Training by Employee <small>report</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-body">
						<div class="form-inline">
							<form>
							{!! Form::text('start_date', isset($start_date) ? $start_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Start Date']) !!}
							&mdash; {!! Form::text('end_date', isset($end_date) ? $end_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'End Date']) !!}
							<button type="submit" class="btn btn-sm btn-primary">Select Dates</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
						<table class="table table-striped table-bordered table-manage">
							<thead>
								<tr>
									<th>Employee</th>
									<th>Training</th>
									<td>Score</th>
									<td>Date Taken</th>
								</tr>
							</thead>
							<tbody>
								@foreach($users AS $k => $user)
									@if($user->trainings->count() > 0)
										@foreach($user->trainings AS $k => $training)
											<tr>
												<td>
													{{ $user->first_name }} {{ $user->last_name }}
												</td>
												<td>
													{{ $training->title_name }}
												</td>
												<td>
													{{ $training->pivot->score }}
												</td>
												<td>
													{{ $training->pivot->created_at->format('m/d/Y') }}
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td>
												{{ $user->first_name }} {{ $user->last_name }}
											</td>
											<td>
												No Trainings Taken
											</td>
											<td></td>
											<td></td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop