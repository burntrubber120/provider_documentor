@extends('layouts.master.master')

@section('title', 'Reports')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Reportable Incident Service Note <small>report (last 30 days)</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-body">
						<div class="form-inline">
							<form>
							{!! Form::text('start_date', isset($start_date) ? $start_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Start Date']) !!}
							&mdash; {!! Form::text('end_date', isset($end_date) ? $end_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'End Date']) !!}
							<button type="submit" class="btn btn-sm btn-primary">Select Dates</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
						
						<table class="table table-striped table-manage table-bordered table-buttons">
							<thead>
								<tr>
									<th>Individual</th>
									<th>Note</th>
									<td>Reportable Incident Date</th>
									<td>Entered By</td>
								</tr>
							</thead>
							<tbody>
								@foreach($individuals AS $k => $individual)
									@if($individual->report_casenotes->count() > 0)
										@foreach($individual->report_casenotes AS $casenote)
											<tr>
												<td>
													{{ $individual->first_name }} {{ $individual->last_name }}
												</td>
												<td>
													{{ $casenote->case_note }}
												</td>
												<td>
													{{ $casenote->note_date->format('m/d/Y')}}
												</td>
												<td>
													{{ $casenote->user->first_name }} {{ $casenote->user->last_name }}
												</td>
											</tr>
										@endforeach
									@else
										<tr>
										<td>{{ $individual->first_name }} {{ $individual->last_name }}</td>
										<td class="text-warning">No Reportable Incident Notes</td>
										<td></td>
										<td></td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop