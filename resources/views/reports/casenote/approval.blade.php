@extends('layouts.master.master')

@section('title', 'Reports')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Service Notes <small>needing approval</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">

			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
						
						<table class="table table-striped table-manage table-bordered">
							<thead>
								<tr>
									<th>Individual</th>
									<th style="width:50%">Note</th>
									<th style="width:5%">Service</th>
									<th>Note Date</th>
									<td>Start Time - End Time</th>
									<td>Entered By</td>
									<td>Approve/Reject</td>
								</tr>
							</thead>
							<tbody>
								@foreach($individuals AS $k => $individual)
									@if($individual->report_casenotes->count() > 0)
										@foreach($individual->report_casenotes AS $casenote)
											<tr>
												<td>
													{{ $individual->first_name }} {{ $individual->last_name }}
												</td>
												<td>
													{{ $casenote->case_note }}
												</td>
												<td>
													{{ isset($casenote->service) ? $casenote->service->code : ''}}
												</td>
												<td>
													{{ $casenote->note_date->format('m/d/Y')}}
												</td>
												<td>
													{{ $casenote->start_dt->format('h:i A')}} - {{ $casenote->end_dt->format('h:i A')}}
												</td>
												<td>
													{{ $casenote->user->first_name }} {{ $casenote->user->last_name }}
												</td>
                                                <td>
                                                    <a href="/individual/casenote/{{encrypt($individual->id)}}/approve/{{encrypt($casenote->id)}}" class="btn btn-sm btn-primary">Approve</a> 
                                                    <a href="/individual/casenote/{{encrypt($individual->id)}}/reject/{{encrypt($casenote->id)}}" class="btn btn-sm btn-danger">Reject</a>
                                                </td>
											</tr>
										@endforeach
									@else
										<tr>
										<td>{{ $individual->first_name }} {{ $individual->last_name }}</td>
										<td class="text-warning">No Service Notes</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop