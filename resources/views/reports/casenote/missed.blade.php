@extends('layouts.master.master')

@section('title', 'Reports')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Missed Medication <small>report</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-body">
						<div class="form-inline">
							<form>
							{!! Form::text('start_date', isset($start_date) ? $start_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'Start Date']) !!}
							&mdash; {!! Form::text('end_date', isset($end_date) ? $end_date->format('m/d/Y') : date('m/d/Y'), ['class' => 'form-control calDatePicker', 'placeholder' => 'End Date']) !!}
							<button type="submit" class="btn btn-sm btn-primary">Select Dates</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
						<fieldset>
							<legend>Missed &mdash; Medication</legend>
						</fieldset>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Individual</th>
									<th>Medications</th>
									<td>Missed Date/Time</th>
								</tr>
							</thead>
							<tbody>
								@foreach($individuals_missed_meds AS $k => $individual)
									@if($individual->missed->count() > 0)
										@foreach($individual->missed AS $missed)
											<tr>
												<td>
													<a class="btn btn-sm btn-success" href="/individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a> {{ $individual->first_name }} {{ $individual->last_name }}
												</td>
												<td>
													{{ $missed['medication']->name }}
												</td>
												<td>
													{{ $missed['day'] }} - {{ $missed['time'] }}
												</td>
											</tr>
										@endforeach
									@else
										<tr>
										<td><a class="btn btn-sm btn-success" href="/individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a> {{ $individual->first_name }} {{ $individual->last_name }}</td>
										<td class="text-warning">No Medications</td>
										</tr>
									@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop