@extends('layouts.master.master')

@section('title', 'Provider')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Provider</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $provider->provider_name }}</h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Provider Edit</legend>
								<div class="form-group">
									<label for="provider_name">Provider Name</label>
									<input type="text" name="provider_name" class="form-control" placeholder="Provider Name" value="{{ !old('provider_name') ? $provider->provider_name : old('provider_name')}}"/>
								</div>
								<div class="form-group">
									<label for="address">Address</label>
									<input type="text" name="address" class="form-control" placeholder="Address" value="{{ !old('address') ? $provider->address : old('address')}}"/>
								</div>
								<div class="form-group">
									<label for="city">City</label>
									<input type="text" name="city" class="form-control" placeholder="City" value="{{ !old('city') ? $provider->city : old('city') }}"/>
								</div>
								<div class="form-group">
									<label for="state">State</label>
									<input type="text" name="state" class="form-control" placeholder="State" value="{{ !old('state') ? $provider->state : old('state') }}"/>
								</div>
								<div class="form-group">
									<label for="zip">Zip</label>
									<input type="text" name="zip" class="form-control" maxlength="5" placeholder="Zip" value="{{ !old('zip') ? $provider->zip : old('zip') }}"/>
								</div>
								<div class="form-group">
									<label for="phone_primary">Phone Primary</label>
									<input type="text" name="phone_primary" class="form-control" placeholder="Phone Primary" value="{{ !old('phone_primary') ? $provider->phone_primary : old('phone_primary') }}"/>
								</div>
								<div class="form-group">
									<label for="phone_secondary">Phone Secondary</label>
									<input type="text" name="phone_secondary" class="form-control" placeholder="Phone Secondary" value="{{ !old('phone_secondary') ? $provider->phone_secondary : old('phone_secondary') }}"/>
								</div>
								
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save Provider</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
				
			</div>
			<!-- end col-6 -->
            <div class="col-md-6">
                <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
                        <h4>User List</h4>
                        <table class="table table-striped table-bordered table-manage">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($provider->users AS $k => $user)
                                <tr>
                                    <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-warning" href="../../user/profile/{{encrypt($user->id)}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
                        <h4>Individual List</h4>
                        <table class="table table-striped table-bordered table-manage">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($provider->individuals AS $k => $individual)
                                <tr>
                                    <td>{{ $individual->first_name }} {{ $individual->last_name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end col-6 -->
		</div>
	</div>
	<!-- end #content -->
@stop