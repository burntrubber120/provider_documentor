@extends('layouts.master.master')

@section('title', 'Admin')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Provider List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Provider <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
				<table class="table table-bordered table-manage">
					<thead>
						<tr>
							<th>Name</th>
							<th>Address</th>
							<th>Phone</th>
							<th># Users</th>
							<th># Individuals</th>
							<th># Houses</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($providers AS $k => $provider)
						<tr class="{{$provider->deleted_at != NULL ? 'bg-danger' : ''}}">
							<td>{{ $provider->provider_name }}</td>
							<td>{{ $provider->address }} {{ $provider->city}}, {{$provider->state}} {{ $provider->zip }}</td>
							<td>
								{{ $provider->phone_primary }} <br />
								{{ $provider->phone_secondary }}
							</td>
							<td>{{ $provider->users->count() }}</td>
							<td>{{ $provider->individuals->count() }}</td>
							<td>{{ $provider->houses->count() }}</td>
							<td>
								<a class="btn btn-sm btn-warning" href="./edit/{{encrypt($provider->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger areYouSure" href="./remove/{{encrypt($provider->id)}}"><i class="fa fa-times"></i></a> 
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<a class="btn btn-primary btn-md" href="/site-admin/provider/add">Add Provider</a>
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop