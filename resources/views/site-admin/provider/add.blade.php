@extends('layouts.master.master')

@section('title', 'Provider')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Provider</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Provider <small>Add</small></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Provider Add</legend>
								<div class="form-group">
									<label for="provider_name">Provider Name</label>
									<input type="text" name="provider_name" class="form-control" placeholder="Provider Name" value="{{ old('provider_name') }}"/>
								</div>
								<div class="form-group">
									<label for="address">Address</label>
									<input type="text" name="address" class="form-control" placeholder="Address" value="{{ old('address') }}"/>
								</div>
								<div class="form-group">
									<label for="city">City</label>
									<input type="text" name="city" class="form-control" placeholder="City" value="{{ old('city') }}"/>
								</div>
								<div class="form-group">
									<label for="state">State</label>
									<input type="text" name="state" class="form-control" placeholder="State" value="{{ old('state') }}"/>
								</div>
								<div class="form-group">
									<label for="zip">Zip</label>
									<input type="text" name="zip" class="form-control" maxlength="5" placeholder="Zip" value="{{ old('zip') }}"/>
								</div>
								<div class="form-group">
									<label for="phone_primary">Phone Primary</label>
									<input type="text" name="phone_primary" class="form-control" placeholder="Phone Primary" value="{{ old('phone_primary') }}"/>
								</div>
								<div class="form-group">
									<label for="phone_secondary">Phone Secondary</label>
									<input type="text" name="phone_secondary" class="form-control" placeholder="Phone Secondary" value="{{ old('phone_secondary') }}"/>
								</div>
								
								<legend>User Add</legend>
								<div class="form-group">
									<label for="first_name">First Name</label>
									<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{ old('first_name') }}"/>
								</div>
								<div class="form-group">
									<label for="last_name">Last Name</label>
									<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{ old('last_name') }}"/>
								</div>
								<div class="form-group">
									<label for="username">User Name</label>
									<input type="text" name="username" class="form-control" placeholder="User Name" value="{{ old('username') }}"/>
								</div>
								<div class="form-group">
									<label for="phone">Phone</label>
									<input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{ old('phone') }}"/>
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}"/>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Add Provider</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->
@stop