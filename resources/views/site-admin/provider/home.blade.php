@extends('layouts.master.master')

@section('title', 'Home')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Home</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Home <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		<div class="p-b-10">
			<h4>Welcome to CD.</h4>
		</div>
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
			
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop