@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach(array_unique($errors->all()) as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($alert = Session::get('error'))
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>{{{ $alert }}}</strong>
	</div>
@endif
@if ($message = Session::get('message'))
	<div class="alert alert-warning alert-dismissible" role="warning">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>{{{ $message }}}</strong>
	</div>
@endif