@if($messages->any())
    <div class="alert alert-danger">
        <ul>
            @foreach(array_unique($messages->all()) as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif