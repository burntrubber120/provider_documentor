@extends('layouts.master.master')

@section('title', 'Home')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		
		<!-- begin page-header -->
		<h1 class="page-header">Home <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor']))
			<div class="col-md-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h4 class="panel-title">Missed Medication</h4>
					</div>
					<div class="panel-body">
						@foreach($houses AS $house)
							@foreach($house->individuals AS $individual)
								@if($individual->missed->count() > 0)
									<div class="row">
										<div class="col-sm-6 col-md-1">
										<a class="btn btn-xs btn-success" href="./individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a> {{$individual->full_name}}
										</div>
										<div class="col-sm-6 col-md-11">
											@foreach($individual->missed AS $miss)
												{{$miss['medication']['name']}} - {{$miss['time']}}<br />
											@endforeach
										</div>
									</div>
									<hr>
								@endif
							@endforeach
						@endforeach
					</div>
				</div>
			</div>
			@endif
            @if($rejected_notes->count() > 0)
            <div class="col-md-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title">Rejected Service Notes</h4>
					</div>
					<div class="panel-body">
							@foreach($rejected_notes AS $case_note)
                                <div class="row p-5">
                                    <div class="col-sm-1 col-md-1">
                                    <a class="btn btn-xs btn-warning" href="./individual/casenote/{{encrypt($case_note->individual_id)}}/edit/{{encrypt($case_note->id)}}">Fix</i></a> 
                                    </div>
                                    <div class="col-sm-1 col-md-1">
                                        <p>{{$case_note->individual->full_name}}</p>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <p>Sent back on: <strong>{{$case_note->reject_date}}</strong></p>
                                    </div>
                                    <div class="col-sm-5 col-md-5">
                                        <p>
                                        {{ nl2br($case_note->reject_note)}}
                                        </p>
                                    </div>
                                </div>
                                <hr>
						@endforeach
					</div>
				</div>
			</div>
            @endif
			<div class="col-md-12">
				<!-- begin panel -->
				
				@foreach($user->current_houses AS $k => $house)
					<div class="panel panel-success">
						<div class="panel-heading">
							<h4 class="panel-title">{{ $house->nickname }} &mdash; {{ $house->address_block }}</h4>
						</div>
						<div class="panel-body">
						@if($house->individuals_with_today_user_case_note->count() > 0)
							@foreach($house->individuals_with_today_user_case_note AS $individual)
								<fieldset>
								<h4 class="bg-primary"><a class="btn btn-sm btn-success" href="./individual/view/{{encrypt($individual->id)}}"><i class="fa fa-eye"></i></a> {{ $individual->first_name }} {{ $individual->last_name }}</h4>
								@if($individual->casenote_today_current_user == null)
									<div class="alert alert-warning">You have not entered a service note today.</div>
								@else
									<p>{{ $individual->casenote_today_current_user->case_note }}</p>
								@endif
							
								@if($individual->medications->count() > 0)
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Medication</th>
											<th>Dosage</th>
											<th>Times/Take</th>
										</tr>
									</thead>
									<tbody>
										@foreach($individual->medications AS $medication)
											@if($medication->today_times->count() > 0)
											<tr>
												<td>{{ $medication->name }}</td>
												<td>{{ $medication->number_doses }} {{ $medication->type }} - {{ $medication->dosage }}</td>
												<td>
													@foreach($medication->today_times AS $time)
														{{ date('h:i A', strtotime($time->time)) }}
														
														@if(isset($time->missed))
															&mdash; Missed
														@elseif(isset($time->logged))
															&mdash; Taken {{ $time->logged->format('D h:i A') }}
														@elseif(strtotime($time->time) >= strtotime("-30 minutes") && strtotime($time->time) <= strtotime("+30 minutes"))
															&mdash; <a class="btn btn-sm btn-warning areYouSure" href="./individual/medication/{{encrypt($individual->id)}}/take/{{encrypt($medication->id)}}"><i class="fa fa-heart"></i></a>
														@endif
														<br />
													@endforeach
												</td>
											</tr>
											@elseif($medication->schedules->count() == 0)
											<tr>
												<td>{{ $medication->name }}</td>
												<td>{{ $medication->number_doses }} {{ $medication->type }} - {{ $medication->dosage }}</td>
												<td><a class="btn btn-sm btn-warning areYouSure" href="./individual/medication/{{encrypt($individual->id)}}/take/{{encrypt($medication->id)}}"><i class="fa fa-heart"></i></a></td>
											</tr>
											@endif
											
										@endforeach
									</tbody>
								</table>
								@endif
								<fieldset>
								<hr>
							@endforeach
						@else
							<h4 class="text-warning">No Individuals Assigned</h4>
						@endif
					</div>
				</div>
				@endforeach
							
			</div>
		</div>
	</div>
	<!-- end #content -->
@stop