@extends('layouts.login.login')

@section('title', 'Login')


@section('content')
    <!-- begin login -->
	<div class="login login-with-news-feed">
		<!-- begin news-feed -->
		<div class="news-feed">
			<div class="news-image">
				<img src="/assets/img/bg-8.jpg" data-id="login-cover-image" alt="" />
			</div>
			<div class="news-caption">
				<h4 class="caption-title"><i class="fa fa-briefcase text-success"></i> PD</h4>
				<p>
					Provider DocuMentor
				</p>
			</div>
		</div>
		<!-- end news-feed -->
		<!-- begin right-content -->
		<div class="right-content">
			<!-- begin login-header -->
			<div class="login-header">
				<div class="brand">
					<span class="logo"></span> Provider DocuMentor
				</div>
				<div class="icon">
					<i class="fa fa-sign-in"></i>
				</div>
			</div>
			<!-- end login-header -->
			@include('includes.error.list')
			<!-- begin login-content -->
			<div class="login-content">
				<form action="" method="POST" class="margin-bottom-0">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group m-b-15">
						<input type="text" name="username" value="{{ old('username') }}" class="form-control input-lg" placeholder="Username" />
					</div>
					<div class="form-group m-b-15">
						<input type="password" name="password" class="form-control input-lg" placeholder="Password" />
					</div>
					<div class="login-buttons">
						<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
					</div>
					<div class="m-t-20 m-b-40 p-b-40">
						<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
					</div>
					<hr />
					<p class="text-center text-inverse">
						&copy; Provider DocuMentor All Right Reserved 2017
					</p>
				</form>
			</div>
			<!-- end login-content -->
		</div>
		<!-- end right-container -->
	</div>
	<!-- end login -->
@stop