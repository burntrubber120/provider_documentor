@extends('layouts.login.login')

@section('title', 'Forgot Password')


@section('content')
    <!-- begin login -->
	<div class="login login-with-news-feed">
		<!-- begin news-feed -->
		<div class="news-feed">
			<div class="news-image">
				<img src="/assets/img/bg-8.jpg" data-id="login-cover-image" alt="" />
			</div>
			<div class="news-caption">
				<h4 class="caption-title"><i class="fa fa-briefcase text-success"></i> PD</h4>
				<p>
					Provider DocuMentor
				</p>
			</div>
		</div>
		<!-- end news-feed -->
		<!-- begin right-content -->
		<div class="right-content">
			<!-- begin login-header -->
			<div class="login-header">
				<div class="brand">
					<span class="logo"></span> Provider DocuMentor
				</div>
				<div class="icon">
					<i class="fa fa-sign-in"></i>
				</div>
			</div>
			<!-- end login-header -->
			@include('includes.error.list')
			<!-- begin login-content -->
			<div class="login-content">
				<form action="{{ route('password.email') }}" method="POST" class="margin-bottom-0">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group m-b-15">
						<input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" placeholder="Email Address" required>
					</div>
					<div class="login-buttons">
						<button type="submit" class="btn btn-primary">Send Password Reset Link</button>
					</div>
					<p class="m-t-5 text-center text-inverse">
						&copy; Provider DocuMentor All Right Reserved 2017
					</p>
				</form>
			</div>
			<!-- end login-content -->
		</div>
		<!-- end right-container -->
	</div>
	<!-- end login -->
@stop