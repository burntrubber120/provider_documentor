@extends('layouts.master.master')

@section('title', 'Training')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/training/list">Training List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Training {{ $training->title }}</h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">{{ $training->title_name }}</h4>
					</div>
					<div class="panel-body">
						<form method="POST" class="form" action="">
							<input type="hidden" name="_token" value="{{ csrf_token() }}"  />
							<fieldset>
							@foreach($training->questions AS $question)						
								@if($question->type == 'textbox')
								<div class="form-group">
									<label for="first_name">{{$question->question}}</label>
									<input type="text" name="q_{{$question->id}}" class="form-control" value="{{ old('q_'.$question->id) }}" />
								</div>
								@endif
								@if($question->type == 'dropdown')
								<div class="form-group">
									<label for="first_name">{{$question->question}}</label>
									<select name="q_{{$question->id}}" class="form-control">
										<option value="">Please Select</option>
										@foreach($question->answers AS $answer)
											<option value="{{$answer->id}}">{{$answer->answer}}</option>
										@endforeach
									</select>
								</div>
								@endif
								@if($question->type == 'checkbox')
								<div class="form-group">
									<label for="first_name">{{$question->question}}</label><br />
									@foreach($question->answers AS $answer)
										<label><input type="checkbox" name="q_{{$question->id}}[]" class="" value="{{$answer->id}}" /> {{$answer->answer}}</label><br />
									@endforeach
								</div>
								@endif
							@endforeach
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Complete Training</button>
						</form>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop