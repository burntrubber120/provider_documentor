@extends('layouts.master.master')

@section('title', 'Training')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/training/list">Training</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Training <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
		<form action="" method="POST">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse">
					<div class="panel-body">
					
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						<fieldset>
							<legend>Training Add</legend>
							<div class="form-group">
								<a class="btn btn-success training_type collapse" id="make_regular">Make Employee Training</a>
								<a class="btn btn-success training_type" id="make_individual">Make Individual Specific</a>
							</div>
							<div class="form-group training_type collapse">
								<label for="title">For Specific Individual</label>
								<select name="individual" id="individual_select" class="form-control">
									<option value="0">Please Select</option>
									@foreach($individuals AS $k => $individual)
									<option value="{{encrypt($individual->id)}}">{{$individual->first_name}} {{$individual->last_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group training_type">
								<label for="title">Title</label>
								<input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') }}"/>
							</div>
							<div class="form-group">
								<label for="description">Description</label>
								<textarea type="text" name="description" class="form-control" placeholder="Description">{{ old('description') }}</textarea>
							</div>
							<div class="form-group">
								<label for="website">Website/Link</label>
								<input type="text" name="website" length="250" class="form-control" placeholder="Website/Link" value="{{ old('website') }}"/>
							</div>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Save Training</button>
						</fieldset>
						
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->
			<div class="col-md-6">
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<fieldset>
							<legend>Questions</legend>
							<div class="form-group">
								<a class="add_question btn btn-sm btn-success">Add Question</a>
								<div class="row"><ol id="question_list"></ol></div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>		
			<!-- end col-6 -->
		</form>
		</div>
	</div>
	<!-- end #content -->
<script>
$('#make_individual').click(function () {
	$('.training_type').toggle();
});
$('#make_regular').click(function () {
	$('.training_type').toggle();
	$('#individual_select').val(0);
});
var question_counter = 1;
var response_counter = 1;
$('.add_question').click(function () {
	question_counter++;
	$('#question_list').append('<div class="row p-5 form-inline border"><div class="col-lg-6 col-md-6 col-sm-6 form-group">' + 
									'<li><input type="text" name="question_' + question_counter  + '" class="form-control" placeholder="Question Text" value=""/>' +
									'<select name="type_' + question_counter + '" class="form-control">' +
										'<option value="dropdown">Dropdown</option>' +
										'<option value="checkbox">Checkbox</option>' +
										'<option value="textarea">Textbox</option>' +
									'</select>' +
									'<a class="btn btn-xs btn-danger remove_question"><i class="fa fa-times"></i></a></li></div>' + 
									'<div class="col-lg-6 col-md-6 col-sm-6 form-group"><a class="btn btn-sm btn-success add_response" question="' + question_counter  + '">Add Response <i class="fa fa-plus"></i></a></div>' + 
								'</div>');
});
$(document).on('click', '.add_response', function () {
	response_counter++;
	$(this).parent().parent().children("div:nth-child(2)").append('<div class="p-5"><input type="text" name="q' + $(this).attr("question") + '_response' + response_counter + '" class="form-control" placeholder="Response Text" value=""/>' +
										'Correct Response: <input type="checkbox" name="q' + $(this).attr("question") + '_correctanswer' + response_counter + '" class="form-control" value="1"/>' +
										'<a class="btn btn-sm btn-danger remove_response">Remove Response <i class="fa fa-times"></i></a>' + 
										'</div>');
});
$(document).on('click', '.remove_question', function() {
	$(this).parent().parent().parent().remove();
});
$(document).on('click', '.remove_response', function() {
	$(this).parent().remove();
});
</script>
@stop