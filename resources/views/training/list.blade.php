@extends('layouts.master.master')

@section('title', 'Training')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Training List</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Training <small>Compliance Documentor</small></h1>
		<!-- end page-header -->
		
		@include('includes.error.list')
		
		<div class="row">
			<div class="panel p-10">
			<legend><h4>Training List</h4></legend>
				<table class="table table-striped table-bordered table-manage">
					<thead>
						<tr>
							<th>Training</th>
							<th>Description</th>
							<th>Website</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($trainings AS $k => $training)
						<tr>
							<td>{{ $training->individual_id ? $training->individual->first_name . ' ' . $training->individual->first_name : $training->title }}</td>
							<td>{{ $training->description }}</td>
							<td>@if($training->website !== "") <a href="{{ strpos($training->website, 'http') ? $training->website : 'http://'.$training->website}}" target="_blank">Click Here</a> @endif</td>
							<td>
								<a class="btn btn-sm btn-success" href="./take/{{encrypt($training->id)}}"><i class="fa fa-eye"></i></a>
								@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager']))
								<a class="btn btn-sm btn-warning collapse" href="./edit/{{encrypt($training->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger areYouSure" href="./remove/{{encrypt($training->id)}}"><i class="fa fa-times"></i></a> 
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>
		<div class="row">
			<div class="panel p-10">
				<legend><h4>Individual Specific</h4></legend>
				<table class="table table-striped table-bordered table-manage">
					<thead>
						<tr>
							<th>Training</th>
							<th>Description</th>
							<th>Website</th>
							<th>Options</th>
						</tr>
					</thead>
					<tbody>
						@foreach($consumer_specific_trainings AS $k => $training)
						<tr>
							<td>{{ $training->individual_id ? $training->individual->first_name . ' ' . $training->individual->last_name : $training->title }}</td>
							<td>{{ $training->description }}</td>
							<td>@if($training->website !== "") <a href="{{ strpos($training->website, 'http') ? $training->website : 'http://'.$training->website}}" target="_blank">Click Here</a> @endif</td>
							<td>
								<a class="btn btn-sm btn-success" href="./take/{{encrypt($training->id)}}"><i class="fa fa-eye"></i></a>
								@if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager']))
								<a class="btn btn-sm btn-warning collapse" href="./edit/{{encrypt($training->id)}}"><i class="fa fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger areYouSure" href="./remove/{{encrypt($training->id)}}"><i class="fa fa-times"></i></a> 
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>
	</div>
	<!-- end #content -->
@stop