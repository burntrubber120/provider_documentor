@extends('layouts.master.master')

@section('title', 'Training')


@section('content')
    <!-- begin #content -->
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="/">Training</a></li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">{{ $training->title }}</h1>
		<!-- end page-header -->
				
		@include('includes.error.list')
		
		<div class="row">
			<div class="col-md-6">
				<!-- begin panel -->
				<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
					<div class="panel-body">
						<form action="" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							<fieldset>
								<legend>Training Edit</legend>
								<div class="form-group">
									<label for="title">Title</label>
									<input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title', $training->title) }}"/>
								</div>
								<div class="form-group">
									<label for="description">Description</label>
									<textarea type="text" name="description" class="form-control" placeholder="Description">
										{{ old('description', $training->description) }}
									</textarea>
								</div>
								<div class="form-group">
									<label for="website">Website/Link</label>
									<input type="text" name="website" class="form-control" placeholder="Website/Link" value="{{ old('website', $training->website) }}"/>
								</div>
								<div class="form-group">
									<legend>Questions</legend>
									<div id="question_list">No Quesitons Added</div>
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5">Save</button>
							</fieldset>
						</form>
					</div>
				</div>
				<!-- end panel -->
			</div>
			<!-- end col-6 -->		
		</div>
	</div>
	<!-- end #content -->

<script>
$('#zip').mask('99999');
</script>
@stop