<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@getLogin']);
Route::post('/login',           'Auth\LoginController@postLogin');
Route::any('/logout',           'Auth\LoginController@anyLogout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => ['auth']], function()
{
	Route::get('/', 'DashboardController@getHome');
	Route::get('/my-profile', 'UserController@getProfile');
    Route::post('/my-profile/password', 'UserController@postProfilePassword');
});

Route::group(['middleware' => ['auth', 'group:provider']], function()
{
	Route::get('/individual/view/{id}', 'IndividualController@getViewIndividual');
	Route::get('/individual/casenote/{id}/add', 'IndividualController@getAddCaseNote');
	Route::post('/individual/casenote/{id}/add', 'IndividualController@postAddCaseNote');
	Route::get('/individual/casenote/{id}/edit/{cn_id}', 'IndividualController@getEditCaseNote');
	Route::post('/individual/casenote/{id}/edit/{cn_id}', 'IndividualController@postEditCaseNote');
	Route::get('/individual/medication/{id}/take/{med}', 'IndividualController@getTakeMedication');

	Route::get('/training/list','TrainingController@getTrainingList');
	Route::get('/training/take/{id}', 'TrainingController@getTrainingTake');
    Route::post('/training/take/{id}', 'TrainingController@postTrainingTake');
    
    Route::get('/clock/{punch}/{user_id?}', 'UserController@getClockInOut');

    Route::get('/house/schedule/{id}', 'HouseController@getHouseSchedule');
});

Route::group(['middleware' => ['auth', 'groupType:provider|Administrator;provider|Manager']], function()
{
	
	Route::get('/user/profile/{id}', 'UserController@getProfile');
	Route::get('/user/list', 'UserController@getUserList');
	Route::get('/user/add', 'UserController@getAddUser');
	Route::post('/user/add', 'UserController@postAddUser');
	Route::get('/user/edit/{id}', 'UserController@getEditUser');
    Route::post('/user/edit/{id}', 'UserController@postEditUser');
    Route::get('/user/remove/{id}', 'UserController@getRemoveUser');

    //user files
    Route::post('/user/file/{id}', 'UserController@postFileUser');
    Route::get('/user/file/{id}/remove/{ur_id}', 'UserController@getRemoveUserFileUpload');

	Route::get('/user/permission/{id}', 'UserController@getAddUserPermission');
	Route::post('/user/permission/{id}', 'UserController@postAddUserPermission');
	Route::get('/user/permission/{id}/remove/{ur_id}', 'UserController@getRemoveUserPermission');
	
	Route::post('/user/edit/{id}/house', 'UserController@postEditUserHouse');
	
	Route::get('/house/list', 'HouseController@getHouseList');
	Route::get('/house/add', 'HouseController@getAddHouse');
	Route::post('/house/add', 'HouseController@postAddHouse');
	Route::get('/house/edit/{id}', 'HouseController@getEditHouse');
	Route::post('/house/edit/{id}', 'HouseController@postEditHouse');
	Route::get('/house/view/{id}', 'HouseController@getViewHouse');
	Route::get('/house/safety-checklist/{id}/add', 'FormController@getSafetyChecklistAdd');
	Route::post('/house/safety-checklist/{id}/add', 'FormController@postSafetyChecklistAdd');
	Route::get('/house/safety-checklist/{id}/edit/{checklist_id}', 'FormController@getSafetyChecklistEdit');
	Route::post('/house/safety-checklist/{id}/edit/{checklist_id}', 'FormController@postSafetyChecklistAdd');
	Route::get('/house/safety-checklist/{id}/view/{checklist_id}', 'FormController@getSafetyChecklistView');	
	Route::get('/house/safety-checklist/{id}/remove/{checklist_id}', 'FormController@getSafetyChecklistRemove');	

    
	
	Route::get('/individual/list', 'IndividualController@getIndividualList');
	Route::get('/individual/add', 'IndividualController@getAddIndividual');
	Route::post('/individual/add', 'IndividualController@postAddIndividual');
	Route::get('/individual/edit/{id}', 'IndividualController@getEditIndividual');
	Route::post('/individual/edit/{id}', 'IndividualController@postEditIndividual');
	//edit/add house
	Route::post('/individual/edit/{id}/house', 'IndividualController@postEditIndividualHouse');
	
	Route::get('/individual/casenote/{id}/list', 'IndividualController@getListCaseNote');
	
	//medication for individual
	Route::get('/individual/medication/{id}/view', 'IndividualController@getViewMedication');
	Route::post('/individual/medication/{id}/add', 'IndividualController@postAddMedication');
	Route::get('/individual/medication/{id}/remove/{med}', 'IndividualController@getRemoveMedication');
	Route::get('/individual/medication/{id}/taken-report', 'IndividualController@getTakenMedication');
	Route::get('/individual/medication/{id}/remove/{med}/{med_taken_id}', 'IndividualController@getRemoveTakenMedication');
	
	//medical appointments
	Route::get('/individual/medical-appointment/{id}/add', 'IndividualController@getMedicalAppointmentAdd');
	Route::post('/individual/medical-appointment/{id}/add', 'IndividualController@postMedicalAppointmentAdd');
	Route::get('/individual/medical-appointment/{id}/edit/{apointment_id}', 'IndividualController@getMedicalAppointmentEdit');
	Route::post('/individual/medical-appointment/{id}/edit/{apointment_id}', 'IndividualController@postMedicalAppointmentAdd');
	Route::get('/individual/medical-appointment/{id}/view/{apointment_id}', 'IndividualController@getMedicalAppointmentView');
	Route::get('/individual/medical-appointment/{id}/remove/{apointment_id}', 'IndividualController@getMedicalAppointmentRemove');

	//support team
	Route::get('/individual/support-team/{id}/add', 'IndividualController@getSupportTeamAdd');
	Route::post('/individual/support-team/{id}/add', 'IndividualController@postSupportTeamAdd');
	Route::get('/individual/support-team/{id}/edit/{member_id}', 'IndividualController@getSupportTeamEdit');
	Route::post('/individual/support-team/{id}/edit/{member_id}', 'IndividualController@postSupportTeamAdd');
	Route::get('/individual/support-team/{id}/view/{member_id}', 'IndividualController@getSupportTeamView');
	Route::get('/individual/support-team/{id}/remove/{member_id}', 'IndividualController@getSupportTeamRemove');
	//suport team concerns
	Route::post('/individual/support-team/{id}/view/{member_id}', 'IndividualController@postSupportTeamConcernAdd');
	Route::get('/individual/team-concern/{id}/remove/{concern_id}', 'IndividualController@getSupportTeamConcernRemove');

	//seizure
	Route::get('/individual/seizures/{id}/add', 'IndividualController@getSeizuresAdd');
	Route::post('/individual/seizures/{id}/add', 'IndividualController@postSeizuresAdd');
	Route::get('/individual/seizures/{id}/edit/{seizure_id}', 'IndividualController@getSeizuresEdit');
	Route::post('/individual/seizures/{id}/edit/{seizure_id}', 'IndividualController@postSeizuresAdd');
	Route::get('/individual/seizures/{id}/view/{seizure_id}', 'IndividualController@getSeizuresView');
	Route::get('/individual/seizures/{id}/remove/{seizure_id}', 'IndividualController@getSeizuresRemove');

	//risk plans
	Route::get('/individual/risk-plan/{id}/add', 'IndividualController@getRiskPlanAdd');
	Route::post('/individual/risk-plan/{id}/add', 'IndividualController@postRiskPlanAdd');
	Route::get('/individual/risk-plan/{id}/edit/{seizure_id}', 'IndividualController@getRiskPlanEdit');
	Route::post('/individual/risk-plan/{id}/edit/{seizure_id}', 'IndividualController@postRiskPlanAdd');
	Route::get('/individual/risk-plan/{id}/remove/{seizure_id}', 'IndividualController@getRiskPlanRemove');

	//goals for individual
	Route::get('/individual/goal/{id}/view', 'IndividualController@getViewGoal');
	Route::post('/individual/goal/{id}/add', 'IndividualController@postAddGoal');
	Route::post('/individual/goal/{id}/strategy', 'IndividualController@postAddStrategy');
    Route::get('/individual/goal/{id}/remove/{goal_id}', 'IndividualController@getRemoveGoal');
    
    //note approval/reject
    Route::get('/individual/casenote/{id}/approve/{cn_id}', 'IndividualController@getCaseNoteApproval');    
    Route::get('/individual/casenote/{id}/reject/{cn_id}', 'IndividualController@getCaseNoteReject');
    Route::post('/individual/casenote/{id}/reject/{cn_id}', 'IndividualController@postCaseNoteReject');
    Route::get('/individual/casenote/{id}/unapprove/{cn_id}', 'IndividualController@getCaseNoteUnApprove');
	
	//Reports
	Route::get('/provider/report/case-notes/minor', 'ReportController@getMinorIncidentNote');
	Route::get('/provider/report/case-notes/major', 'ReportController@getMajorIncidentNote');
	Route::get('/provider/report/case-notes/by-individual', 'ReportController@getIndividualNote');
	Route::get('/provider/report/case-notes/with-qol', 'ReportController@getQolNote');
	Route::get('/provider/report/case-notes/approval', 'ReportController@getIndividualNoteApproval');
	Route::get('/provider/report/case-notes/approved', 'ReportController@getIndividualNoteApproved');
	Route::get('/provider/report/employee-list', 'ReportController@getEmployeeList');
	Route::get('/provider/report/medication/by-individual', 'ReportController@getMedicationByIndividual');
	Route::get('/provider/report/medication/missed-by-time', 'ReportController@getMissedMedicationByTime');
	Route::get('/provider/report/training/by-employee', 'ReportController@getTrainingByEmployee');
	Route::get('/provider/report/time-punches/by-employee', 'ReportController@getClockInOutByEmployee');
	Route::get('/provider/report/time-punches/employee-hours', 'ReportController@getHoursByEmployee');
	
	/**
	 * Start Forms
	 */
	//Field checklist FC Checklist
	Route::get('/individual/fc-checklist/{id}/add', 'FormController@getFieldChecklistAdd');
	Route::post('/individual/fc-checklist/{id}/add', 'FormController@postFieldChecklistAdd');
	Route::get('/individual/fc-checklist/{id}/edit/{checklist_id}', 'FormController@getFieldChecklistEdit');
	Route::post('/individual/fc-checklist/{id}/edit/{checklist_id}', 'FormController@postFieldChecklistAdd');
	Route::get('/individual/fc-checklist/{id}/view/{summary_id}', 'FormController@getFieldChecklistView');
	Route::get('/individual/fc-checklist/{id}/remove/{summary_id}', 'FormController@getFieldChecklistRemove');

	//Quarterly Summary
	Route::get('/individual/quarterly-summary/{id}/add', 'FormController@getQuarterlySummaryAdd');
	Route::post('/individual/quarterly-summary/{id}/add', 'FormController@postQuarterlySummaryAdd');
	Route::get('/individual/quarterly-summary/{id}/edit/{summary_id}', 'FormController@getQuarterlySummaryEdit');
	Route::post('/individual/quarterly-summary/{id}/edit/{summary_id}', 'FormController@postQuarterlySummaryAdd');
	Route::get('/individual/quarterly-summary/{id}/view/{summary_id}', 'FormController@getQuarterlySummaryView');
	Route::get('/individual/quarterly-summary/{id}/remove/{summary_id}', 'FormController@getQuarterlySummaryRemove');
	/**
	 * End forms
	 */

	Route::get('/training/add','TrainingController@getAddTraining');
    Route::post('/training/add','TrainingController@postAddTraining');
    
    /**
     * Start Graphs
     */
    Route::get('/individual/graph/{id}/view/{graph_id}', 'IndividualController@getGraphView');
    Route::post('/individual/graph/{id}/view/{graph_id}', 'IndividualController@postGraphView');
    Route::get('/individual/graph-data/{id}/remove/{graph_id}/{data_id}', 'IndividualController@getRemoveGraphData');
    Route::get('/individual/graph/{id}/add', 'IndividualController@getGraphAdd');
	Route::post('/individual/graph/{id}/add', 'IndividualController@postGraphAdd');
	Route::get('/individual/graph/{id}/remove/{graph_id}', 'IndividualController@getRemoveGraph');
     /**
      * End Graphs
      */
});


Route::group(['middleware' => ['auth', 'groupType:site|admin']], function()
{
	Route::get('/site-admin', 'AdminController@getHome');
	Route::get('/site-admin/provider/add', 'AdminController@getProviderAdd');
	Route::post('/site-admin/provider/add', 'AdminController@postProviderAdd');
	Route::get('/site-admin/provider/list', 'AdminController@getProviderList');
	Route::get('/site-admin/provider/edit/{provider_id}', 'AdminController@getProviderEdit');
	Route::post('/site-admin/provider/edit/{provider_id}', 'AdminController@postProviderEdit');
	Route::get('/site-admin/provider/remove/{provider_id}', 'AdminController@getProviderRemove');
	
	Route::get('/site-admin/user/list', 'AdminController@getUserList');
	
	Route::get('/site-admin/user/profile/{id}', 'UserController@getProfile');
	Route::get('/site-admin/user/reset/{id}', 'AdminController@resetUserPw');
    
    Route::get('/site-admin/user/import', 'AdminController@importUser');
	//Route::get('/user/list', 'AdminController@getUserList');
	//Route::get('/user/add', 'AdminController@getAddUser');
	//Route::post('/user/add', 'AdminController@postAddUser');
	
});

