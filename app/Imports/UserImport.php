<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class UserImport implements ToModel
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //ignore header row
        if (!isset($row[0])) {
            return null;
        }
        $password_clear = $this->generate_password();
        $password_hash = bcrypt($password_clear);
        return new User([
            //
            'provider_id' => $row[0],
            'username' => $row[1].$row[2],
            'first_name' => $row[1],
            'last_name' => $row[2],
            'phone' => trim($row[3]) != '' ? $row[3] : $row[4],
            'password' => $password_hash
        ]);
    }

    public function generate_password($length = 6, $add_dashes = false, $available_sets = 'ud')
    {
        $sets = array();
        
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        
        $all = '';
        $password = '';
        
        foreach($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        
        $all = str_split($all);
        
        for($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
            
        $password = str_shuffle($password);
        
        if(!$add_dashes) {
            return $password;
        }
        
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        
        while(strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        
        $dash_str .= $password;
        return $dash_str;
    }
}
