<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderTraining extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderTraining';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function individual()
    {
        return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\ProviderTrainingQuestion', 'training_id', 'id');
    }

    public function getTitleNameAttribute()
    {
        return $this->individual_id ? $this->individual->first_name . ' ' . $this->individual->last_name : $this->title;
    }
}
