<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
	use SoftDeletes;
	protected $table = 'tblRole';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
        'group', 'type'
    ];
}
