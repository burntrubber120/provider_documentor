<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderHouse extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderHouse';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'provider_id', 'nickname', 'address', 'city', 'state', 'zip', 'phone'
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function employees()
    {
        return $this->belongsToMany('App\Models\User', 'tblProviderHouseUser', 'house_id', 'user_id')->whereNull('tblProviderHouseUser.deleted_at');
    }
	
	public function individuals()
    {
        return $this->belongsToMany('App\Models\ProviderIndividual', 'tblProviderHouseIndividual', 'house_id', 'individual_id')->whereNull('tblProviderHouseIndividual.deleted_at');
    }
	
	public function individuals_with_casenotes()
    {
        return $this->belongsToMany('App\Models\ProviderIndividual', 'tblProviderHouseIndividual', 'house_id', 'individual_id')->whereNull('tblProviderHouseIndividual.deleted_at')->with('casenotes');
    }
	
	public function individuals_with_today_user_case_note()
    {
        return $this->belongsToMany('App\Models\ProviderIndividual', 'tblProviderHouseIndividual', 'house_id', 'individual_id')->whereNull('tblProviderHouseIndividual.deleted_at')->with('casenote_today_current_user');
    }

    public function safety_checklists()
	{
		return $this->hasMany('App\Models\ProviderHouseFormSafetyChecklist', 'house_id', 'id')->orderBy('drill_date', 'DESC');
	}
	
	public function getAddressBlockAttribute()
	{
		return "{$this->address} " . "{$this->city} " . "{$this->state}, " . "{$this->zip}";
	}
}
