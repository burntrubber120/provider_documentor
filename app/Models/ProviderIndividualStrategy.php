<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualStrategy extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualStrategy';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'goal_id', 'user_id', 'individual_id', 'edited_by_id', 'strategy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function edited_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function goal()
	{
		return $this->hasOne('App\Models\ProviderIndividualGoal', 'id', 'goal_id');
    }
    
    public function getStrategyGoalAttribute()
    {
        return substr($this->goal->goal, 0, 30) .'&mdash;'. substr($this->strategy, 0, 30);
    }
}
