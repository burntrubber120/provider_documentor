<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualFormQuarterlySummary extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualFormQuarterlySummary';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_complete', 'noa_start_date', 'noa_end_date', 'pcisp_date', 'bsp_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function edited_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function individual()
	{
		return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
	}
	
	public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'tblProviderIndividualFormQuarterlySummaryService', 'summary_id', 'service_id')
                    ->whereNull('tblProviderIndividualFormQuarterlySummaryService.deleted_at')->withTimestamps();
    }

    public function month_one_services()
    {
        return $this->belongsToMany('App\Models\Service', 'tblProviderIndividualFormQuarterlySummaryService', 'summary_id', 'service_id')
                    ->where('month', 1)->whereNull('tblProviderIndividualFormQuarterlySummaryService.deleted_at')->withTimestamps();
    }

    public function month_two_services()
    {
        return $this->belongsToMany('App\Models\Service', 'tblProviderIndividualFormQuarterlySummaryService', 'summary_id', 'service_id')
                    ->where('month', 2)->whereNull('tblProviderIndividualFormQuarterlySummaryService.deleted_at')->withTimestamps();
    }

    public function month_three_services()
    {
        return $this->belongsToMany('App\Models\Service', 'tblProviderIndividualFormQuarterlySummaryService', 'summary_id', 'service_id')
                    ->where('month', 3)->whereNull('tblProviderIndividualFormQuarterlySummaryService.deleted_at')->withTimestamps();
    }
}
