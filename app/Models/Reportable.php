<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reportable extends Model
{
	use SoftDeletes;
	protected $table = 'tblReportable';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
        'reportable_text'
    ];
    
}
