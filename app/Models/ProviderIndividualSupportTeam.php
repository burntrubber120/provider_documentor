<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualSupportTeam extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualSupportTeam';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function getFullNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function edited_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function individual()
	{
		return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
    }
    
    public function concerns()
	{
		return $this->hasMany('App\Models\ProviderIndividualSupportTeamConcern', 'support_team_id', 'id');
	}
	
}
