<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProvider';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
        'provider_name', 'address', 'city', 'state', 'zip', 'phone_primary', 'phone_secondary'
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function users()
    {
        return $this->hasMany('App\Models\User', 'provider_id', 'id');
    }
	
	public function individuals()
    {
        return $this->hasMany('App\Models\ProviderIndividual', 'provider_id', 'id');
    }
	
	public function houses()
	{
		return $this->hasMany('App\Models\ProviderHouse', 'provider_id', 'id');
    }
    
    public function trainings()
	{
		return $this->hasMany('App\Models\ProviderTraining', 'provider_id', 'id');
	}
}
