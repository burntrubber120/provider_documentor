<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class ProviderIndividual extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividual';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'dob', 'dod'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'provider_id', 'first_name', 'last_name', 'ssn', 'dob', 'height', 'weight', 'email', 'phone', 'dod', 'about_me'
    ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
	];
	
	public function getFullNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function casenotes()
    {
        return $this->hasMany('App\Models\ProviderIndividualCaseNote', 'individual_id', 'id')->orderBy('note_date', 'DESC');
    }
	
	public function ten_casenotes()
    {
        return $this->hasMany('App\Models\ProviderIndividualCaseNote', 'individual_id', 'id')->orderBy('note_date', 'DESC')->limit(10);
    }
	
	public function casenote_today_current_user()
	{
		return $this->hasOne('App\Models\ProviderIndividualCaseNote', 'individual_id', 'id')->where('user_id', Auth::user()->id)->where('note_date', date('Y-m-d'))->orderBy('note_date', 'DESC')->limit(1);
	}
	
	public function report_casenotes()
	{
		return $this->hasMany('App\Models\ProviderIndividualCaseNote', 'individual_id', 'id')->with('user')->orderBy('note_date', 'DESC');
    }
	
	public function current_house()
    {
        return $this->belongsToMany('App\Models\ProviderHouse', 'tblProviderHouseIndividual', 'individual_id', 'house_id')->whereNull('tblProviderHouseIndividual.deleted_at');
	}
	
	public function field_checklists()
	{
		return $this->hasMany('App\Models\ProviderIndividualFormFieldChecklist', 'individual_id', 'id')->orderBy('date_complete', 'DESC');
	}

	public function quarterly_summaries()
	{
		return $this->hasMany('App\Models\ProviderIndividualFormQuarterlySummary', 'individual_id', 'id')->orderBy('date_complete', 'DESC');
	}
	
	public function houses()
    {
        return $this->belongsToMany('App\Models\ProviderHouse', 'tblProviderHouseIndividual', 'individual_id', 'house_id');
	}
	
	public function medical_appointments()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicalAppointment', 'individual_id', 'id');
	}
	
	public function medications()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedication', 'individual_id', 'id');
	}
	
	public function goals()
	{
		return $this->hasMany('App\Models\ProviderIndividualGoal', 'individual_id', 'id');
    }
    
    public function strategies()
	{
		return $this->hasMany('App\Models\ProviderIndividualStrategy', 'individual_id', 'id');
	}

	public function seizures()
	{
		return $this->hasMany('App\Models\ProviderIndividualSeizure', 'individual_id', 'id');
	}
	
	public function risk_plans()
	{
		return $this->hasMany('App\Models\ProviderIndividualRiskPlan', 'individual_id', 'id');
	}

	public function support_team()
	{
		return $this->hasMany('App\Models\ProviderIndividualSupportTeam', 'individual_id', 'id');
	}
	
	public function retired_medications()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedication', 'individual_id', 'id')->withTrashed()->whereNotNull('deleted_at');
	}
	
	public function medication_logs()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicationLog', 'individual_id', 'id');
    }
    
    public function graphs()
	{
		return $this->hasMany('App\Models\ProviderIndividualGraph', 'individual_id', 'id');
	}
	
	public function missed_meds($start_date=null, $end_date=null)
	{
		$this->missed = collect([]);
		$today = Carbon::now();
		if($start_date == null) {
			$start_date = Carbon::now();
		}
		if($end_date == null || $end_date > date('Y-m-d')) {
			$end_date = Carbon::now();
		}
		
		//loop over each day
		while($end_date >= $start_date) {
			
			//each medication
			foreach($this->medications AS $medication) {
				//each medication schedules
				foreach($medication->schedules AS $schedule) {
					//if the schedule lines up with the end_date
					
					if($schedule->day_of_week == date('N', strtotime($end_date))) {
						//check the log too see if it was taken on the day
						$logs = $medication->logs;
						$missed = true;

						foreach($logs AS $log) {
							$log->day_of_week--;
							if($log->day_of_week == 0) {
								$log->day_of_week = 7;
							}
							
							//3600 is one hour changed to 1800 for 30 minutes on each side
							if(date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) - 1800) <= date('YmdHis', strtotime($log->administered_dt)) && 
							   date('YmdHis', strtotime($log->administered_dt)) <= date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) + 1800)) {
								$missed = false;
								$schedule->logged = $log->administered_dt;
							}
						}
						if($missed == true && date('YmdHis', strtotime($today)) >= date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) + 1800)) {
							$this->missed->push(collect(['medication' => $medication, 'day' => date('m/d/Y', strtotime($end_date)), 'time' => date('h:i A', strtotime($schedule->time))]));// 'time' => $schedule->time);
						}
						
					}
				}
			}
			$end_date = $end_date->subDays(1); //subtract 1 day so we don't loop forever
		}
	}

	public function medications_with_missed($start_date=null, $end_date=null)
	{
		$this->missed = collect([]);
		$today = Carbon::now();
		if($start_date == null) {
			$start_date = Carbon::now();
		}
		if($end_date == null || $end_date > date('Y-m-d')) {
			$end_date = Carbon::now();
		}
		
		//loop over each day
		while($end_date >= $start_date) {
			
			//each medication
			foreach($this->medications AS $medication) {
				//each medication schedules
				foreach($medication->today_times AS $schedule) {
					//if the schedule lines up with the end_date
					
					if($schedule->day_of_week == date('N', strtotime($end_date))) {
						//check the log too see if it was taken on the day
						$logs = $medication->logs;
						$missed = true;

						foreach($logs AS $log) {
							$log->day_of_week--;
							if($log->day_of_week == 0) {
								$log->day_of_week = 7;
							}
							
							//3600 is one hour changed to 1800 for 30 minutes on each side
							if(date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) - 1800) <= date('YmdHis', strtotime($log->administered_dt)) && 
							   date('YmdHis', strtotime($log->administered_dt)) <= date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) + 1800)) {
								$schedule->logged = $log->administered_dt;
								$missed = false;
							}
						}
						if($missed == true && date('YmdHis', strtotime($today)) >= date('YmdHis', strtotime($end_date->format('Y-m-d') . ' ' . $schedule->time) + 1800)) {
							$schedule->missed = true;
						}
						
					}
				}
			}
			$end_date = $end_date->subDays(1); //subtract 1 day so we don't loop forever
		}
	}
}
