<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualMedicationSchedule extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualMedicationSchedule';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'medication_id', 'individual_id', 'day_of_week', 'time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
		
	public function medication()
    {
        return $this->hasOne('App\Models\ProviderIndividualMedication', 'id', 'medication_id');
    }
	
	public function last_taken()
	{
		return $this->hasOne('App\Models\ProviderIndividualMedicationLog', 'medication_id', 'medication_id')
					->orderBy('administered_dt', 'DESC');
	}
	
	public function taken_on_day()
	{	
		return $this->hasMany('App\Models\ProviderIndividualMedicationLog', 'medication_id', 'medication_id')
					->where(DB::raw('DAYOFWEEK(administered_dt)'), '=', $this->day_of_week)
					->whereTime('administered_dt', '>=', date('H:i:s', strtotime($this->time) - 36000))
					->whereTime('administered_dt', '<=', date('H:i:s', strtotime($this->time) + 36000))
					->orderBy('administered_dt', 'DESC');
	}
	
	public function getWeekDayAttribute()
	{
		$weekday = "";
		switch($this->day_of_week) {
			case 1:
				$weekday = "Monday";
			break;
			case 2:
				$weekday = "Tuesday";
			break;
			case 3:
				$weekday = "Wednesday";
			break;
			case 4:
				$weekday = "Thursday";
			break;
			case 5:
				$weekday = "Friday";
			break;
			case 6:
				$weekday = "Saturday";
			break;
			case 7:
				$weekday = "Sunday";
			break;
		}
		return "{$weekday}";
	}
}
