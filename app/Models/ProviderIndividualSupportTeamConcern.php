<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualSupportTeamConcern extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualSupportTeamConcern';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date_of_concern'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function getFullNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function team_member()
	{
		return $this->hasOne('App\Models\ProviderIndividualSupportTeam', 'id', 'support_team_id');
    }
    
}
