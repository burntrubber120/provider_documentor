<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
	use SoftDeletes;
	
	protected $table = 'tblUser';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'tblUserRole', 'user_id', 'role_id')->withPivot('id', 'created_at', 'deleted_at')->whereNull('tblUserRole.deleted_at');
    }
	
	public function hasGroup($name)
    {
        if(is_array($name)) {
            foreach($name as $roleName) {
                $hasRole = $this->hasGroup($roleName);
				if($hasRole) return true;
			}
            
        } else {
			//dd($this);
            foreach ($this->roles as $role) {
				//dd($role);
                if ($role->group == $name && NULL === $role->pivot->deleted_at) {
                    return true;
                }
            }
        }
        
        return false;
    }
	
	public function hasGroupType($group, $type)
    {       
		if(is_array($type)) {
            foreach($type as $roleType) {
                $hasType = $this->hasGroupType($group, $roleType);
				if($hasType) return true;
			}
            
        } else {
			foreach ($this->roles as $role) {
				if($role->group == $group && $role->type == $type && NULL === $role->pivot->deleted_at) {
					return true;
				}
			}
		}

        
        return false;
    }
	
	public function provider()
    {
        return $this->hasOne('App\Models\Provider', 'id', 'provider_id');
    }
	
	public function current_houses()
    {
        return $this->belongsToMany('App\Models\ProviderHouse', 'tblProviderHouseUser', 'user_id', 'house_id')->whereNull('tblProviderHouseUser.deleted_at');
    }
	
	public function houses()
    {
        return $this->belongsToMany('App\Models\ProviderHouse', 'tblProviderHouseUser', 'user_id', 'house_id');
    }
    
    public function trainings()
    {
        return $this->belongsToMany('App\Models\ProviderTraining', 'tblProviderTrainingUser', 'user_id', 'training_id')
                        ->withPivot('score', 'created_at')
                        ->whereNull('tblProviderTrainingUser.deleted_at');
    }

    public function files()
    {
        return $this->belongsToMany('App\Models\FileUpload', 'tblFileUploadUser', 'user_id', 'file_upload_id')
                    ->withPivot('start_date', 'end_date', 'file_type')
                    ->whereNull('tblFileUploadUser.deleted_at');
    }

    public function time_punches()
    {
        return $this->hasMany('App\Models\UserTimePunch', 'user_id', 'id')->orderBy('time_punch', 'DESC');
    }

    public function getClockedInOutAttribute()
    {
        return $this->time_punches->first();
    }

    public function calculate_hours()
    {
        $this->punched_hours = collect([]);
        if($this->time_punches->count() > 0) {
            $check_punch = false;
            foreach($this->time_punches AS $punch) {
                
                if($punch->punch == 'out') {
                    $check_punch = true;
                    $single_punch = collect([]);
                    $single_punch->put('punch_out', $punch->time_punch);
                }
                if($punch->punch == 'in' && $check_punch == true) {
                    $check_punch = false;
                    $single_punch->put('punch_in', $punch->time_punch);
                    $single_punch->put('hours', $single_punch['punch_out']->diff($punch->time_punch)->format('%H:%I'));
                    $this->punched_hours->push($single_punch);
                }
            }
        }
    }
}
