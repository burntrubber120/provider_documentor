<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualMedicationLog extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualMedicationLog';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'administered_dt'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'medication_id', 'individual_id', 'user_id', 'administered_dt', 'note',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function medication()
    {
        return $this->hasOne('App\Models\ProviderIndividualMedication', 'id', 'medication_id')->withTrashed();
    }
}
