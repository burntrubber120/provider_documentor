<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderHouseUser extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderHouseUser';
	
	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
        'provider_id', 'user_id', 'house_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
}
