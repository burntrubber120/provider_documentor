<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualCaseNote extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualCaseNote';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'note_date', 'start_dt', 'end_dt'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }

    public function individual()
    {
        return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
    }
	
	public function edited_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function service()
	{
		return $this->hasOne('App\Models\Service', 'id', 'service_id');
	}
	
	public function goal()
	{
		return $this->hasOne('App\Models\ProviderIndividualGoal', 'id', 'goal_id');
	}
	
	public function strategy()
	{
		return $this->hasOne('App\Models\ProviderIndividualStrategy', 'id', 'strategy_id');
	}
	
}
