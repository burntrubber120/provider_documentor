<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualGraph extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualGraph';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'plan_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function edited_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
	
	public function individual()
	{
		return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
	}
    
    public function graph_data()
	{
		return $this->hasMany('App\Models\ProviderIndividualGraphData', 'graph_id', 'id')->orderBy('data_date', 'ASC');
	}
}
