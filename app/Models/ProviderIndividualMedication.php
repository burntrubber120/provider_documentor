<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualMedication extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualMedication';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'fill_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'name', 'dosage', 'user_id', 'individual_id', 'type', 'number_doses', 'note', 'fill_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }
	
	public function today_times()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicationSchedule', 'medication_id', 'id')->where('day_of_week', date('N'))->orderBy('time');
	}
	
	public function schedules()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicationSchedule', 'medication_id', 'id')->orderBy('day_of_week')->orderBy('time');
	}
	
	public function last_taken()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicationLog', 'medication_id', 'id')->orderBy('administered_dt', 'DESC')->limit(1);
	}
	
	public function logs()
	{
		return $this->hasMany('App\Models\ProviderIndividualMedicationLog', 'medication_id', 'id')
					->selectRaw('tblProviderIndividualMedicationLog.*, DAYOFWEEK(tblProviderIndividualMedicationLog.administered_dt) AS day_of_week')
					->orderBy('administered_dt', 'DESC');
	}
}
