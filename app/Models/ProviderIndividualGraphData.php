<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderIndividualGraphData extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderIndividualGraphData';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'data_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
	
	public function graph()
    {
        return $this->hasOne('App\Models\ProviderIndividualGraph', 'id', 'graph_id');
    }
	
	
}
