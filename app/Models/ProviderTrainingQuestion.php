<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderTrainingQuestion extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderTrainingQuestion';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $fillable = [
		'training_id', 'question', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function answers()
    {
        return $this->hasMany('App\Models\ProviderTrainingQuestionAnswer', 'question_id', 'id');
    }
}
