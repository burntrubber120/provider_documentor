<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderTrainingUser extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblProviderTrainingUser';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id')->withTrashed();
    }

    public function individual()
    {
        return $this->hasOne('App\Models\ProviderIndividual', 'id', 'individual_id');
    }

    public function training()
    {
        return $this->hasMany('App\Models\ProviderTraining', 'id', 'training_id');
    }
}
