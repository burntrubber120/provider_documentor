<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileUpload extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblFileUpload';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'start_date', 'end_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'uploaded_by_id')->withTrashed();
    }

}
