<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTimePunch extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblUserTimePunch';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'time_punch'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

}
