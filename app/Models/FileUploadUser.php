<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileUploadUser extends Model
{
	use SoftDeletes;
	
	protected $table = 'tblFileUploadUser';
	
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'start_date', 'end_date'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */	
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       
    ];
    
    public function file()
    {
        return $this->hasOne('App\Models\FileUpload', 'id', 'file_upload_id');
    }

}
