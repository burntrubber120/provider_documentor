<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ProviderHouse;


class HouseController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | House Controller
    |--------------------------------------------------------------------------
    |
    */

    public function getHouseList()
    {
		$houses = ProviderHouse::with('employees', 'individuals', 'safety_checklists')->where('provider_id', Auth::user()->provider_id)->get();
		return view('house.list', ['houses' => $houses]);
    }
	
	public function getAddHouse()
	{
		return view('house.add');
	}
	
	public function postAddHouse(Request $request)
	{
		$this->validate($request, [
			'address' => 'required|max:50',
			'city' => 'required|max:50',
			'state' => 'required|max:50',
			'zip' => 'required|max:5'
		]);
		
		$input = $request->all();
				
		$house = ProviderHouse::create([
            'provider_id' => Auth::user()->provider_id,
            'nickname' => $input['nickname'],
            'address' => $input['address'],
            'city' => $input['city'],
            'state' => $input['state'],
            'zip' => $input['zip'],
            'phone' => $input['phone']
        ]);
		//$this->dispatch(new SendActivationEmail($user, $password_clear));
		return redirect()->to('/house/list')->with('message', 'House Added.');
	}
	
	public function getEditHouse($id)
	{
		$house = ProviderHouse::where('id', decrypt($id))->firstOrFail();
		return view('house.edit', ['house' => $house]);
	}
	
	public function postEditHouse(Request $request, $id)
	{
		$this->validate($request, [
			'address' => 'required|max:50',
			'city' => 'required|max:50',
			'state' => 'required|max:50',
			'zip' => 'required|max:5'
		], array());
		
		$input = $request->all();
				
		$house = ProviderHouse::where('id', decrypt($id))->firstOrFail();
		$house->nickname = $input['nickname'];
		$house->address = $input['address'];
		$house->city = $input['city'];
		$house->state = $input['state'];
		$house->zip = $input['zip'];
		$house->phone = $input['phone'];
		$house->save();
		
		return redirect()->to('house/list')->with('message', 'House Updated.');
	}
	
	public function getViewHouse($id)
	{
		$house = ProviderHouse::with('employees')->with('individuals')->where('id', decrypt($id))->firstOrFail();
		
		return view('house.view', ['house' => $house]); 
	}
	
    public function getHouseSchedule($id)
    {
        $color_array = array('bg-purple', 'bg-red', 'bg-blue', 'bg-orange', 'bg-light-blue', 'bg-light-purple');
        $house = ProviderHouse::with('employees')->with('individuals')->where('id', decrypt($id))->firstOrFail();

        return view('house.schedule', ['house' => $house, 'color_array' => $color_array]);
    }
}
