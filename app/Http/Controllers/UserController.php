<?php

namespace App\Http\Controllers;

use Auth;
use Uuid;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserTimePunch;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\FileUpload;
use App\Models\FileUploadUser;
use App\Models\ProviderHouse;
use App\Models\ProviderHouseUser;
use App\Jobs\SendActivationEmail;


class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    */
	
	public function getProfile($id=NULL)
	{
		if($id == NULL) {
			$user = User::where('id', Auth::id())->firstOrFail();
		}
		else {
			$user = User::where('id', decrypt($id))->firstOrFail();
		}
		return view('user.profile', ['user' => $user]);
	}
	
	public function postProfilePassword(Request $request)
	{
		$input = $request->all();
		
		$this->validate($request, [
			'current_password' => 'required',
			'new_password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/|same:confirm_password',
		], array('new_password.regex' => 'Password must have at least one Upper Case, Lower Case, and Number.', 'new_password.min' => 'Password must be at least 6 characters long',
		'new_password.required' => 'Please enter a new password.', 'new_password.same' => 'The passwords do not match.', 'current_password.required' => 'Password is required.'));
		
		$user = User::where('id', Auth::id())->firstOrFail();
		
		$info = [
			'username' => Auth::user()->username,
			'password' => $input['current_password'],
		];
		$message = 'Password Updated';
		if (Auth::attempt($info)) {
			$user->password = bcrypt($input['new_password']);
			$user->save();
		}
		else {
			return back()->with('error', 'Current Password could not be validated.');
		}
		
		return back()->with('message', 'Password Updated');
	}
    
	public function getUserList()
	{
		$users = User::where('provider_id', Auth::user()->provider->id)->get();
		return view('user.list', ['users' => $users]);
	}
	
	public function getAddUser()
	{
		return view('user.add');
	}
	
	public function postAddUser(Request $request)
	{
		$this->validate($request, [
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'username' => 'required|unique:tblUser|max:50',
			'email' => 'email|nullable|max:255'
		], array('username.unique' => 'That username is already taken.'));
		
		$input = $request->all();
		
		$password_phrase = '';
		$password_clear = $this->generate_password();
        $password_hash = bcrypt($password_clear);
		$password_phrase .= ' -  User password set to:   '.$password_clear;
		
		$user = User::create([
			'provider_id' => Auth::user()->provider_id,
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'username' => $input['username'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'password' => $password_hash
        ]);
		//$this->dispatch(new SendActivationEmail($user, $password_clear));
		return redirect()->back()->with('message', 'User Added.  ' . $password_phrase);
    }
    
    public function getRemoveUser($id)
	{
        if(Auth::user()->hasGroupType('site','admin')) {
            $user = User::where('id', decrypt($id))->delete();
        }
        else {
            $user = User::where('provider_id', Auth::user()->id)->where('id', decrypt($id))->delete();
        }
             
		return back()->with('message', 'User Removed');
	}
	
	public function getEditUser($id)
	{
        $user = User::with('roles', 'current_houses', 'files')->where('id', decrypt($id))->firstOrFail();
        
		$user_houses = $user->current_houses->keyBy('id');		
		$houses = ProviderHouse::where('provider_id', Auth::user()->provider_id)->get();
        $roles = Role::whereNotIn('id', $user->roles->pluck('id'))->where('group', 'provider')->get();
        $file_types = array('CPR Certificate' => 'CPR Certificate', 'Driver License' => 'Driver License', 
                            'Drug Test' => 'Drug Test', 'Background Check' => 'Background Check');
		return view('user.edit', ['user' => $user, 'user_houses' => $user_houses, 'houses' => $houses, 'roles' => $roles, 'file_types' => $file_types]);
	}
	
	public function postEditUser(Request $request, $id)
	{
		$this->validate($request, [
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'email' => 'email|nullable|max:255'
		], array());
		
		$input = $request->all();
		
		$password_phrase = '';
		if(isset($input['reset_password'])) {
			$password_clear = $this->generate_password();
			$password_hash = bcrypt($password_clear);
			$password_phrase .= ' -  User password set to:   '.$password_clear;
		}
		
		$user = User::where('id', decrypt($id))->firstOrFail();
		$user->first_name = $input['first_name'];
		$user->last_name = $input['last_name'];
		$user->email = $input['email'];
		$user->phone = $input['phone'];
		$user->save();
		
		return redirect()->to('user/list')->with('message', 'User Updated.  ' . $password_phrase);
	}
	
	public function postEditUserHouse(Request $request, $id)
	{
		$input = $request->all();
		
		ProviderHouseUser::where('user_id', decrypt($id))->where('provider_id', Auth::user()->provider_id)->delete();
		
		if(isset($input['house_id']) && count($input['house_id']) > 0) {
			foreach($input['house_id'] AS $house_id) {
				$user = ProviderHouseUser::create([
					'provider_id' => Auth::user()->provider_id,
					'user_id' => decrypt($id),
					'house_id' => decrypt($house_id)
				]);
			}
		}
		return redirect()->back()->with('message', 'Houses Updated.');
	}
	
	public function getAddUserPermission($id)
	{
		$user = User::with('roles')->where('id', decrypt($id))->firstOrFail();
		$roles = Role::whereNotIn('id', $user->roles->pluck('id'))->where('group', 'provider')->get();
		return view('user.permission', ['user' => $user, 'roles' => $roles]);
	}
	
	public function postAddUserPermission(Request $request)
	{
		$this->validate($request, [
			'permission' => 'required'
		], array('permission' => 'That username is already taken.'));
		
		$input = $request->all();
		
		$user = User::where('id', decrypt($input['user_id']))->firstOrFail();
		
		$userrole = UserRole::create([
            'user_id' => $user->id,
            'role_id' => decrypt($input['permission'])
        ]);
		
		return redirect()->back()->with('message', 'Permission Added.');
	}
	
	public function getRemoveUserPermission($id, $ur_id)
	{
		$user = User::where('id', decrypt($id))->firstOrFail();
		UserRole::where('user_id', $user->id)->where('id', decrypt($ur_id))->delete();
		return back()->with('message', 'Permission Removed');
	}
	
	public function postFileUser(Request $request, $id)
	{
        
        if($request->file('file')) {
            $provider_name = str_replace(array(' ', "'", '!', '"', '%', '*', '(', ')', '#', '@', '/', '\\', ':', ';'), '', Auth::user()->provider->provider_name);
            $date = str_replace('-', '/', date('Y-m-d'));
            $target_dir = base_path()."/../uploads/$provider_name/".$date.'/';
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0775, true);
            }
            
            $file = $request->file('file');
                        
            $uuid = Uuid::generate()->string;
            $file_ext = strtolower(pathinfo($file,PATHINFO_EXTENSION));
            //move_uploaded_file($tmp_name, $name);
            if(!$file->move($target_dir, $uuid)) {
                //file failed to move
            }

            $file_upload = FileUpload::create([
                'uploaded_by_id' => Auth::id(),
                'uuid'           => $uuid,
                'extension'      => $file->getClientOriginalExtension(),
                'file_name'      => $file->getClientOriginalName(),
                'upload_path'    => $target_dir,
            ]);
            
            $user = User::where('id', decrypt($request->user_id))->firstOrFail();
            FileUploadUser::create([                
                'user_id'           => $user->id,
                'file_upload_id'    => $file_upload->id,
                'file_type'          => $request->file_type,
                'start_date'        => Carbon::parse($request->start_date),
                'end_date'          => Carbon::parse($request->end_date),
            ]);
        }
        //echo json_encode($result);
        return redirect()->back()->with('message', 'File Added.');
    }
    
    public function getRemoveUserFileUpload($id, $ur_id)
	{
		$user = User::where('id', decrypt($id))->firstOrFail();
		FileUpload::where('id', decrypt($ur_id))->delete();
		FileUploadUser::where('user_id', $user->id)->where('file_upload_id', decrypt($ur_id))->delete();
		return back()->with('message', 'File Removed');
    }
    
    public function getClockInOut($punch, $user_id=null)
	{
        if($user_id) {
            $user = User::where('provider_id', Auth::user()->provider_id)->where('id', decrypt($id))->firstOrFail();
            if($punch === 'in' || $punch === 'out') {
                $clocked_in_out = $punch;
            } else {
                return back()->with('message', 'Bad Punch');
            }
        } else {
            $user = Auth::user();
            $clocked_in_out = 'out';//default value out
            
            if(!Auth::user()->clocked_in_out || (Auth::user()->clocked_in_out && Auth::user()->clocked_in_out->punch == 'out')) {
                $clocked_in_out = 'in';//if already clocked out clock in
            }
        }
        UserTimePunch::create([                
            'user_id'           => $user->id,
            'provider_id'       => $user->provider_id,
            'punch_by_id'       => Auth::user()->id,
            'punch'             => $clocked_in_out,
            'time_punch'        => Carbon::now()
        ]);
        return back()->with('message', 'Clocked '.$clocked_in_out);
	}
	
	public function generate_password($length = 6, $add_dashes = false, $available_sets = 'ud')
    {
        $sets = array();
        
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        
        $all = '';
        $password = '';
        
        foreach($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        
        $all = str_split($all);
        
        for($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
            
        $password = str_shuffle($password);
        
        if(!$add_dashes) {
            return $password;
        }
        
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        
        while(strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        
        $dash_str .= $password;
        return $dash_str;
    }
}
