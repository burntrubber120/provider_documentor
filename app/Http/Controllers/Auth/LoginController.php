<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('anyLogout');
    }
	
	public function getLogin() {
		return view('auth.login');
	}
	
	public function postLogin(Request $request)
	{
		
		$input = $request->all();
		$user = [
			'username' => $input['username'],
			'password' => $input['password'],
		];
		if (Auth::attempt($user)) {
			if(Auth::user()->has_reset) {
				return redirect('/create-password');
			} else {       
				return redirect()->intended($this->redirectTo)->with('message', 'You are successfully logged in.');
			}
		} else {
			return redirect('/login')
				->with('error', 'Your username/password combination was incorrect.')
				->withInput($request->except('password'));
		}
		
	}
	
	public function anyLogout()
	{
		Auth::logout();
		return redirect('/login')
				->with('message', 'You are successfully logged out.');
	}
}
