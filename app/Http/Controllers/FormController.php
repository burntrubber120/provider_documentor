<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Upload;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\FileUpload;
use App\Models\Service;
use App\Models\ProviderIndividual;
use App\Models\ProviderHouse;
use App\Models\ProviderHouseIndividual;
use App\Models\ProviderHouseUser;
use App\Models\ProviderIndividualFormFieldChecklist;
use App\Models\ProviderIndividualFormQuarterlySummary;
use App\Models\ProviderHouseFormSafetyChecklist;

class FormController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Form Controller
    |--------------------------------------------------------------------------
    |
	*/
	public function __construct()
	{
		$this->yes_no = array("Yes" => "Yes", "No" => "No", "N/A" => "N/A");
		$this->drill_types = array("Fire Drill" => "Fire Drill", "Tornado Drill" => "Tornado Drill", "Gas Leak Drill" => "Gas Leak Drill", 
									"Earth Quake Drill" => "Earth Quake Drill", "Bomb Threat Drill" => "Bomb Threat Drill", "Other" => "Other");
		$this->has_needs = array("Has" => "Has", "Needs" => "Needs", "N/A" => "N/A");
		$this->years = array(date('Y')+1 => date('Y')+1, date('Y') => date('Y'), date('Y')-1 => date('Y')-1);
		$this->quarters = array(1 => 'Quarter 1 (1, 2, 3)', 2 => 'Quarter 2 (4, 5, 6)', 3 => 'Quarter 3 (7, 8, 9)', 4 => 'Quarter 4 (10, 11, 12)');
	}
	
	/****************
	Start Individual Forms
	*************/
	public function getFieldChecklistAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$field_checklist = new ProviderIndividualFormFieldChecklist();
		return view('individual.forms.field-checklist.add', ['individual' => $individual, 'field_checklist' => $field_checklist, 'yes_no' => $this->yes_no, 'action' => $action]);
	}

	public function postFieldChecklistAdd(Request $request, $id, $checklist_id=null)
	{
		$this->validate($request, [
			'date_complete' => 'required|date'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		if($checklist_id == null) {
			$word = 'Added';//this is for the string for return
			$field_checklist = ProviderIndividualFormFieldChecklist::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'date_complete' => Carbon::parse($input['date_complete']),
				'money_count' => trim($input['money_count']),
				'receipt_match' => trim($input['receipt_match']),
				'review_done' => trim($input['review_done']),
				'controlled_med_count' => trim($input['controlled_med_count']),
				'controlled_med_count_match' => trim($input['controlled_med_count_match']),
				'buddy_checklist' => trim($input['buddy_checklist']),
				'chio_notes' => trim($input['chio_notes']),
				'time_sheets' => trim($input['time_sheets']),
				'transportation_logs' => trim($input['transportation_logs']),
				'cleaning' => trim($input['cleaning']),
				'office' => trim($input['office']),
				'medical' => trim($input['medical']),
				'bus_passes' => trim($input['bus_passes']),
				'five_day_med' => trim($input['five_day_med']),
				'check_dates' => trim($input['check_dates']),
				'prn_reorder' => trim($input['prn_reorder']),
				'double_locked' => trim($input['double_locked'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$field_checklist = ProviderIndividualFormFieldChecklist::where('id', decrypt($checklist_id))->firstOrFail();
			$field_checklist->date_complete = Carbon::parse($input['date_complete']);
			$field_checklist->money_count = trim($input['money_count']);
			$field_checklist->receipt_match = trim($input['receipt_match']);
			$field_checklist->review_done = trim($input['review_done']);
			$field_checklist->controlled_med_count = trim($input['controlled_med_count']);
			$field_checklist->controlled_med_count_match = trim($input['controlled_med_count_match']);
			$field_checklist->buddy_checklist = trim($input['buddy_checklist']);
			$field_checklist->chio_notes = trim($input['chio_notes']);
			$field_checklist->time_sheets = trim($input['time_sheets']);
			$field_checklist->transportation_logs = trim($input['transportation_logs']);
			$field_checklist->cleaning = trim($input['cleaning']);
			$field_checklist->office = trim($input['office']);
			$field_checklist->medical = trim($input['medical']);
			$field_checklist->bus_passes = trim($input['bus_passes']);
			$field_checklist->five_day_med = trim($input['five_day_med']);
			$field_checklist->check_dates = trim($input['check_dates']);
			$field_checklist->prn_reorder = trim($input['prn_reorder']);
			$field_checklist->double_locked = trim($input['double_locked']);
			$field_checklist->save();
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Field Coordinator Checklist ' . $word);
	}

	public function getFieldChecklistEdit($id, $checklist_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$field_checklist = ProviderIndividualFormFieldChecklist::where('id', decrypt($checklist_id))->firstOrFail();
		return view('individual.forms.field-checklist.add', ['individual' => $individual, 'field_checklist' => $field_checklist, 'yes_no' => $this->yes_no, 'action' => $action]);
	}

	public function getFieldChecklistView($id, $checklist_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$field_checklist = ProviderIndividualFormFieldChecklist::where('id', decrypt($checklist_id))->firstOrFail();
		return view('individual.forms.field-checklist.view', ['individual' => $individual, 'field_checklist' => $field_checklist, 'yes_no' => $this->yes_no]);
	}
	public function getFieldChecklistRemove($id, $checklist_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$field_checklist = ProviderIndividualFormFieldChecklist::where('individual_id', $individual->id)->where('id', decrypt($checklist_id))->delete();
		return redirect()->back()->with('message', 'Quarterly Summary Removed.');
	}

	public function getQuarterlySummaryAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$quarterly_summary = new ProviderIndividualFormQuarterlySummary();
		$services = Service::all();

		return view('individual.forms.quarterly-summary.add', ['individual' => $individual, 'years' => $this->years, 'quarters' => $this->quarters, 'yes_no' => $this->yes_no,
														 	   'quarterly_summary' => $quarterly_summary, 'services' => $services, 'action' => $action]);
	}

	public function postQuarterlySummaryAdd(Request $request, $id, $summary_id=null)
	{
		$this->validate($request, [
			'date_complete' => 'required|date',
			'noa_start_date' => 'required|date',
			'noa_end_date' => 'required|date',
			'pcisp_date' => 'required|date',
			'bsp_date' => 'required|date'
		], array());
		
		$input = $request->all();
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		if($summary_id == null) {
			$word = 'Added';//this is for the string for return
			$quarterly_summary = ProviderIndividualFormQuarterlySummary::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'date_complete' => Carbon::parse($input['date_complete']),
				'year' => $input['year'],
				'quarter' => $input['quarter'],
				'service_issues' => trim($input['service_issues']),
				'noa_start_date' => Carbon::parse($input['noa_start_date']),
				'noa_end_date' => Carbon::parse($input['noa_end_date']),
				'noa_reflect' => trim($input['noa_reflect']),
				'pcisp_date' => Carbon::parse($input['pcisp_date']),
				'bsp_date' => Carbon::parse($input['bsp_date']),
				'transportation_utilized' => trim($input['transportation_utilized']),
				'transportation_noa' => trim($input['transportation_noa']),
				'transportation_type' => trim($input['transportation_type']),
				'transportation_logs' => trim($input['transportation_logs']),
				'five_day_med' => trim($input['five_day_med']),
				'fire_extinguisher' => trim($input['fire_extinguisher']),
				'evac_posted' => trim($input['evac_posted']),
				'smoke_detector' => trim($input['smoke_detector']),
				'carbon_detector' => trim($input['carbon_detector']),
				'adaptive_equipment' => trim($input['adaptive_equipment']),
				'equipment_issues' => trim($input['equipment_issues']),
				'community_events' => trim($input['community_events']),
				'community_assessment' => trim($input['community_assessment']),
				'wellness_utilized' => trim($input['wellness_utilized']),
				'medical_concerns' => trim($input['medical_concerns']),
				'medication_responsibility' => trim($input['medication_responsibility']),
				'additional_notes' => trim($input['additional_notes'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$quarterly_summary = ProviderIndividualFormQuarterlySummary::where('id', decrypt($summary_id))->firstOrFail();
			$quarterly_summary->date_complete = Carbon::parse($input['date_complete']);
			$quarterly_summary->year = $input['year'];
			$quarterly_summary->quarter = $input['quarter'];
			$quarterly_summary->service_issues = trim($input['service_issues']);
			$quarterly_summary->noa_start_date = Carbon::parse($input['noa_start_date']);
			$quarterly_summary->noa_end_date = Carbon::parse($input['noa_end_date']);
			$quarterly_summary->noa_reflect = $input['noa_reflect'];
			$quarterly_summary->pcisp_date = Carbon::parse($input['pcisp_date']);
			$quarterly_summary->bsp_date = Carbon::parse($input['bsp_date']);
			$quarterly_summary->transportation_utilized = trim($input['transportation_utilized']);
			$quarterly_summary->transportation_noa = trim($input['transportation_noa']);
			$quarterly_summary->transportation_type = trim($input['transportation_type']);
			$quarterly_summary->transportation_logs = trim($input['transportation_logs']);
			$quarterly_summary->five_day_med = trim($input['five_day_med']);
			$quarterly_summary->fire_extinguisher = trim($input['fire_extinguisher']);
			$quarterly_summary->evac_posted = trim($input['evac_posted']);
			$quarterly_summary->smoke_detector = trim($input['smoke_detector']);
			$quarterly_summary->carbon_detector = trim($input['carbon_detector']);
			$quarterly_summary->adaptive_equipment = trim($input['adaptive_equipment']);
			$quarterly_summary->equipment_issues = trim($input['equipment_issues']);
			$quarterly_summary->community_events = trim($input['community_events']);
			$quarterly_summary->community_assessment = trim($input['community_assessment']);
			$quarterly_summary->wellness_utilized = trim($input['wellness_utilized']);
			$quarterly_summary->medical_concerns = trim($input['medical_concerns']);
			$quarterly_summary->medication_responsibility = trim($input['medication_responsibility']);
			$quarterly_summary->additional_notes = trim($input['additional_notes']);
			$quarterly_summary->save();
		}
		//attach individual to the checklist
		$quarterly_summary->services()->detach();//detach any if there are some
		if(isset($input['month_one_services'])) {
			foreach($input['month_one_services'] AS $service) {
				$quarterly_summary->services()->attach($service, ['month' => 1]);
			}
		}
		if(isset($input['month_two_services'])) {
			foreach($input['month_two_services'] AS $service) {
				$quarterly_summary->services()->attach($service, ['month' => 2]);
			}
		}
		if(isset($input['month_three_services'])) {
			foreach($input['month_three_services'] AS $service) {
				$quarterly_summary->services()->attach($service, ['month' => 3]);
			}
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Quarterly Service Summary ' . $word);
	}
	
	public function getQuarterlySummaryEdit($id, $summary_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$quarterly_summary = ProviderIndividualFormQuarterlySummary::where('id', decrypt($summary_id))->firstOrFail();
		$services = Service::all();
		return view('individual.forms.quarterly-summary.add', ['individual' => $individual, 'years' => $this->years, 'quarters' => $this->quarters, 'yes_no' => $this->yes_no,
																'quarterly_summary' => $quarterly_summary, 'services' => $services, 'action' => $action]);
	}

	public function getQuarterlySummaryView($id, $summary_id)
	{
		$quarterly_summary = ProviderIndividualFormQuarterlySummary::where('id', decrypt($summary_id))->firstOrFail();		
		switch($quarterly_summary->quarter) {
			case 1:
				$start = Carbon::parse($quarterly_summary->year.'-01-01');
				$end = Carbon::parse($quarterly_summary->year.'-03-31');
			break;
			case 2:
				$start = Carbon::parse($quarterly_summary->year.'-04-01');
				$end = Carbon::parse($quarterly_summary->year.'-06-30');
			break;
			case 3:
				$start = Carbon::parse($quarterly_summary->year.'-07-01');
				$end = Carbon::parse($quarterly_summary->year.'-09-30');
			break;
			case 4:
				$start = Carbon::parse($quarterly_summary->year.'-10-01');
				$end = Carbon::parse($quarterly_summary->year.'-12-31');
			break;
			default:
				$start = Carbon::parse($quarterly_summary->year.'-01-01');
				$end = Carbon::parse($quarterly_summary->year.'-03-31');
			break;
		}
		
		$individual = ProviderIndividual::with('risk_plans', 'support_team.concerns')
										->with(['medical_appointments' => function($query) use($start, $end) {
											$query->whereBetween('appointment_date', array($start, $end));
										}])
										->with(['medications' => function($query) use($start, $end) {
											$query->whereBetween('fill_date', array($start, $end));
										}])
										->with(['retired_medications' => function($query) use($start, $end) {
											$query->whereBetween('deleted_at', array($start, $end));
										}])
										->with(['seizures' => function($query) use($start, $end) {
											$query->whereBetween('seizure_date', array($start, $end));
										}])
										->with(['support_team.concerns' => function($query) use($start, $end) {
											$query->whereBetween('date_of_concern', array($start, $end));
										}])
										->where('id', decrypt($id))->firstOrFail();
		$services = Service::all();
		return view('individual.forms.quarterly-summary.view', ['individual' => $individual, 'years' => $this->years, 'quarters' => $this->quarters, 'yes_no' => $this->yes_no,
																'quarterly_summary' => $quarterly_summary, 'services' => $services]);
	}
	public function getQuarterlySummaryRemove($id, $summary_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$quarterly_summary = ProviderIndividualFormQuarterlySummary::where('individual_id', $individual->id)->where('id', decrypt($summary_id))->delete();
		return redirect()->back()->with('message', 'Quarterly Summary Removed.');
	}
	/****************
	End Individual Forms
	*************/

	/****************
	Start House Forms
	*************/
	public function getSafetyChecklistAdd($house_id)
	{
		$action = 'Add';
		$house = ProviderHouse::with('individuals')->where('id', decrypt($house_id))->firstOrFail();
		$safety_checklist = new ProviderHouseFormSafetyChecklist();
		return view('house.safety-checklist.add', ['house' => $house, 'drill_types' => $this->drill_types, 'has_needs' => $this->has_needs, 
													'safety_checklist' => $safety_checklist, 'action' => $action]);
	}

	public function postSafetyChecklistAdd(Request $request, $house_id, $checklist_id=null)
	{
		$this->validate($request, [
			'drill_type' => 'required',
			'drill_date' => 'required|date',
			'drill_start_time' => 'required|date_format:g:i A',
			'drill_end_time' => 'required|date_format:g:i A|after:drill_start_time',
			'individuals' => 'required'
		], array());
		
		$input = $request->all();
		$house = ProviderHouse::where('id', decrypt($house_id))->firstOrFail();
		if($checklist_id == null) {
			$word = 'Added';//this is for the string for return
			$safety_checklist = ProviderHouseFormSafetyChecklist::create([
				'user_id' => Auth::user()->id,
				'house_id' => $house->id,
				'drill_date' => Carbon::parse($input['drill_date']),
				'drill_start_time' => Carbon::parse($input['drill_start_time'])->format('H:i:s'),
				'drill_end_time' => Carbon::parse($input['drill_end_time'])->format('H:i:s'),
				'drill_type' => $input['drill_type'] != 'Other' ? $input['drill_type'] : $input['drill_type_other'],
				'before_drill' => trim($input['before_drill']),
				'during_drill' => trim($input['during_drill']),
				'after_drill' => trim($input['after_drill']),
				'fire_extinguisher' => trim($input['fire_extinguisher']),
				'fire_extinguisher_tag' => trim($input['fire_extinguisher_tag']),
				'co_detector' => trim($input['co_detector']),
				'water_temp_lowered' => trim($input['water_temp_lowered']),
				'flash_light' => trim($input['flash_light']),
				'chemicals_locked' => trim($input['chemicals_locked']),
				'smoke_detectors' => trim($input['smoke_detectors']),
				'first_aid_kit' => trim($input['first_aid_kit']),
				'precautions_kit' => trim($input['precautions_kit']),
				'evac_plan' => trim($input['evac_plan']),
				'phone_list' => trim($input['phone_list']),
				'fire_ladder' => trim($input['fire_ladder'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$safety_checklist = ProviderHouseFormSafetyChecklist::where('id', decrypt($checklist_id))->firstOrFail();
			$safety_checklist->drill_date = Carbon::parse($input['drill_date']);
			$safety_checklist->drill_start_time = Carbon::parse($input['drill_start_time'])->format('H:i:s');
			$safety_checklist->drill_end_time = Carbon::parse($input['drill_end_time'])->format('H:i:s');
			$safety_checklist->drill_type = $input['drill_type'] != 'Other' ? $input['drill_type'] : $input['drill_type_other'];
			$safety_checklist->before_drill = trim($input['before_drill']);
			$safety_checklist->during_drill = trim($input['during_drill']);
			$safety_checklist->after_drill = trim($input['after_drill']);
			$safety_checklist->fire_extinguisher = trim($input['fire_extinguisher']);
			$safety_checklist->fire_extinguisher_tag = trim($input['fire_extinguisher_tag']);
			$safety_checklist->co_detector = trim($input['co_detector']);
			$safety_checklist->water_temp_lowered = trim($input['water_temp_lowered']);
			$safety_checklist->flash_light = trim($input['flash_light']);
			$safety_checklist->chemicals_locked > trim($input['chemicals_locked']);
			$safety_checklist->smoke_detectors = trim($input['smoke_detectors']);
			$safety_checklist->first_aid_kit = trim($input['first_aid_kit']);
			$safety_checklist->precautions_kit = trim($input['precautions_kit']);
			$safety_checklist->evac_plan = trim($input['evac_plan']);
			$safety_checklist->phone_list = trim($input['phone_list']);
			$safety_checklist->fire_ladder = trim($input['fire_ladder']);
			$safety_checklist->save();
		}
		//attach individual to the checklist
		$safety_checklist->individuals()->detach();//detach any if there are some
		foreach($input['individuals'] AS $individual) {
			$test = ProviderHouseIndividual::where('house_id', $house->id)->where('individual_id', $individual)->firstOrFail();
			$safety_checklist->individuals()->attach($individual);
		}

		return redirect('/house/view/'.encrypt($house->id))->with('message', 'Safety Checklist ' . $word);
	}
	
	public function getSafetyChecklistEdit($house_id, $checklist_id)
	{
		$action = 'Edit';
		$house = ProviderHouse::with('individuals')->where('id', decrypt($house_id))->firstOrFail();
		$safety_checklist = ProviderHouseFormSafetyChecklist::where('id', decrypt($checklist_id))->firstOrFail();
		
		if(!in_array($safety_checklist->drill_type, $this->drill_types)) {
			$safety_checklist->drill_type_other = $safety_checklist->drill_type;
			$safety_checklist->drill_type = 'Other';
		}
		return view('house.safety-checklist.add', ['house' => $house, 'drill_types' => $this->drill_types, 'has_needs' => $this->has_needs, 
													'safety_checklist' => $safety_checklist, 'action' => $action]);
	}
	
	public function getSafetyChecklistView($house_id, $checklist_id)
	{
		$house = ProviderHouse::with('individuals')->where('id', decrypt($house_id))->firstOrFail();
		$safety_checklist = ProviderHouseFormSafetyChecklist::where('id', decrypt($checklist_id))->firstOrFail();
		return view('house.safety-checklist.view', ['house' => $house, 'safety_checklist' => $safety_checklist]);
	}

	public function getSafetyChecklistRemove($house_id, $checklist_id)
	{
		$house = ProviderHouse::with('individuals')->where('id', decrypt($house_id))->firstOrFail();
		$safety_checklist = ProviderHouseFormSafetyChecklist::where('house_id', $house->id)->where('id', decrypt($checklist_id))->delete();
		return redirect()->back()->with('message', 'Safety Checklist Removed');
	}
}
