<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Reportable;
use App\Models\Provider;
use App\Models\ProviderHouse;
use App\Models\ProviderHouseIndividual;
use App\Models\ProviderIndividual;
use App\Models\ProviderIndividualCaseNote;
use App\Models\ProviderIndividualGoal;
use App\Models\ProviderIndividualMedication;
use App\Models\ProviderIndividualMedicationLog;
use App\Models\ProviderIndividualMedicationSchedule;
use App\Models\ProviderIndividualStrategy;
use App\Models\ProviderIndividualQuarterlySummary;
use App\Models\ProviderIndividualMedicalAppointment;
use App\Models\ProviderIndividualSupportTeam;
use App\Models\ProviderIndividualSupportTeamConcern;
use App\Models\ProviderIndividualSeizure;
use App\Models\ProviderIndividualRiskPlan;
use App\Models\ProviderIndividualGraph;
use App\Models\ProviderIndividualGraphData;

use App\Models\Service;
use Carbon\Carbon;

class IndividualController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Individual Controller
    |--------------------------------------------------------------------------
    |
	*/
	public function __construct()
	{
		$this->yes_no = array("Yes" => "Yes", "No" => "No", "N/A" => "N/A");
		$this->member_types = array("Case Manager" => "Case Manager", "Family" => "Family", "Friend" => "Friend", "Behavior Specialist" => "Behavior Specialist", 
								"Day Program" => "Day Program");
		$this->plan_types = array("High Risk Plan" => "High Risk Plan", "Dining Plan" => "Dining Plan", "Seizure Plan" => "Seizure Plan", "Med Admin System" => "Med Admin System");
		
	}

    public function getIndividualList()
    {
		$individuals = ProviderIndividual::where('provider_id', Auth::user()->provider_id)->get();
		return view('individual.list', ['individuals' => $individuals]);
    }
	
	public function getAddIndividual()
	{
		$houses = ProviderHouse::where('provider_id', Auth::user()->provider_id)->get();
		return view('individual.add', ['houses' => $houses]);
	}
	
	public function postAddIndividual(Request $request)
	{
		$this->validate($request, [
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'ssn' => 'max:9'
		]);
		
		$input = $request->all();
				
		$individual = ProviderIndividual::create([
			'provider_id' => Auth::user()->provider_id,
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'ssn' => $input['ssn'] != '' ? encrypt($input['ssn']) : NULL,
            'dob' => isset($input['dob']) ? Carbon::parse($input['dob']) : NULL,
            'height' => $input['height'],
            'weight' => $input['weight'],
            'email' => $input['email'],
            'phone' => $input['phone'],
			'dod' => isset($input['dod']) ? Carbon::parse($input['dod']) : NULL,
			'about_me' => $input['about_me']
        ]);
		//$this->dispatch(new SendActivationEmail($user, $password_clear));
		return redirect()->to('/individual/list')->with('message', 'Individual Added.');
	}
	
	public function getEditIndividual($id)
	{
		$individual = ProviderIndividual::with('current_house')->where('id', decrypt($id))->firstOrFail();
		$houses = ProviderHouse::where('provider_id', Auth::user()->provider_id)->get();
		return view('individual.edit', ['individual' => $individual, 'houses' => $houses]);
	}
	
	public function postEditIndividual(Request $request, $id)
	{
		$this->validate($request, [
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'ssn' => 'max:9'
		], array());
		
		$input = $request->all();
				
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$individual->first_name = $input['first_name'];
		$individual->last_name = $input['last_name'];
		$individual->ssn = $input['ssn'] != '' ? encrypt($input['ssn']) : NULL;
		$individual->dob = isset($input['dob']) ? Carbon::parse($input['dob']) : NULL;
		$individual->height = $input['height'];
		$individual->weight = $input['weight'];
		$individual->email = $input['email'];
		$individual->phone = $input['phone'];
		$individual->about_me = $input['about_me'];
		$individual->dod = isset($input['dod']) ? Carbon::parse($input['dod']) : NULL;
		$individual->save();
		
		return redirect()->to('individual/list')->with('message', 'Individual Updated.');
	}
	
	public function postEditIndividualHouse(Request $request, $id)
	{
		$this->validate($request, [
			'house_id' => 'required',
		], array('house_id.required' => 'Please select a house.'));
		
		$input = $request->all();
		
		ProviderHouseIndividual::where('individual_id', decrypt($id))->where('provider_id', Auth::user()->provider_id)->delete();
		$new_house = ProviderHouseIndividual::create([
			'provider_id' => Auth::user()->provider_id,
            'individual_id' => decrypt($id),
            'house_id' => decrypt($input['house_id'])
        ]);
		
		return redirect()->back()->with('message', 'House Added.');
	}
	
	public function getViewIndividual($id)
	{
        $individual = ProviderIndividual::with('current_house','ten_casenotes.user','goals.strategies','field_checklists.user','quarterly_summaries.user', 'medical_appointments',
                                            'graphs.graph_data')
										->where('id', decrypt($id))->firstOrFail();
		$services = Service::get();
		$reportables = Reportable::get();
		return view('individual.view', ['individual' => $individual, 'services' => $services, 'reportables' => $reportables]); 
    }
    
    public function getAddCaseNote($id)
	{
		$individual = ProviderIndividual::with('goals.strategies')->where('id', decrypt($id))->firstOrFail();
		$case_note = new ProviderIndividualCaseNote();
		$action = 'Add';
		$services = Service::get();
        $reportables = Reportable::get();

        $location_value = array('Home', 'Other');
        $location_value = array_combine($location_value, $location_value);

        $qol_value = array('Bodily Integrity', 'Feeling Safe', 'Feeling Self-Worth' ,'Having a Life Structure', 'Sense of Belongingness', 
                            'Social Participation', 'Meaningful Daily Activities', 'Inner Contentment');
        $qol_value = array_combine($qol_value, $qol_value);
        $qol_plus_eoc = array('Interactions - Warm', 'Dialog - Listening', 'Protection - Warm Protection', 'Rewards - Unconditional', 'Companionship - Good', 'Engagement - Together',
                        'Purpose - Build Relationship', 'Focus - Flexible', 'Memories - New', 'Language - Concrete');
        $qol_plus_eoc = array_combine($qol_plus_eoc, $qol_plus_eoc);
        $qol_minus_eoc = array('Interactions - Cold', 'Dialog - Talking', 'Protection - Restraint', 'Rewards - Earned', 'Companionship - Poor', 'Engagement - Self',
                        'Purpose - Modify Behavior', 'Focus - Rigid', 'Memories - Old', 'Language - Abstract');
        $qol_minus_eoc = array_combine($qol_minus_eoc, $qol_minus_eoc);
        unset($this->yes_no['N/A']);

        $goal_ability_engagement = array('None (0%)', 'Minimal (25%)', 'Moderate (50%)', 'Substantial (75%)', 'Full (100%)');
        $goal_ability_engagement = array_combine($goal_ability_engagement, $goal_ability_engagement);

        return view('individual.casenote.edit', ['individual' => $individual, 'case_note' => $case_note, 'services' => $services, 'reportables' => $reportables, 'yes_no' => $this->yes_no,
                                                'location_value' => $location_value, 'action' => $action, 'qol_value' => $qol_value, 'qol_plus_eoc' => $qol_plus_eoc, 'qol_minus_eoc' => $qol_minus_eoc,
                                                'goal_ability_engagement' => $goal_ability_engagement]);
	}
		
	public function postAddCaseNote(Request $request, $id)
	{
		$this->validate($request, [
			'start_dt' => 'required|date',
			'end_dt' => 'required|date|after:start_dt',
			'note' => 'required',
			'strategy' => 'required'
		], array('start_dt.required' => 'Please select a start date and time.', 'start_dt.date' => 'Start Date/Time is not valid',
				'end_dt.required' => 'Please select a start date and time.', 'end_dt.date' => 'Start Date/Time is not valid', 'end_dt.after' => 'End Date/Time needs to be after Start Date/Time',
				'strategy.required' => 'Goal/Strategy is required.'));
		
		$input = $request->all();
		
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$strategy = ProviderIndividualStrategy::where('individual_id', $individual->id)->where('id', $input['strategy'])->firstOrFail();
		$service = isset($input['service']) ? $input['service'] : NULL;
		$reportable = isset($input['reportable']) ? $input['reportable'] : NULL;
        
        $location = $input['location'] == 'Other' ? $input['location_other'] : 'Home';

		$casenote = ProviderIndividualCaseNote::create([
            'user_id' => Auth::user()->id,
            'individual_id' => $individual->id,
			'service_id' => $service,
			'goal_id' => $strategy->goal_id,
			'strategy_id' => $strategy->id,
			
			'engaged' => $input['engaged'],
			'engaged_other' => $input['engaged_other'],
			'goal_duration' => $input['goal_duration'],
			'meaning_full' => $input['meaning_full'],
			'qol_value' => $input['qol_value'],
			'qol_plus_eoc' => $input['qol_plus_eoc'],
            'qol_minus_eoc' => $input['qol_minus_eoc'],
            'goal_ability' => $input['goal_ability'],            
			'goal_engagement' => $input['goal_engagement'],
			'reportable_id' => $reportable,
			'location' => $location,
			'note_date' => Carbon::parse($input['start_dt']),
			'start_dt' => Carbon::parse($input['start_dt']),
			'end_dt' => Carbon::parse($input['end_dt']),
			'case_note' => $input['note'],
			'priority' => $input['priority']
        ]);
		
		return redirect()->back()->with('message', 'Service Note Added.');
	}
	
	public function getEditCaseNote($id, $cn_id)
	{
		$individual = ProviderIndividual::with('goals.strategies', 'strategies.goal')->where('id', decrypt($id))->firstOrFail();
		$case_note = ProviderIndividualCaseNote::where('id', decrypt($cn_id))->firstOrFail();
		
		$no_edit = date('Y-m-d', strtotime('-2 day'));
		if($case_note->reject_date == NULL) {
            if($case_note->note_date < $no_edit) {
                return back()->with('message', 'The note is to old to edit');
            }
        }
		if($case_note->user_id != Auth::id() && !Auth::user()->hasGroupType('provider', 'Administrator')) {
			return back()->with('message', 'You do not have permission to edit this note');
        }
        $action = 'Edit';
		$services = Service::get();
        $reportables = Reportable::get();
        
        $location_value = array('Home', 'Other');
        $location_value = array_combine($location_value, $location_value);

        $qol_value = array('Bodily Integrity', 'Feeling Safe', 'Feeling Self-Worth' ,'Having a Life Structure', 'Sense of Belongingness', 
                            'Social Participation', 'Meaningful Daily Activities', 'Inner Contentment');
        $qol_value = array_combine($qol_value, $qol_value);
        $qol_plus_eoc = array('Interactions - Warm', 'Dialog - Listening', 'Protection - Warm Protection', 'Rewards - Unconditional', 'Companionship - Good', 'Engagement - Together',
                        'Purpose - Build Relationship', 'Focus - Flexible', 'Memories - New', 'Language - Concrete');
        $qol_plus_eoc = array_combine($qol_plus_eoc, $qol_plus_eoc);
        $qol_minus_eoc = array('Interactions - Cold', 'Dialog - Talking', 'Protection - Restraint', 'Rewards - Earned', 'Companionship - Poor', 'Engagement - Self',
                        'Purpose - Modify Behavior', 'Focus - Rigid', 'Memories - Old', 'Language - Abstract');
        $qol_minus_eoc = array_combine($qol_minus_eoc, $qol_minus_eoc);
        unset($this->yes_no['N/A']);

        $goal_ability_engagement = array('None (0%)', 'Minimal (25%)', 'Moderate (50%)', 'Substantial (75%)', 'Full (100%)');
        $goal_ability_engagement = array_combine($goal_ability_engagement, $goal_ability_engagement);
		return view('individual.casenote.edit', ['individual' => $individual, 'case_note' => $case_note, 'services' => $services, 'reportables' => $reportables, 'yes_no' => $this->yes_no,
        'location_value' => $location_value, 'action' => $action, 'qol_value' => $qol_value, 'qol_plus_eoc' => $qol_plus_eoc, 'qol_minus_eoc' => $qol_minus_eoc,
        'goal_ability_engagement' => $goal_ability_engagement]);
	}
	
	public function postEditCaseNote(Request $request, $id, $cn_id)
	{
		$this->validate($request, [
			'start_dt' => 'required|date',
			'end_dt' => 'required|date|after:start_dt',
			'note' => 'required',
			'strategy' => 'required'
		], array('start_dt.required' => 'Please select a start date and time.', 'start_dt.date' => 'Start Date/Time is not valid',
				'end_dt.required' => 'Please select a start date and time.', 'end_dt.date' => 'Start Date/Time is not valid', 'end_dt.after' => 'End Date/Time needs to be after Start Date/Time',
				'strategy.required' => 'Goal/Strategy is required.'));
		
		$input = $request->all();
		
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$case_note = ProviderIndividualCaseNote::where('id', decrypt($cn_id))->firstOrFail();
		$strategy = ProviderIndividualStrategy::where('individual_id', $individual->id)->where('id', $input['strategy'])->firstOrFail();
        
        $location = $input['location'] == 'Other' ? $input['location_other'] : 'Home';
        
		$case_note->service_id = isset($input['service']) ? $input['service'] : NULL;
		$case_note->reportable_id = isset($input['reportable']) ? $input['reportable'] : NULL;
		$case_note->goal_id = $strategy->goal_id;
		$case_note->strategy_id = $strategy->id;
		$case_note->edit_by_id = Auth::id();
		$case_note->note_date = Carbon::parse($input['start_dt']);
		$case_note->start_dt = Carbon::parse($input['start_dt']);
		$case_note->end_dt = Carbon::parse($input['end_dt']);
		$case_note->case_note = $input['note'];
        $case_note->priority = $input['priority'];

        $case_note->engaged = $input['engaged'];
        $case_note->engaged_other = $input['engaged_other'];
        $case_note->goal_duration = $input['goal_duration'];
        $case_note->meaning_full = $input['meaning_full'];
        $case_note->qol_value = $input['qol_value'];
        $case_note->qol_plus_eoc = $input['qol_plus_eoc'];
        $case_note->qol_minus_eoc = $input['qol_minus_eoc'];
        $case_note->goal_ability = $input['goal_ability'];          
        $case_note->goal_engagement = $input['goal_engagement'];
        $case_note->location = $location;
        $case_note->reject_date = NULL;
        $case_note->reject_note = NULL;
		$case_note->save();
		
		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Service Note Updated');
	}
	
	public function getListCaseNote($id)
	{
		$individual = ProviderIndividual::with('casenotes.user', 'casenotes.service', 'casenotes.strategy', 'casenotes.goal')->where('id', decrypt($id))->firstOrFail();
		
		return view('individual.casenote.list', ['individual' => $individual]); 
	}
	
	public function getViewMedication($id)
	{
		$individual = ProviderIndividual::with('retired_medications.logs')->with('medications')->where('id', decrypt($id))->firstOrFail();
		return view('individual.medication.view', ['individual' => $individual]); 
	}
	
	public function postAddMedication(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required',
			'dosage' => 'required',
		], array('name.required' => 'Medication Name is required.', 'dosage.required' => 'Dosage is required.'));
		$input = $request->all();
		
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		$fill_date = isset($input['fille_date']) ? date('Y-m-d', strtotime($input['fill_date'])) : NULL;
		
		$medication = ProviderIndividualMedication::create([
			'user_id' => Auth::user()->id,
            'individual_id' => $individual->id,
			'name' => $input['name'],
			'dosage' => $input['dosage'],
			'type' => $input['type'],
			'number_doses' => $input['number_doses'],
			'fill_date' => $fill_date,
			'note' => $input['note'],
		]);
		
		if(isset($input['sun_schedule'])) {
			foreach($input['sun_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 7,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['mon_schedule'])) {
			foreach($input['mon_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 1,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['tue_schedule'])) {
			foreach($input['tue_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 2,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['wed_schedule'])) {
			foreach($input['wed_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 3,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['thu_schedule'])) {
			foreach($input['thu_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 4,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['fri_schedule'])) {
			foreach($input['fri_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 5,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		if(isset($input['sat_schedule'])) {
			foreach($input['sat_schedule'] AS $time) {
				$medication_schedule = ProviderIndividualMedicationSchedule::create([
					'medication_id' => $medication->id,
					'individual_id' => $individual->id,
					'day_of_week' => 6,
					'time' => date('H:i', strtotime($time))
				]);
			}
		}
		
		return redirect()->back()->with('message', 'Medication Added.');
	}
	
	public function getTakeMedication($id, $med)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$medication = ProviderIndividualMedication::where('id', decrypt($med))->firstOrFail();
		$log = ProviderIndividualMedicationLog::create([
			'medication_id' => $medication->id,
			'individual_id' => $individual->id,
			'user_id'		=> Auth::id(),
			'administered_dt' => Carbon::now()
		]);
		
		return redirect()->back()->with('message', 'Medication Administered');
	}

	public function getRemoveTakenMedication($id, $med, $med_taken_id)
	{
		ProviderIndividualMedicationLog::where('id', decrypt($med_taken_id))->where('medication_id', decrypt($med))->where('individual_id', decrypt($id))->delete();
		
		return redirect()->back()->with('message', 'Medication Log Removed');
	}
	
	public function getRemoveMedication($id, $med)
	{
		$medication = ProviderIndividualMedication::where('id', decrypt($med))->delete();
		return redirect()->back()->with('message', 'Medication Removed');
	}
	
	public function getTakenMedication($id)
	{
		$individual = ProviderIndividual::with('medication_logs.medication', 'medication_logs.user')->where('id', decrypt($id))->firstOrFail();
		return view('individual.medication.taken-report', ['individual' => $individual]); 
	}
	
	////////////////////////
	/*  Goals/Strategies */
	////////////////////////
	public function getViewGoal($id)
	{
		$individual = ProviderIndividual::with('goals.strategies')->where('id', decrypt($id))->firstOrFail();
		return view('individual.goal.view', ['individual' => $individual]); 
	}
	
	public function postAddGoal(Request $request, $id)
	{
		$this->validate($request, [
			'goal' => 'required'
		]);
		$input = $request->all();
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$goal = ProviderIndividualGoal::create([
			'individual_id' => $individual->id,
			'user_id'		=> Auth::id(),
			'goal' => $input['goal']
		]);
		
		return redirect()->back()->with('message', 'Goal Created');
	}
	
	public function postAddStrategy(Request $request, $id)
	{
		$this->validate($request, [
			'strategy' => 'required'
		]);
		$input = $request->all();
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$goal = ProviderIndividualGoal::where('id', decrypt($input['goal_id']))->firstOrFail();
		$strategy = ProviderIndividualStrategy::create([
			'individual_id' => $individual->id,
			'user_id'		=> Auth::id(),
			'goal_id' => $goal->id,
			'strategy' => $input['strategy']
		]);
		
		return redirect()->back()->with('message', 'Strategy Created');
	}

	/****************
	Start Medical Appointments
	*************/
	public function getMedicalAppointmentAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$medical_appointment = new ProviderIndividualMedicalAppointment();
		return view('individual.medical-appointment.add', ['individual' => $individual, 'medical_appointment' => $medical_appointment, 'action' => $action]);
	}

	public function postMedicalAppointmentAdd(Request $request, $id, $apointment_id=null)
	{
		$this->validate($request, [
			'appointment_date' => 'required|date'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		if($apointment_id == null) {
			$word = 'Added';//this is for the string for return
			$medical_appointment = ProviderIndividualMedicalAppointment::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'appointment_date' => Carbon::parse($input['appointment_date']),
				'doctor_name' => trim($input['doctor_name']),
				'purpose_of_visit' => trim($input['purpose_of_visit']),
				'follow_up_date' => isset($input['follow_up_date']) ? Carbon::parse($input['follow_up_date']) : null
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$medical_appointment = ProviderIndividualMedicalAppointment::where('id', decrypt($apointment_id))->firstOrFail();
			$medical_appointment->appointment_date = Carbon::parse($input['appointment_date']);
			$medical_appointment->doctor_name = trim($input['doctor_name']);
			$medical_appointment->purpose_of_visit = trim($input['purpose_of_visit']);
			$medical_appointment->follow_up_date = isset($input['follow_up_date']) ? Carbon::parse($input['follow_up_date']) : null;
			$medical_appointment->save();
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Field Coordinator Checklist ' . $word);
	}

	public function getMedicalAppointmentEdit($id, $apointment_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$medical_appointment = ProviderIndividualMedicalAppointment::where('id', decrypt($apointment_id))->firstOrFail();
		return view('individual.medical-appointment.add', ['individual' => $individual, 'medical_appointment' => $medical_appointment, 'action' => $action]);
	}

	public function getMedicalAppointmentView($id, $apointment_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$medical_appointment = ProviderIndividualMedicalAppointment::where('id', decrypt($apointment_id))->firstOrFail();
		return view('individual.medical-appointment.view', ['individual' => $individual, 'medical_appointment' => $medical_appointment]);
	}
	public function getMedicalAppointmentRemove($id, $apointment_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$medical_appointment = ProviderIndividualMedicalAppointment::where('individual_id', $individual->id)->where('id', decrypt($apointment_id))->delete();
		return redirect()->back()->with('message', 'Medical Appointment Removed.');
	}

	/****************
	Start Support Team
	*************/

	public function getSupportTeamAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$support_team = new ProviderIndividualSupportTeam();
		return view('individual.support-team.add', ['individual' => $individual, 'support_team' => $support_team, 'member_types' => $this->member_types, 'action' => $action]);
	}

	public function postSupportTeamAdd(Request $request, $id, $member_id=null)
	{
		$this->validate($request, [
			'member_type' => 'required'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		if($member_id == null) {
			$word = 'Added';//this is for the string for return
			$support_team = ProviderIndividualSupportTeam::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'member_type' => trim($input['member_type']),
				'first_name' => trim($input['first_name']),
				'last_name' => trim($input['last_name']),
				'email' => trim($input['email']),
				'phone' => trim($input['phone'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$support_team = ProviderIndividualSupportTeam::where('id', decrypt($member_id))->firstOrFail();
			$support_team->member_type = trim($input['member_type']);
			$support_team->first_name = trim($input['first_name']);
			$support_team->last_name = trim($input['last_name']);
			$support_team->email = trim($input['email']);
			$support_team->phone = trim($input['phone']);
			$support_team->save();
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Field Coordinator Checklist ' . $word);
	}

	public function getSupportTeamEdit($id, $member_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$support_team = ProviderIndividualSupportTeam::where('id', decrypt($member_id))->firstOrFail();
		return view('individual.support-team.add', ['individual' => $individual, 'support_team' => $support_team, 'member_types' => $this->member_types, 'action' => $action]);
	}

	public function getSupportTeamView($id, $member_id)
	{
		$concern = new ProviderIndividualSupportTeamConcern();
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$support_team = ProviderIndividualSupportTeam::with('concerns')->where('id', decrypt($member_id))->firstOrFail();
		return view('individual.support-team.view', ['individual' => $individual, 'support_team' => $support_team, 'concern' => $concern]);
	}
	public function getSupportTeamRemove($id, $member_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$support_team = ProviderIndividualSupportTeam::where('individual_id', $individual->id)->where('id', decrypt($member_id))->delete();
		return redirect()->back()->with('message', 'Team Member Removed.');
	}

	/**
	 * Support Team Concerns
	 */

	public function postSupportTeamConcernAdd(Request $request, $id, $member_id)
	{
		$this->validate($request, [
			'date_of_concern' => 'required|date'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$support_team_member = ProviderIndividualSupportTeam::where('id', decrypt($member_id))->firstOrFail();
		
		if($support_team_member) {
			$concern = ProviderIndividualSupportTeamConcern::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'support_team_id' => $support_team_member->id,
				'date_of_concern' => Carbon::parse($input['date_of_concern']),
				'concern' => trim($input['concern'])
			]);
		}

		return redirect()->back()->with('message', 'Concern Added.');
	}

	public function getSupportTeamConcernRemove($id, $concern_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		ProviderIndividualSupportTeamConcern::where('individual_id', $individual->id)->where('id', decrypt($concern_id))->delete();
		return redirect()->back()->with('message', 'Concern Removed.');
	}

	/****************
	Start Seizures
	*************/

	public function getSeizuresAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$seizure = new ProviderIndividualSeizure();
		return view('individual.seizures.add', ['individual' => $individual, 'seizure' => $seizure, 'yes_no' => $this->yes_no, 'action' => $action]);
	}

	public function postSeizuresAdd(Request $request, $id, $seizure_id=null)
	{
		$this->validate($request, [
			'seizure_date' => 'required|date'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		if($seizure_id == null) {
			$word = 'Added';//this is for the string for return
			$seizure = ProviderIndividualSeizure::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'seizure_date' => Carbon::parse($input['seizure_date']),
				'follow_up_needed' => trim($input['follow_up_needed']),
				'additional_note' => trim($input['additional_note'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$seizure = ProviderIndividualSeizure::where('id', decrypt($seizure_id))->firstOrFail();
			$seizure->seizure_date = Carbon::parse($input['seizure_date']);
			$seizure->follow_up_needed = trim($input['follow_up_needed']);
			$seizure->additional_note = trim($input['additional_note']);
			$seizure->save();
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Seizure Incident ' . $word);
	}

	public function getSeizuresEdit($id, $seizure_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$seizure = ProviderIndividualSeizure::where('id', decrypt($seizure_id))->firstOrFail();
		return view('individual.seizures.add', ['individual' => $individual, 'seizure' => $seizure, 'yes_no' => $this->yes_no, 'action' => $action]);
	}

	public function getSeizuresView($id, $seizure_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$seizure = ProviderIndividualSeizure::where('id', decrypt($seizure_id))->firstOrFail();
		return view('individual.seizures.view', ['individual' => $individual, 'seizure' => $seizure, 'yes_no' => $this->yes_no]);
	}
	public function getSeizuresRemove($id, $seizure_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$seizure = ProviderIndividualSeizure::where('individual_id', $individual->id)->where('id', decrypt($seizure_id))->delete();
		return redirect()->back()->with('message', 'Seizure Incident Removed.');
	}

	/****************
	Start Risk Plans
	*************/

	public function getRiskPlanAdd($id)
	{
		$action = 'Add';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$risk_plan = new ProviderIndividualRiskPlan();
		return view('individual.risk-plan.add', ['individual' => $individual, 'risk_plan' => $risk_plan, 'plan_types' => $this->plan_types, 'action' => $action]);
	}

	public function postRiskPlanAdd(Request $request, $id, $plan_id=null)
	{
		$this->validate($request, [
			'plan_date' => 'required|date'
		], array());	
		$input = $request->all();

		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		
		if($plan_id == null) {
			$word = 'Added';//this is for the string for return
			$risk_plan = ProviderIndividualRiskPlan::create([
				'user_id' => Auth::user()->id,
				'individual_id' => $individual->id,
				'plan_date' => Carbon::parse($input['plan_date']),
				'plan_type' => trim($input['plan_type'])
			]);
		}
		else {
			$word = 'Updated';//this is for the string for return
			$risk_plan = ProviderIndividualRiskPlan::where('id', decrypt($plan_id))->firstOrFail();
			$risk_plan->plan_date = Carbon::parse($input['plan_date']);
			$risk_plan->plan_type = trim($input['plan_type']);
			$risk_plan->save();
		}

		return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Risk Plan ' . $word);
	}

	public function getRiskPlanEdit($id, $plan_id)
	{
		$action = 'Edit';
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$risk_plan = ProviderIndividualRiskPlan::where('id', decrypt($plan_id))->firstOrFail();
		return view('individual.risk-plan.add', ['individual' => $individual, 'risk_plan' => $risk_plan, 'plan_types' => $this->plan_types, 'action' => $action]);
	}

	public function getRiskPlanRemove($id, $plan_id)
	{
		$individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
		$risk_plan = ProviderIndividualRiskPlan::where('individual_id', $individual->id)->where('id', decrypt($plan_id))->delete();
		return redirect()->back()->with('message', 'Risk Plan Removed.');
    }
    
    public function getGraphView($id, $graph_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $graph = ProviderIndividualGraph::with('graph_data')->where('id', decrypt($graph_id))->firstOrFail();

        $graph->item_labels = json_encode(array($graph->item_1, $graph->item_2, $graph->item_3, $graph->item_4));
        $x_axis_labels = array();
        $data_1 = array();
        $data_2 = array();
        $data_3 = array();
        $data_4 = array();
        foreach($graph->graph_data AS $data) {
            $x_axis_labels[] = $data->data_date->timestamp*1000;
            if($data->data_1 != '')
                $data_1[] = $data->data_1;
            if($graph->item_2 != '' && $data->data_2 != '')
                $data_2[] = $data->data_2;
            if($graph->item_3 != '' && $data->data_3 != '')
                $data_3[] = $data->data_3;
            if($graph->item_4 != '' && $data->data_4 != '')
                $data_4[] = $data->data_4;
        }

        $x_axis_labels = json_encode($x_axis_labels);
        $data_1 = json_encode($data_1);
        $data_2 = json_encode($data_2);
        $data_3 = json_encode($data_3);
        $data_4 = json_encode($data_4);

        return view('individual.graphs.view', ['individual' => $individual, 'graph' => $graph, 'x_axis_labels' => $x_axis_labels, 'data_1' => $data_1, 'data_2' => $data_2,
                                            'data_3' => $data_3, 'data_4' => $data_4]);
    }

    public function postGraphView(Request $request, $id, $graph_id)
    {
        $this->validate($request, [
            'data_date' => 'required|date',
            'data_1' => 'required',
        ], array('data_1.required' => 'Please enter data into Data 1'));

        $input = $request->all();
        
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $graph = ProviderIndividualGraph::where('id', decrypt($graph_id))->firstOrFail();

        $graph = ProviderIndividualGraphData::create([
            'graph_id' => $graph->id,
            'data_date' => isset($input['data_date']) ? Carbon::parse($input['data_date']) : NULL,
            'data_1' => $input['data_1'] != '' ? $input['data_1'] : 0,
            'data_2' => $input['data_2'] != '' ? $input['data_2'] : 0,
            'data_3' => $input['data_3'] != '' ? $input['data_3'] : 0,
            'data_4' => $input['data_4'] != '' ? $input['data_4'] : 0,
        ]);

        return redirect()->back()->with('message', 'Data Added');
    }

    public function getRemoveGraphData($id, $graph_id, $data_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $graph = ProviderIndividualGraph::where('id', decrypt($graph_id))->firstOrFail();
        ProviderIndividualGraphData::where('id', $data_id)->where('graph_id', $graph->id)->delete();

        return redirect()->back()->with('message', 'Data Removed');
    }

    public function getGraphAdd($id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        
        return view('individual.graphs.add', ['individual' => $individual]);
    }

    public function postGraphAdd(Request $request, $id)
    {

        $input = $request->all();
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        
        $graph = ProviderIndividualGraph::create([
            'individual_id' => $individual->id,
            'graph_title' => $input['graph_title'],
            'item_1' => $input['item_1'],
            'item_2' => $input['item_2'],
            'item_3' => $input['item_3'],
            'item_4' => $input['item_4'],
        ]);
        
        return redirect('/individual/view/'.encrypt($individual->id))->with('message', 'Graph Aded - ' . $input['graph_title']);
    }

    public function getRemoveGraph($id, $graph_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $graph = ProviderIndividualGraph::where('id', decrypt($graph_id))->delete();

        return redirect()->back()->with('message', 'Graph Removed');
    }

    public function getCaseNoteApproval($id, $note_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $case_note = ProviderIndividualCaseNote::where('id', decrypt($note_id))->firstOrFail();

        $case_note->approval_date = new Carbon;
        $case_note->reject_date = NULL;
        $case_note->reject_note = NULL;
        $case_note->save();

        return redirect()->back()->with('message', 'Note Approved');
    }

    public function getCaseNoteReject($id, $note_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $case_note = ProviderIndividualCaseNote::where('id', decrypt($note_id))->firstOrFail();

        return view('individual.casenote.reject', ['individual' => $individual, 'case_note' => $case_note]);
    }

    public function postCaseNoteReject(Request $request, $id, $note_id)
    {
        $input = $request->all();
        
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $case_note = ProviderIndividualCaseNote::where('id', decrypt($note_id))->firstOrFail();

        $case_note->reject_note = $input['reject_note'];
        $case_note->reject_date = new Carbon;
        $case_note->save();

        return redirect('/provider/report/case-notes/approval')->with('message', 'Note Rejected');
    }

    public function getCaseNoteUnApprove($id, $note_id)
    {
        $individual = ProviderIndividual::where('id', decrypt($id))->firstOrFail();
        $case_note = ProviderIndividualCaseNote::where('id', decrypt($note_id))->firstOrFail();

        $case_note->approval_date = NULL;
        $case_note->save();

        return redirect()->back()->with('message', 'Note Approval Removed');
    }
}
