<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Upload;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Provider;
use App\Models\ProviderIndividual;
use App\Models\ProviderIndividualMedication;
//use App\Models\ProviderIndividualMedicationSchedule;
use App\Jobs\SendActivationEmail;
use App\Imports\UserImport;


class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Admin Controller
    |--------------------------------------------------------------------------
    |
    */

    public function getHome()
    {
		$user = Provider::with('houses')->withTrashed()->get();
		
		return view('home', ['user' => $user]);
    }
	
	public function getProviderList()
	{
		$providers = Provider::with('houses', 'individuals', 'users')->withTrashed()->get();
		return view('site-admin.provider.list', ['providers' => $providers]);
	}
	
	public function getProviderAdd()
	{
		return view('site-admin.provider.add');
	}
	
	public function postProviderAdd(Request $request)
	{
		$this->validate($request, [
			'provider_name' => 'required|max:50',
			'address' => 'required|max:50',
			'city' => 'required|max:50',
			'state' => 'required|max:50',
			'zip' => 'required|max:50',
		
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'username' => 'required|unique:tblUser|max:50',
			'email' => 'email|nullable|max:255'
		], array('username.unique' => 'That username is already taken.'));
		
		$input = $request->all();
		
		$provider = Provider::create([
            'provider_name' => $input['provider_name'],
            'address' => $input['address'],
            'city' => $input['city'],
            'state' => $input['state'],
            'zip' => $input['zip'],
            'phone_primary' => $input['phone_primary'],
            'phone_secondary' => $input['phone_secondary']            
        ]);
		
		$password_phrase = '';
		$password_clear = $this->generate_password();
        $password_hash = bcrypt($password_clear);
		$password_phrase .= ' -  User password set to:   '.$password_clear;
		
		$user = User::create([
			'provider_id' => $provider->id,
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'username' => $input['username'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'password' => $password_hash
        ]);
		
		$role = Role::where('group', 'provider')->where('type', 'Administrator')->first();
		
		$userrole = UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);
		
		return redirect()->to('/site-admin/provider/list')->with('message', 'Provider Added.  ' . $password_phrase);
	}
	
	public function getProviderEdit($provider_id)
	{
		$provider = Provider::with('users', 'individuals', 'houses')->where('id', decrypt($provider_id))->withTrashed()->firstOrFail();
		return view('site-admin.provider.edit', ['provider' => $provider]);
	}
	
	public function postProviderEdit(Request $request, $provider_id)
	{
		$this->validate($request, [
			'provider_name' => 'required|max:50',
			'address' => 'required|max:50',
			'city' => 'required|max:50',
			'state' => 'required|max:50',
			'zip' => 'required|max:50',
		
			
		], array());
		
		$input = $request->all();
		
		$provider = Provider::where('id', decrypt($provider_id))->withTrashed()->firstOrFail();
		
		$provider->provider_name = $input['provider_name'];
		$provider->address = $input['address'];
		$provider->city = $input['city'];
		$provider->state = $input['state'];
		$provider->zip = $input['zip'];
		$provider->phone_primary = $input['phone_primary'];
		$provider->phone_secondary = $input['phone_secondary'];
		$provider->save();
		
		
		return redirect()->to('/site-admin/provider/list')->with('message', 'Provider Updated.');
	}
	
	public function getUserList()
	{
		$users = User::get();
		return view('site-admin.user.list', ['users' => $users]);
	}
		
	public function getProviderRemove($provider_id)
	{
		$provider = Provider::where('id', decrypt($provider_id))->firstOrFail();
		$provider->delete();
		return back()->with('message', 'Provider Removed');
	}
	
	public function resetUserPw($id)
	{
		$user = User::where('id', decrypt($id))->firstOrFail();
		$password_phrase = '';
		$password_clear = $this->generate_password();
        $password_hash = bcrypt($password_clear);
		$password_phrase .= ' -  User password set to:   '.$password_clear;
		$user->password = $password_hash;
		$user->save();
		
		return back()->with('message', $password_phrase);
    }
    
    public function importUser()
    {
        (new UserImport)->import('staff phone list.csv');
        return back()->with('message', 'Import Complete');
    }
	
	public function generate_password($length = 6, $add_dashes = false, $available_sets = 'ud')
    {
        $sets = array();
        
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        
        $all = '';
        $password = '';
        
        foreach($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        
        $all = str_split($all);
        
        for($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
            
        $password = str_shuffle($password);
        
        if(!$add_dashes) {
            return $password;
        }
        
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        
        while(strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        
        $dash_str .= $password;
        return $dash_str;
    }
}
