<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Provider;
use App\Models\ProviderHouse;
use App\Models\ProviderHouseIndividual;
use App\Models\ProviderIndividual;
use App\Models\ProviderIndividualCaseNote;
use App\Models\ProviderIndividualMedication;
use App\Models\ProviderIndividualMedicationLog;
use App\Models\ProviderIndividualMedicationSchedule;
use App\Models\ProviderTraining;


class ReportController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Report Controller
    |--------------------------------------------------------------------------
    |
    */

    public function getMedicationByIndividual()
    {
          
        $individuals = ProviderIndividual::with('medications')->where('provider_id', Auth::user()->provider_id)->get();
	
	    return view('reports.medication.by-individual', ['individuals' => $individuals]);
	}
	
	public function getMissedMedicationByTime(Request $request)
	{
		$input = $request->all();
		
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(7);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with('medications.schedules')
											->with(array('medications.logs' => function($query) use($start_date, $end_date) {
												$query->whereBetween('administered_dt', array($start_date, $end_date));
												//$query->where('medication_id', 2);		
											}))->where('provider_id', Auth::user()->provider_id)->get();
		$individuals_missed_meds = $individuals->each(function ($individual, $key) use ($start_date, $end_date){
			$individual->missed_meds($start_date, $end_date);
		});
		
		return view('reports.medication.missed-by-time', ['individuals' => $individuals, 'start_date' => $start_date, 'end_date' => $end_date, 'individuals_missed_meds' => $individuals_missed_meds]);
    }
	
	public function getMinorIncidentNote(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) use($start_date, $end_date) {
												$query->whereBetween('note_date', array($start_date, $end_date))
													  ->where('priority', 'Non Reportable Incident');
											}))->where('provider_id', Auth::user()->provider_id)->get();
		
		return view('reports.casenote.minor-incident', ['individuals' => $individuals, 'start_date' => $start_date, 'end_date' => $end_date]);
	}
	
	public function getMajorIncidentNote(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) use($start_date, $end_date) {
												$query->whereBetween('note_date', array($start_date, $end_date))
													  ->where('priority', 'Reportable Incident');
											}))->where('provider_id', Auth::user()->provider_id)->get();
											
		return view('reports.casenote.major-incident', ['individuals' => $individuals, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
    
    public function getIndividualNote(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) use($start_date, $end_date) {
												$query->whereBetween('note_date', array($start_date, $end_date));
											}))->where('provider_id', Auth::user()->provider_id)->get();
		
		return view('reports.casenote.by-individual', ['individuals' => $individuals, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
    
    public function getQolNote(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) use($start_date, $end_date) {
												$query->whereBetween('note_date', array($start_date, $end_date));
											}))->where('provider_id', Auth::user()->provider_id)->get();
		
		return view('reports.casenote.with-qol', ['individuals' => $individuals, 'start_date' => $start_date, 'end_date' => $end_date]);
	}
	
	public function getTrainingByEmployee(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$users = User::with(array('trainings' => function($query) use($start_date, $end_date) {
										$query->whereBetween('tblProviderTrainingUser.created_at', array($start_date, $end_date));
									}))->where('provider_id', Auth::user()->provider->id)->get();
        
		return view('reports.training.by_employee', ['users' => $users, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
    
    public function getClockInOutByEmployee(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$users = User::with(array('time_punches' => function($query) use($start_date, $end_date) {
										$query->whereBetween('tblUserTimePunch.time_punch', array($start_date, $end_date));
									}))->where('provider_id', Auth::user()->provider->id)->get();
        
		return view('reports.time-punches.by_employee', ['users' => $users, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
    
    public function getHoursByEmployee(Request $request)
	{
		$input = $request->all();
		$start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$users = User::with(array('time_punches' => function($query) use($start_date, $end_date) {
										$query->whereBetween('tblUserTimePunch.time_punch', array($start_date, $end_date));
                                    }))->where('provider_id', Auth::user()->provider->id)->get();
        $users->each(function($user, $key) {
            $user->calculate_hours();
        });
        
		return view('reports.time-punches.employee_hours', ['users' => $users, 'start_date' => $start_date, 'end_date' => $end_date]);
    }
    
    public function getIndividualNoteApproval()
	{	
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) {
                                                $query->where('approval_date', NULL)
                                                ->where('reject_date', NULL);
											}))->where('provider_id', Auth::user()->provider_id)->get();
		
		return view('reports.casenote.approval', ['individuals' => $individuals]);
    }
    
    public function getIndividualNoteApproved(Request $request)
	{	
        $input = $request->all();
        $start_date = isset($input['start_date']) ? Carbon::parse($input['start_date']) : Carbon::now()->subDays(30);
		$end_date = isset($input['end_date']) ? Carbon::parse($input['end_date']) : Carbon::now();
		
		$individuals = ProviderIndividual::with(array('report_casenotes' => function($query) use($start_date, $end_date) {
                                                $query->whereBetween('note_date', array($start_date, $end_date))
                                                ->where('approval_date', '!=', NULL);
											}))->where('provider_id', Auth::user()->provider_id)->get();
		
		return view('reports.casenote.approved', ['individuals' => $individuals]);
	}
	
}
