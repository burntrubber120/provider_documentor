<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Upload;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\FileUpload;
use App\Models\ProviderIndividual;
use App\Models\ProviderHouse;
use App\Models\ProviderHouseUser;
use App\Models\ProviderTraining;
use App\Models\ProviderTrainingUser;
use App\Models\ProviderTrainingQuestion;
use App\Models\ProviderTrainingQuestionAnswer;

class TrainingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Training Controller
    |--------------------------------------------------------------------------
    |
    */
	
	public function getTrainingList()
	{
		$trainings = ProviderTraining::where('individual_id', NULL)->where('provider_id', Auth::user()->provider_id)->get();
		if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor'])) {
			$consumer_specific_trainings = ProviderTraining::where('individual_id', '!=', NULL)->where('provider_id', Auth::user()->provider_id)->get();
		}
		else {
			$user = User::with('current_houses.individuals')->where('id', Auth::id())->firstOrFail();
			$individuals = collect([]);
			foreach($user->current_houses AS $house) {
				foreach($house->individuals AS $individual) {
					$individuals->push($individual);
				}
			}
			$consumer_specific_trainings = ProviderTraining::whereIn('individual_id', $individuals->pluck('id'))->where('provider_id', Auth::user()->provider_id)->get();
		}
		return view('training.list', ['trainings' => $trainings, 'consumer_specific_trainings' => $consumer_specific_trainings]);
	}
	
	public function getAddTraining()
	{
		$individuals = ProviderIndividual::where('provider_id', Auth::user()->provider_id)->get();
		return view('training.add', ['individuals' => $individuals]);
	}
	
	public function postAddTraining(Request $request)
	{
		$this->validate($request, [
			'description' => 'max:250',
			'website' => 'max:250',
		], array());
		
		$input = $request->all();

		$questions = array();
		$question_number = 0;
		$response_number = 0;
		foreach($input AS $key => $value) {
			if(strpos($key, "question") !== false) {
				$question_number = ltrim($key, "question_");
				$questions[$question_number]['text'] = trim($value);
			}
			if(strpos($key, "type") !== false) {
				$question_number = ltrim($key, "type_");
				$questions[$question_number]['type'] = trim($value);
			}
			if(strpos($key, "response") !== false) {
				$explode = explode('_', $key);
				$question_number = ltrim($explode[0], "q");
				$response_number = ltrim($explode[1], "response");
				$questions[$question_number]['responses'][$response_number] = trim($value);
			}
			if(strpos($key, 'q'.$question_number) !== false && strpos($key, "correctanswer") !== false) {
				$explode = explode('_', $key);
				$question_number = ltrim($explode[0], "q");
				$response_number = ltrim($explode[1], "correctanswer");				
				$questions[$question_number]['correct'][$response_number] = true;
			}
		}
		
		$training = ProviderTraining::create([
			'provider_id' => Auth::user()->provider_id,
			'individual_id' => $input['individual'] != '0' ? decrypt($input['individual']) : NULL,
            'title' => trim($input['title']),
            'description' => trim($input['description']),
            'website' => trim($input['website'])
        ]);
		
		foreach($questions AS $q_num => $question) {
			$training_question = ProviderTrainingQuestion::create([
				'training_id' => $training->id,
				   'question' => $question['text'],
				   'type' => $question['type']
			]);
			foreach($question['responses'] AS $r_num => $response) {
				$answer = ProviderTrainingQuestionAnswer::create([
					'question_id' => $training_question->id,
					'answer' => $response,
					'correct' => isset($questions[$q_num]['correct'][$r_num]) ? 1 : 0
				]);
			}
		}

		return redirect()->back()->with('message', 'Training Added.');
	}
	
	public function getEditTraining($id)
	{
		$training = Training::where('id', decrypt($id))->where('provider_id', Auth::user()->provider_id)->get();
		return view('training.edit', ['training' => $training]);
	}
	
	public function postEditTraining(Request $request, $id)
	{
		$this->validate($request, [
			'title' => 'required|max:50',
			'description' => 'max:250',
			'website' => 'max:250',
		], array());
		
		$training = ProviderTraining::where('id', decrypt($id))->firstOrFail();
		$training->title = trim($input['title']);
		$training->description = trim($input['description']);
		$training->website = trim($input['website']);
		$training->save();
		
		return redirect()->to('training/list')->with('message', 'Training Updated.');
	}
		
	public function getTrainingTake($id) 
	{
		$training = ProviderTraining::with('questions.answers')->where('id', decrypt($id))->where('provider_id', Auth::user()->provider_id)->first();
		
		return view('training.view', ['training' => $training]);
	}
	
	public function postTrainingTake(Request $request, $id)
	{
		$training = ProviderTraining::with('questions.answers')->where('id', decrypt($id))->where('provider_id', Auth::user()->provider_id)->first();
		
		$input = $request->all();
		
		$correct = 0;
		foreach($training->questions AS $question) {
			$user_answer = $input['q_'.$question->id];
			if($question->type == 'dropdown') {
				foreach($question->answers AS $answer) {
					if($user_answer == $answer->id && $answer->correct == 1) {
						$correct++;
					}
				}
			}
			if($question->type == 'checkbox') {
				
				$user_correct = $question->answers->where('correct', 1)->whereIn('id', $user_answer);
				$question_correct = $question->answers->where('correct', 1);				
				if(count($user_answer) == $user_correct->count() && $user_correct->count() == $question_correct->count()) {
					$correct++;
				}
				
			}
        }
        $question_count = $training->questions->count() > 0 ? $training->questions->count() : 1;
		$score = $correct/$question_count*100;
		
		$user_training = ProviderTrainingUser::create([
			'training_id' => $training->id,
			'user_id' => Auth::id(),
			'individual_id' => $training->individual_id != NULL ? $training->individual_id : NULL,
            'score' => $score,
        ]);
        
		return redirect()->to('training/list')->with('message', 'Training Complete with a score of '. $score . '.');
	}
}
