<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Classes\Upload;
use App\Models\User;
use App\Models\Role;
use App\Models\ProviderIndividual;
use App\Models\ProviderHouse;
use App\Models\ProviderIndividualMedication;
use App\Models\ProviderIndividualCaseNote;
//use App\Models\ProviderIndividualMedicationSchedule;
use App\Jobs\SendActivationEmail;


class DashboardController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Dashboard Controller
  |--------------------------------------------------------------------------
  |
  */

  public function getHome()
  {
    $today = date('Y-m-d');
    $user = User::with('current_houses.individuals_with_today_user_case_note.medications.today_times')->where('id', Auth::id())->firstOrFail();
    
    foreach($user->current_houses AS $house) {
      foreach($house->individuals_with_today_user_case_note AS $individual) {
        $individual->medications_with_missed();
      }
    }

    $houses = collect([]);
    if(Auth::user()->hasGroupType('provider', ['Administrator', 'Manager', 'Supervisor'])) {
      $houses = ProviderHouse::with('employees', 'individuals.medications.today_times.last_taken')->where('provider_id', Auth::user()->provider_id)->get();
      foreach($houses AS $house) {
        foreach($house->individuals AS $individual) {
          $individual->missed_meds();
        }
      }
    }

    $rejected_notes = ProviderIndividualCaseNote::with('individual')->where('reject_date', '!=', NULL)->where('user_id', Auth::user()->id)->get();

    //strtotime-strtotime >= 1800 is 30 minutes
    return view('home', ['user' => $user, 'houses' => $houses, 'rejected_notes' => $rejected_notes]);
  }

}
