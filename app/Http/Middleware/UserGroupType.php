<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class UserGroupType {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $grouptypes)
	{
		$grouptypes = explode(';', $grouptypes);
        $bool = false;

        if($this->auth->guest()) {
            redirect()->back()->withMessage('You must be logged in to access this area.');
        }
        
		//check types (tblRole -> type)
		foreach($grouptypes as $grouptype) {
			$grouptype = explode('|', $grouptype);
			
			if($request->user()->hasGroupType($grouptype[0], $grouptype[1])) {
				$bool = true;
			}
		}
		
        if($request->user()->hasGroupType('site', 'admin')) {
            $bool = true;
        }
        
        if($bool) {
            return $next($request);
        } else {
            return redirect()->back()->withMessage('You do not have permission to access this area.');
        }
	}

}