<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class UserGroup {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $groups)
	{
		$groups = explode('|', $groups);
        $bool = false;
        
        if($this->auth->guest()) {
            redirect()->back()->withMessage('You must be logged in to access this area.');
        }
        
		//check types (tblRole -> type)
		foreach($groups as $group) {
			if($request->user()->hasGroup($group)) {
				$bool = true;
			}
		}
		
		if($request->user()->hasGroupType('site', 'admin')) {
            $bool = true;
        }
		
        if($bool) {
            return $next($request);
        } else {
            return redirect()->back()->withMessage('You do not have permission to access this area.');
        }
	}

}